package refree.gautam.com.musicplayer.utils;

import android.app.Activity;
import android.content.Intent;

public class DirectionUtils {
    public static void changeActivity(Activity mActivity, int animIn, int animOut, boolean hasFinish, Intent mIntent) {
        if (mActivity != null && mIntent != null) {
            mActivity.startActivity(mIntent);
            mActivity.overridePendingTransition(animIn, animOut);
            if (hasFinish) {
                mActivity.finish();
            }
        }
    }
}
