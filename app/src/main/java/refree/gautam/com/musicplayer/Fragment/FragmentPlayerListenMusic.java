package refree.gautam.com.musicplayer.Fragment;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.github.clans.fab.FloatingActionButton;

import java.util.ArrayList;
import java.util.Iterator;

import eu.gsottbauer.equalizerview.EqualizerView;
import refree.gautam.com.musicplayer.DataManger.MusicDataMng;
import refree.gautam.com.musicplayer.InterFace.IXMusicConstants;
import refree.gautam.com.musicplayer.InterFace.IYPYCallback;
import refree.gautam.com.musicplayer.InterFace.YPYSettingManager;
import refree.gautam.com.musicplayer.Models.TrackModel;
import refree.gautam.com.musicplayer.R;
import refree.gautam.com.musicplayer.MainActivity;
import refree.gautam.com.musicplayer.abtractclass.fragment.DBFragment;
import refree.gautam.com.musicplayer.imageloader.GlideImageLoader;
import refree.gautam.com.musicplayer.imageloader.target.GlideViewGroupTarget;
import refree.gautam.com.musicplayer.playservice.IYPYMusicConstant;
import refree.gautam.com.musicplayer.utils.StringUtils;
import refree.gautam.com.musicplayer.view.CircularProgressBar;
import refree.gautam.com.musicplayer.view.MaterialIconView;
import refree.gautam.com.musicplayer.view.SliderView;

public class FragmentPlayerListenMusic extends DBFragment implements IXMusicConstants, OnClickListener {
    public static final int[] RES_ID_CLICKS = new int[]{R.id.btn_close, R.id.img_share, R.id.btn_next, R.id.btn_prev, R.id.img_add_playlist, R.id.img_equalizer, R.id.img_sleep_mode};
    public static final int[] RES_IMAGE = new int[]{R.drawable.ic_arrow_down_white_36dp, R.drawable.ic_share_white_36dp, R.drawable.ic_skip_next_white_36dp, R.drawable.ic_skip_previous_white_36dp, R.drawable.ic_add_to_playlist_white_36dp, R.drawable.ic_equalizer_white_36dp, R.drawable.ic_sleep_mode_white_36dp};
    public static final String TAG = FragmentPlayerListenMusic.class.getSimpleName();
    private MaterialIconView mBtnPlay;
    private ImageView mCbRepeat;
    private ImageView mCbShuffe;
    private MainActivity mContext;
    private long mCurrentId;
    private TrackModel mCurrentTrackObject;
    private EqualizerView mEqualizer;
    private ImageView mImgTrack;
    private LinearLayout mLayoutContent;
    private RelativeLayout mLayoutControl;
    private ArrayList<TrackModel> mListSongs;
    private CircularProgressBar mProgressBar;
    private SliderView mSeekbar;
    private GlideViewGroupTarget mTarget;
    private TextView mTvCurrentTime;
    private TextView mTvDuration;
    private TextView mTvSinger;
    private TextView mTvSong;

    class C14501 implements SliderView.OnValueChangedListener {
        C14501() {
        }

        public void onValueChanged(int value) {
            Log.d("value",String.valueOf(value));
            if (FragmentPlayerListenMusic.this.mCurrentTrackObject != null) {
                FragmentPlayerListenMusic.this.mContext.onProcessSeekAudio((int) (((float) (((long) value) * FragmentPlayerListenMusic.this.mCurrentTrackObject.getDuration())) / 100.0f));
            }
        }
    }

    class C14512 implements IYPYCallback {
        C14512() {
        }

        public void onAction() {
            FragmentPlayerListenMusic.this.mContext.notifyData(9);
        }
    }

    public View onInflateLayout(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_player_listen_music, container, false);
    }

    public void findView() {
        this.mContext = (MainActivity) getActivity();
        this.mImgTrack = (ImageView) this.mRootView.findViewById(R.id.img_track);
        this.mTvSong = (TextView) this.mRootView.findViewById(R.id.tv_current_song);
        this.mTvSong.setTypeface(this.mContext.mTypefaceBold);
        this.mTvSinger = (TextView) this.mRootView.findViewById(R.id.tv_current_singer);
        this.mTvSinger.setTypeface(this.mContext.mTypefaceNormal);
        ((TextView) this.mRootView.findViewById(R.id.tv_now_playing)).setTypeface(this.mContext.mTypefaceBold);
        this.mEqualizer = (EqualizerView) this.mRootView.findViewById(R.id.big_equalizer);
        int len = RES_ID_CLICKS.length;
        for (int i = 0; i < len; i++) {
            ImageView mImgView = (ImageView) this.mRootView.findViewById(RES_ID_CLICKS[i]);
            mImgView.setOnClickListener(this);
            this.mContext.setUpImageViewBaseOnColor((View) mImgView, this.mContext.mIconColor, RES_IMAGE[i], false);
        }
        FloatingActionButton mFloatingActionButton = (FloatingActionButton) this.mRootView.findViewById(R.id.fb_play);
        mFloatingActionButton.setColorNormal(this.mContext.getResources().getColor(R.color.colorAccent));
        mFloatingActionButton.setColorPressed(this.mContext.getResources().getColor(R.color.colorAccent));
        mFloatingActionButton.setColorRipple(getResources().getColor(R.color.main_color_divider));
        mFloatingActionButton.setOnClickListener(this);
        this.mProgressBar = (CircularProgressBar) this.mRootView.findViewById(R.id.progressBar1);
        this.mProgressBar.setVisibility(View.VISIBLE);
        this.mLayoutControl = (RelativeLayout) this.mRootView.findViewById(R.id.layout_control);
        this.mLayoutContent = (LinearLayout) this.mRootView.findViewById(R.id.layout_content);
        this.mBtnPlay = (MaterialIconView) this.mRootView.findViewById(R.id.btn_play);
        this.mTvCurrentTime = (TextView) this.mRootView.findViewById(R.id.tv_current_time);
        this.mTvCurrentTime.setTypeface(this.mContext.mTypefaceLight);
        this.mTvDuration = (TextView) this.mRootView.findViewById(R.id.tv_duration);
        this.mTvDuration.setTypeface(this.mContext.mTypefaceLight);
        this.mSeekbar = (SliderView) this.mRootView.findViewById(R.id.seekBar1);
        this.mSeekbar.setProcessColor(getResources().getColor(R.color.colorAccent));
        this.mSeekbar.setBackgroundColor(getResources().getColor(R.color.default_image_color));
        this.mSeekbar.setOnValueChangedListener(new C14501());
        this.mCbShuffe = (ImageView) this.mRootView.findViewById(R.id.cb_shuffle);
        this.mCbShuffe.setOnClickListener(this);
        updateTypeShuffle();
        setUpBackground();
        this.mCbRepeat = (ImageView) this.mRootView.findViewById(R.id.cb_repeat);
        this.mCbRepeat.setOnClickListener(this);
        updateTypeRepeat();
        this.mCurrentTrackObject = MusicDataMng.getInstance().getCurrentTrackObject();
        this.mCurrentId = this.mCurrentTrackObject != null ? this.mCurrentTrackObject.getId() : 0;
        showLoading(MusicDataMng.getInstance().isLoading());
        if (MusicDataMng.getInstance().isPlayingMusic()) {
            this.mEqualizer.animateBars();
        } else {
            this.mEqualizer.stopBars();
        }
        onPlayerUpdateState(MusicDataMng.getInstance().isPlayingMusic());
        updateInformation();
    }

    private void updateTypeShuffle() {
        if (this.mCbShuffe != null) {
            this.mContext.setUpImageViewBaseOnColor(this.mCbShuffe, getResources().getColor(YPYSettingManager.getShuffle(this.mContext) ? R.color.colorAccent : R.color.icon_color), (int) R.drawable.ic_shuffle_white_36dp, false);
        }
    }

    private void updateTypeRepeat() {
        if (this.mCbRepeat != null) {
            int type = YPYSettingManager.getNewRepeat(this.mContext);
            if (type == 0) {
                this.mContext.setUpImageViewBaseOnColor(this.mCbRepeat, this.mContext.mIconColor, (int) R.drawable.ic_repeat_white_36dp, false);
            } else if (type == 1) {
                this.mContext.setUpImageViewBaseOnColor(this.mCbRepeat, this.mContext.getResources().getColor(R.color.colorAccent), (int) R.drawable.ic_repeat_one_white_36dp, false);
            } else if (type == 2) {
                this.mContext.setUpImageViewBaseOnColor(this.mCbRepeat, this.mContext.getResources().getColor(R.color.colorAccent), (int) R.drawable.ic_repeat_white_36dp, false);
            }
        }
    }

    public void setUpInfo(ArrayList<TrackModel> mListSongs) {
        if (this.mListSongs != null) {
            this.mListSongs.clear();
            this.mListSongs = null;
        }
        this.mCurrentTrackObject = MusicDataMng.getInstance().getCurrentTrackObject();
        if (mListSongs != null && mListSongs.size() != 0 && this.mCurrentTrackObject != null) {
            this.mListSongs = (ArrayList) mListSongs.clone();
            updateInformation();
        }
    }

    public void updateInformation() {
        TrackModel mCurrentTrackObject = MusicDataMng.getInstance().getCurrentTrackObject();
        if (mCurrentTrackObject != null) {
            this.mTvSong.setText(String.format(getString(R.string.format_current_song), new Object[]{mCurrentTrackObject.getTitle()}));
            String artist = mCurrentTrackObject.getAuthor();
            if (StringUtils.isEmptyString(artist) || artist.equalsIgnoreCase(IXMusicConstants.PREFIX_UNKNOWN)) {
                this.mTvSinger.setText(String.format(getString(R.string.format_current_singer), new Object[]{this.mContext.getString(R.string.title_unknown)}));
            } else {
                this.mTvSinger.setText(String.format(getString(R.string.format_current_singer), new Object[]{mCurrentTrackObject.getAuthor()}));
            }
            String artworkUrl = mCurrentTrackObject.getArtworkUrl();
            if (TextUtils.isEmpty(artworkUrl)) {
                Uri mUri = mCurrentTrackObject.getURI();
                Log.d("uri",String.valueOf(mUri));
                if (mUri != null) {
                    GlideImageLoader.displayImageFromMediaStore(this.mContext, this.mImgTrack, mUri, R.drawable.ic_disk);
                    return;
                } else {
                    this.mImgTrack.setImageResource(R.drawable.ic_disk);
                    return;
                }
            }
            GlideImageLoader.displayImage(this.mContext, this.mImgTrack, artworkUrl, (int) R.drawable.ic_disk);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.mEqualizer != null) {
            this.mEqualizer.stopBars();
        }
        if (this.mListSongs != null) {
            this.mListSongs.clear();
            this.mListSongs = null;
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fb_play:
                onActionPlay();
                return;
            case R.id.btn_next:
                this.mContext.startMusicService(IYPYMusicConstant.ACTION_NEXT);
                return;
            case R.id.btn_prev:
                this.mContext.startMusicService(IYPYMusicConstant.ACTION_PREVIOUS);
                return;
            case R.id.cb_repeat:
                int repeat = YPYSettingManager.getNewRepeat(this.mContext) + 1;
                if (repeat > 2) {
                    repeat = 0;
                }
                YPYSettingManager.setNewRepeat(this.mContext, repeat);
                updateTypeRepeat();
                return;
            case R.id.cb_shuffle:
                YPYSettingManager.setShuffle(this.mContext, !YPYSettingManager.getShuffle(this.mContext));
                updateTypeShuffle();
                return;
            case R.id.btn_close:
                this.mContext.collapseListenMusic();
                return;
            case R.id.img_sleep_mode:
                this.mContext.showDialogSleepMode();
                return;
            case R.id.img_equalizer:
                this.mContext.goToEqualizer();
                return;
            case R.id.img_add_playlist:
                TrackModel mCurrentTrack = MusicDataMng.getInstance().getCurrentTrackObject();
                if (mCurrentTrack != null) {
                    this.mContext.showDialogPlaylist(mCurrentTrack, new C14512());
                    return;
                }
                return;
            case R.id.img_share:
                TrackModel mCurrentTrack1 = MusicDataMng.getInstance().getCurrentTrackObject();
                if (mCurrentTrack1 != null) {
                    this.mContext.shareFile(mCurrentTrack1);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onActionPlay() {
        int size;
        ArrayList<TrackModel> mListMusics = MusicDataMng.getInstance().getListPlayingTrackObjects();
        if (mListMusics != null) {
            size = mListMusics.size();
        } else {
            size = 0;
        }
        if (size > 0 && MusicDataMng.getInstance().isPrepaireDone()) {
            this.mContext.startMusicService(IYPYMusicConstant.ACTION_TOGGLE_PLAYBACK);
        } else if (this.mListSongs != null && this.mListSongs.size() > 0) {
            MusicDataMng.getInstance().setListPlayingTrackObjects((ArrayList) this.mListSongs.clone());
            Iterator it = this.mListSongs.iterator();
            while (it.hasNext()) {
                TrackModel mTrackObject = (TrackModel) it.next();
                if (mTrackObject.getId() == this.mCurrentId) {
                    MusicDataMng.getInstance().setCurrentIndex(mTrackObject);
                    this.mContext.startMusicService(IYPYMusicConstant.ACTION_PLAY);
                    return;
                }
            }
            MusicDataMng.getInstance().setCurrentIndex((TrackModel) this.mListSongs.get(0));
            this.mContext.startMusicService(IYPYMusicConstant.ACTION_PLAY);
        }
    }

    public void showLoading(boolean isShow) {
        int i = 0;
        if (this.mLayoutContent != null) {
            this.mLayoutContent.setVisibility(isShow ? View.INVISIBLE : View.VISIBLE);
            CircularProgressBar circularProgressBar = this.mProgressBar;
            if (!isShow) {
                i = 8;
            }
            circularProgressBar.setVisibility(i);
            this.mEqualizer.stopBars();
            if (isShow) {
                updateInformation();
            }
        }
    }

    public void onUpdatePos(long currentPos) {
        if (currentPos > 0 && this.mCurrentTrackObject != null && this.mTvCurrentTime != null) {
            this.mTvCurrentTime.setText(this.mContext.getStringDuration(currentPos / 1000));
            this.mSeekbar.setValue((int) ((((float) currentPos) / ((float) this.mCurrentTrackObject.getDuration())) * 100.0f));
        }
    }

    public void onPlayerStop() {
        if (this.mBtnPlay != null) {
            this.mBtnPlay.setText(Html.fromHtml(getString(R.string.icon_play)));
            this.mSeekbar.setValue(0);
            this.mTvCurrentTime.setText(this.mContext.getStringDuration(0));
            this.mTvDuration.setText(this.mContext.getStringDuration(0));
            this.mEqualizer.stopBars();
            setUpInfo(null);
        }
    }

    public void onPlayerUpdateState(boolean isPlay) {
        if (this.mBtnPlay != null) {
            this.mBtnPlay.setText(Html.fromHtml(getString(isPlay ? R.string.icon_pause : R.string.icon_play)));
            this.mCurrentTrackObject = MusicDataMng.getInstance().getCurrentTrackObject();
            if (this.mCurrentTrackObject != null) {
                this.mCurrentId = this.mCurrentTrackObject.getId();
                if (this.mCurrentTrackObject != null) {
                    this.mTvDuration.setText(this.mContext.getStringDuration(this.mCurrentTrackObject.getDuration() / 1000));
                }
            }
            if (isPlay) {
                this.mEqualizer.animateBars();
            } else {
                this.mEqualizer.stopBars();
            }
        }
    }

    public void setUpBackground() {
        try {
            RelativeLayout mLayoutBg = (RelativeLayout) this.mRootView.findViewById(R.id.layout_listen_bg);
            if (mLayoutBg != null) {
                this.mTarget = new GlideViewGroupTarget(this.mContext, mLayoutBg) {
                    protected void setResource(Bitmap resource) {
                        super.setResource(resource);
                    }
                };
                String imgBg = YPYSettingManager.getBackground(this.mContext);
                Log.e("DCM", "=============>getBackground=" + imgBg);
                if (TextUtils.isEmpty(imgBg)) {
                    mLayoutBg.setBackgroundColor(getResources().getColor(R.color.colorBackground));
                    return;
                }
                Glide.with((Fragment) this).load(Uri.parse(imgBg)).asBitmap().placeholder((int) R.drawable.default_bg_app).into(this.mTarget);            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
