package refree.gautam.com.musicplayer.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.android.gms.ads.AdRequest.Builder;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.NativeExpressAdView;
import java.util.ArrayList;

import refree.gautam.com.musicplayer.BaseActivity.FragmentActivity;
import refree.gautam.com.musicplayer.InterFace.IXMusicConstants;
import refree.gautam.com.musicplayer.Models.PlaylistModel;
import refree.gautam.com.musicplayer.R;
import refree.gautam.com.musicplayer.abtractclass.RecyclerViewAdapter;
import refree.gautam.com.musicplayer.imageloader.GlideImageLoader;
import refree.gautam.com.musicplayer.utils.FBAds;
import refree.gautam.com.musicplayer.view.MaterialIconView;

public class PlaylistDataAdapter extends RecyclerViewAdapter implements IXMusicConstants {
    public static final String TAG = PlaylistDataAdapter.class.getSimpleName();
    private LayoutInflater mInflater;
    private int mTypeUI;
    private Typeface mTypefaceBold;
    private Typeface mTypefaceLight;
    private OnPlaylistListener onPlaylistListener;

    public interface OnPlaylistListener {
        void onViewDetail(PlaylistModel playlistModel);

        void showPopUpMenu(View view, PlaylistModel playlistModel);
    }

    public class PlaylistHolder extends ViewHolder {
        public CardView mCardView;
        public MaterialIconView mImgMenu;
        public ImageView mImgPlaylist;
        public View mLayoutRoot;
        public TextView mTvNumberMusic;
        public TextView mTvPlaylistName;

        public PlaylistHolder(View convertView) {
            super(convertView);
            this.mTvPlaylistName = (TextView) convertView.findViewById(R.id.tv_playlist_name);
            this.mTvPlaylistName.setTypeface(PlaylistDataAdapter.this.mTypefaceBold);
            this.mTvNumberMusic = (TextView) convertView.findViewById(R.id.tv_number_music);
            this.mTvNumberMusic.setTypeface(PlaylistDataAdapter.this.mTypefaceLight);
            this.mCardView = (CardView) convertView.findViewById(R.id.card_view);
            this.mImgMenu = (MaterialIconView) convertView.findViewById(R.id.img_menu);
            this.mImgPlaylist = (ImageView) convertView.findViewById(R.id.img_playlist);
            this.mLayoutRoot = convertView.findViewById(R.id.layout_root);
        }
    }

    public class ViewNativeHolder extends ViewHolder {
        public RelativeLayout mRootLayoutAds;

        public ViewNativeHolder(View convertView) {
            super(convertView);
            this.mRootLayoutAds = (RelativeLayout) convertView.findViewById(R.id.layout_ad_root);
        }
    }

    public PlaylistDataAdapter(FragmentActivity mContext, ArrayList<PlaylistModel> mListObjects, Typeface mTypefaceBold, Typeface mTypefaceLight, View mHeaderView, int typeUI) {
        super(mContext, mListObjects, mHeaderView);
        this.mContext = mContext;
        this.mTypefaceBold = mTypefaceBold;
        this.mTypefaceLight = mTypefaceLight;
        this.mTypeUI = typeUI;
        this.mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setOnPlaylistListener(OnPlaylistListener onPlaylistListener) {
        this.onPlaylistListener = onPlaylistListener;
    }

    public void onBindNormalViewHolder(ViewHolder holder1, int position) {
        final PlaylistModel mPlaylistModel;
        if (holder1 instanceof PlaylistHolder) {
            String data;
            mPlaylistModel = (PlaylistModel) this.mListObjects.get(position);
            final PlaylistHolder mHolder = (PlaylistHolder) holder1;
            mHolder.mTvPlaylistName.setText(mPlaylistModel.getName());
            if (mPlaylistModel.getNumberVideo() <= 1) {
                data = String.format(this.mContext.getString(R.string.format_number_music), new Object[]{String.valueOf(5)});
            } else {
                data = String.format(this.mContext.getString(R.string.format_number_musics), new Object[]{String.valueOf(5)});
            }
            String artwork = mPlaylistModel.getArtwork();
            if (TextUtils.isEmpty(artwork)) {
                Uri mUri = mPlaylistModel.getURI();
                if (mUri != null) {
                    GlideImageLoader.displayImageFromMediaStore(this.mContext, mHolder.mImgPlaylist, mUri, R.drawable.ic_rect_music_default);
                } else {
                    mHolder.mImgPlaylist.setImageResource(R.drawable.ic_rect_music_default);
                }
            } else {
                GlideImageLoader.displayImage(this.mContext, mHolder.mImgPlaylist, artwork, (int) R.drawable.ic_rect_music_default);
            }
            mHolder.mTvNumberMusic.setText(data);
            if (mHolder.mCardView != null) {
                mHolder.mCardView.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        if (PlaylistDataAdapter.this.onPlaylistListener != null) {
                            PlaylistDataAdapter.this.onPlaylistListener.onViewDetail(mPlaylistModel);
                        }
                    }
                });
            } else {
                mHolder.mLayoutRoot.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        if (PlaylistDataAdapter.this.onPlaylistListener != null) {
                            PlaylistDataAdapter.this.onPlaylistListener.onViewDetail(mPlaylistModel);
                        }
                    }
                });
            }
            mHolder.mImgMenu.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    if (PlaylistDataAdapter.this.onPlaylistListener != null) {
                        PlaylistDataAdapter.this.onPlaylistListener.showPopUpMenu(mHolder.mImgMenu, mPlaylistModel);
                    }
                }
            });
        } else if (holder1 instanceof ViewNativeHolder) {
            mPlaylistModel = (PlaylistModel) this.mListObjects.get(position);
            ViewNativeHolder mHolder2 = (ViewNativeHolder) holder1;
            mHolder2.mRootLayoutAds.removeAllViews();
            NativeExpressAdView mAdView;
            if (mPlaylistModel.getNativeExpressAdView() == null) {
                mAdView = (NativeExpressAdView) LayoutInflater.from(this.mContext).inflate(R.layout.item_native, null);
                mAdView.setAdSize(AdSize.MEDIUM_RECTANGLE);
                mPlaylistModel.setNativeExpressAdView(mAdView);
                mAdView.loadAd(new Builder().addTestDevice(IXMusicConstants.AD_TEST_ID).build());
            } else {
                mAdView = mPlaylistModel.getNativeExpressAdView();
                if (mAdView.getParent() != null) {
                    ((ViewGroup) mAdView.getParent()).removeAllViews();
                }
            }
            mHolder2.mRootLayoutAds.addView(mPlaylistModel.getNativeExpressAdView());
        }
    }

    public ViewHolder onCreateNormalViewHolder(ViewGroup v, int viewType) {
        if (viewType == 1) {
            return new ViewNativeHolder(this.mInflater.inflate(R.layout.item_native_ads, v, false));
        }
        return new PlaylistHolder(this.mInflater.inflate(this.mTypeUI == 2 ? R.layout.item_grid_playlist : R.layout.item_list_playlist, v, false));
    }

    public int getItemViewType(int position) {
        if (position <= 0 || !((PlaylistModel) this.mListObjects.get(position - 1)).isNativeAds()) {
            return super.getItemViewType(position);
        }
        return 1;
    }
}
