package refree.gautam.com.musicplayer.Models;

import java.util.ArrayList;

public class TotalDataModel {
    private ArrayList<TrackModel> listTrackObjects;
    private ArrayList<PlaylistModel> playlistObjects;

    public ArrayList<TrackModel> getListTrackObjects() {
        return this.listTrackObjects;
    }

    public void setListTrackObjects(ArrayList<TrackModel> listTrackObjects) {
        this.listTrackObjects = listTrackObjects;
    }

    public ArrayList<PlaylistModel> getPlaylistObjects() {
        return this.playlistObjects;
    }

    public void setPlaylistObjects(ArrayList<PlaylistModel> playlistObjects) {
        this.playlistObjects = playlistObjects;
    }

    public void onDestroy() {
        try {
            if (this.listTrackObjects != null) {
                this.listTrackObjects.clear();
                this.listTrackObjects = null;
            }
            if (this.playlistObjects != null) {
                this.playlistObjects.clear();
                this.playlistObjects = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
