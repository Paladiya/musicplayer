package refree.gautam.com.musicplayer.DataManger;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import org.json.JSONObject;

import refree.gautam.com.musicplayer.InterFace.IXMusicConstants;
import refree.gautam.com.musicplayer.InterFace.IXmusicSoundCloudConstants;
import refree.gautam.com.musicplayer.Models.TrackModel;
import refree.gautam.com.musicplayer.utils.DBLog;
import refree.gautam.com.musicplayer.utils.DownloadUtils;
import refree.gautam.com.musicplayer.utils.StringUtils;

public class MusicNetUtils implements IXMusicConstants, IXmusicSoundCloudConstants {
    public static final String TAG = MusicNetUtils.class.getSimpleName();

    public static String getRedirectAppUrl(String urlOriginal) {
        URL url;
        MalformedURLException e;
        IOException e2;
        String str = null;
        if (!StringUtils.isEmptyString(urlOriginal)) {
            try {
                URL u = new URL(urlOriginal);
                try {
                    HttpURLConnection huc = (HttpURLConnection) u.openConnection();
                    huc.setInstanceFollowRedirects(false);
                    huc.setReadTimeout(5000);
                    boolean redirect = false;
                    int status = huc.getResponseCode();
                    if (status != 200 && (status == 302 || status == 301 || status == 303)) {
                        redirect = true;
                    }
                    if (redirect) {
                        str = huc.getHeaderField("Location");
                    } else {
                        url = u;
                    }
                } catch (MalformedURLException e3) {
                    e = e3;
                    url = u;
                    e.printStackTrace();
                    return str;
                } catch (IOException e4) {
                    e2 = e4;
                    url = u;
                    e2.printStackTrace();
                    return str;
                }
            } catch (MalformedURLException e5) {
                e = e5;
                e.printStackTrace();
                return str;
            }
        }
        return str;
    }

    public static ArrayList<TrackModel> getListTrackObjectsByGenre(String genre, int offset, int limit) {
        StringBuilder mStringBuilder = new StringBuilder();
        mStringBuilder.append(IXmusicSoundCloudConstants.URL_API);
        mStringBuilder.append(IXmusicSoundCloudConstants.METHOD_TRACKS);
        mStringBuilder.append(IXmusicSoundCloudConstants.JSON_PREFIX);
        mStringBuilder.append(String.format(IXmusicSoundCloudConstants.FORMAT_CLIENT_ID, new Object[]{IXMusicConstants.SOUND_CLOUD_CLIENT_ID}));
        mStringBuilder.append(String.format(IXmusicSoundCloudConstants.FILTER_GENRE, new Object[]{genre}));
        mStringBuilder.append(String.format("&offset=%1$s&limit=%2$s", new Object[]{String.valueOf(offset), String.valueOf(limit)}));
        String url = mStringBuilder.toString();
        DBLog.LOGD(TAG, "==============>getListTrackObjectsByGenre=" + url);
        InputStream data = DownloadUtils.download(url);
        if (data != null) {
            return JsonParsingUtils.parsingListTrackObjects(data);
        }
        return null;
    }

    public static ArrayList<TrackModel> getListTrackObjectsByQuery(String query, int offset, int limit) {
        StringBuilder mStringBuilder = new StringBuilder();
        mStringBuilder.append(IXmusicSoundCloudConstants.URL_API);
        mStringBuilder.append(IXmusicSoundCloudConstants.METHOD_TRACKS);
        mStringBuilder.append(IXmusicSoundCloudConstants.JSON_PREFIX);
        mStringBuilder.append(String.format(IXmusicSoundCloudConstants.FORMAT_CLIENT_ID, new Object[]{IXMusicConstants.SOUND_CLOUD_CLIENT_ID}));
        mStringBuilder.append(String.format(IXmusicSoundCloudConstants.FILTER_QUERY, new Object[]{query}));
        mStringBuilder.append(String.format("&offset=%1$s&limit=%2$s", new Object[]{String.valueOf(offset), String.valueOf(limit)}));
        String url = mStringBuilder.toString();
        DBLog.LOGD(TAG, "==============>getListTrackObjectsByQuery=" + url);
        InputStream data = DownloadUtils.download(url);
        if (data != null) {
            return JsonParsingUtils.parsingListTrackObjects(data);
        }
        return null;
    }

    public static String getLinkStreamFromSoundCloud(long id) {
        String manualUrl = String.format(IXmusicSoundCloudConstants.FORMAT_URL_SONG, new Object[]{String.valueOf(id), IXMusicConstants.SOUND_CLOUD_CLIENT_ID});
        String redirectUrl = getRedirectAppUrl(manualUrl);
        if (!StringUtils.isEmptyString(redirectUrl)) {
            return redirectUrl;
        }
        String dataServer = DownloadUtils.downloadString(manualUrl);
        String finalUrl = null;
        if (!StringUtils.isEmptyString(dataServer)) {
            try {
                finalUrl = new JSONObject(dataServer).getString("http_mp3_128_url");
            } catch (Exception e) {
                e.printStackTrace();
                finalUrl = manualUrl;
            }
        }
        return finalUrl;
    }

    public static ArrayList<TrackModel> getListHotTrackObjectsInGenre(String genre, int offset, int limit) {
        return getListHotTrackObjectsInGenre(genre, IXmusicSoundCloudConstants.KIND_TOP, offset, limit);
    }

    public static ArrayList<TrackModel> getListHotTrackObjectsInGenre(String genre, String kind, int offset, int limit) {
        StringBuilder mStringBuilder = new StringBuilder();
        mStringBuilder.append(IXmusicSoundCloudConstants.URL_API_V2);
        mStringBuilder.append(IXmusicSoundCloudConstants.METHOD_CHARTS);
        mStringBuilder.append(String.format(IXmusicSoundCloudConstants.PARAMS_KIND, new Object[]{kind}));
        mStringBuilder.append(String.format(IXmusicSoundCloudConstants.PARAMS_NEW_CLIENT_ID, new Object[]{IXMusicConstants.SOUND_CLOUD_CLIENT_ID}));
        mStringBuilder.append(String.format(IXmusicSoundCloudConstants.PARAMS_GENRES, new Object[]{genre}));
        mStringBuilder.append(String.format("&offset=%1$s&limit=%2$s", new Object[]{String.valueOf(offset), String.valueOf(limit)}));
        mStringBuilder.append(IXmusicSoundCloudConstants.PARAMS_LINKED_PARTITION);
        String url = mStringBuilder.toString();
        DBLog.LOGD(TAG, "==============>getListHotTrackObjectsInGenre=" + url);
        InputStream data = DownloadUtils.download(url);
        if (data != null) {
            return JsonParsingUtils.parsingListHotTrackObjects(data);
        }
        return null;
    }
}
