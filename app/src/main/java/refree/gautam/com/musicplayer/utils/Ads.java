package refree.gautam.com.musicplayer.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import refree.gautam.com.musicplayer.InterFace.IXMusicConstants;
import refree.gautam.com.musicplayer.R;

public class Ads {

    public static InterstitialAd mInterstitialAd;

    public static void LoadInterstitialAds(final Context context)
    {
        mInterstitialAd=new InterstitialAd(context);
        mInterstitialAd.setAdUnitId(IXMusicConstants.ADMOB_INTERSTITIAL_ID);

        AdRequest adRequest = new AdRequest.Builder()
                .build();

        // Load ads into Interstitial Ads
        mInterstitialAd.loadAd(adRequest);
        mInterstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                Log.d("interstialad code = ","Loaded");

//                showFbInterstitial(context);
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                Log.d("interstialad code = ",String.valueOf(i));
            }
        });
    }

    public static void showInterstitial(Context context) {

        if (mInterstitialAd == null)
        {
            LoadInterstitialAds(context);
            return;
        }

        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
            LoadInterstitialAds(context);
        }
    }

    public static AdView GetAdview(final Context context, int margin)
    {
        AdView adView=new AdView(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.topMargin = margin;
        adView.setLayoutParams(layoutParams);
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId(IXMusicConstants.ADMOB_BANNER_ID);
        AdRequest adRequest=new AdRequest.Builder()
                .addTestDevice(IXMusicConstants.AD_TEST_ID)
                .build();

        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
               DBLog.LOGD("fbadview","on ad loaded");
            }

            @Override
            public void onAdClosed() {
                DBLog.LOGD("fbadview","on ad closed");

                new android.os.Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        GetAdview(context,0);
                    }
                },1);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                DBLog.LOGD("fbadview","on ad fail loaded");

            }

            @Override
            public void onAdLeftApplication() {
                DBLog.LOGD("fbadview","on left app");
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
                DBLog.LOGD("fbadview","on ad open");
            }
        });

        return adView;
    }

}
