package refree.gautam.com.musicplayer.Models;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.Iterator;

public class ResultCollectionModel {
    @SerializedName("collection")
    private ArrayList<CollectionModel> listCollectionObjects;
    private transient ArrayList<TrackModel> listTrackObjects;

    public ArrayList<CollectionModel> getListCollectionObjects() {
        return this.listCollectionObjects;
    }

    public void setListCollectionObjects(ArrayList<CollectionModel> listCollectionObjects) {
        this.listCollectionObjects = listCollectionObjects;
    }

    public ArrayList<TrackModel> getListTrackObjects() {
        if (this.listCollectionObjects != null && this.listCollectionObjects.size() > 0 && (this.listTrackObjects == null || this.listTrackObjects.size() == 0)) {
            this.listTrackObjects = new ArrayList();
            Iterator it = this.listCollectionObjects.iterator();
            while (it.hasNext()) {
                CollectionModel mCollectionObject = (CollectionModel) it.next();
                if (mCollectionObject.getTrackObject() != null) {
                    this.listTrackObjects.add(mCollectionObject.getTrackObject());
                }
            }
        }
        return this.listTrackObjects;
    }
}
