package refree.gautam.com.musicplayer;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;

import com.facebook.ads.AdSettings;
import com.facebook.ads.AudienceNetworkAds;

import refree.gautam.com.musicplayer.InterFace.IXMusicConstants;
import refree.gautam.com.musicplayer.utils.FBAds;

public class MyApplication extends Application {

    String CHANNEL_ID = IXMusicConstants.NOTIFICATION_CHANNEL_ID;// The id of the channel.
    CharSequence name = IXMusicConstants.NOTIFICATION_CHANNEL_NAME;
    NotificationChannel mChannel;
    NotificationManager notificationManager;

    @Override
    public void onCreate() {
        super.onCreate();
        AudienceNetworkAds.initialize(this);
        AdSettings.addTestDevice(IXMusicConstants.AD_TEST_ID);
        FBAds.LoadFbInterstitialAd(this);
        FBAds.LoadGoogleInterstitialAds(this);
        createNotificationChannel();
    }

    private void createNotificationChannel() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            notificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
            mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            notificationManager.createNotificationChannel(mChannel);
        }
    }
}
