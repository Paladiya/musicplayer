package refree.gautam.com.musicplayer;

import android.os.Bundle;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import refree.gautam.com.musicplayer.BaseActivity.FragmentActivity;
import refree.gautam.com.musicplayer.InterFace.IYPYCallback;
import refree.gautam.com.musicplayer.InterFace.YPYSettingManager;
import refree.gautam.com.musicplayer.executor.DBExecutorSupplier;
import refree.gautam.com.musicplayer.utils.DBLog;
import refree.gautam.com.musicplayer.utils.DirectionUtils;
import refree.gautam.com.musicplayer.utils.IOUtils;
import refree.gautam.com.musicplayer.view.CircularProgressBar;

public class SplashActivity extends FragmentActivity {

    public static final int REQUEST_GOOGLE_PLAY_SERVICES = 1000;
    public static final String REQUEST_PERMISSION = "android.permission.WRITE_EXTERNAL_STORAGE";
    public static final int REQUEST_PERMISSION_CODE = 1001;
    public static final String TAG = SplashActivity.class.getSimpleName();
    private boolean isCheckGoogle;
    private boolean isLoading;
    private Handler mHandler = new Handler();
    private CircularProgressBar mProgressBar;

    class StartActivity implements IYPYCallback {

        public void onAction() {
            SplashActivity.this.isCheckGoogle = false;
            SplashActivity.this.startActivityForResult(new Intent("android.settings.MEMORY_CARD_SETTINGS"), 0);
        }
    }

    class OnFinish implements IYPYCallback {

        public void onAction() {
            SplashActivity.this.onDestroyData();
            SplashActivity.this.finish();
        }
    }

    class GoNext implements Runnable {

        class Start implements Runnable {

            public void run() {
                SplashActivity.this.goToMainActivity();
            }
        }


        public void run() {
            SplashActivity.this.mTotalMng.readConfigure(SplashActivity.this);
            SplashActivity.this.mTotalMng.readGenreData(SplashActivity.this);
            SplashActivity.this.mTotalMng.readCached(5);
            SplashActivity.this.mTotalMng.readPlaylistCached();
            SplashActivity.this.mTotalMng.readLibraryTrack(SplashActivity.this);
            SplashActivity.this.runOnUiThread(new Start());
        }
    }

    class   ChageActivity implements IYPYCallback {


        public void onAction() {
            SplashActivity.this.mProgressBar.setVisibility(View.INVISIBLE);
            DirectionUtils.changeActivity(SplashActivity.this, R.anim.slide_in_from_right, R.anim.slide_out_to_left, true, new Intent(SplashActivity.this, MainActivity.class));
        }
    }

    class RequestPermissoin implements IYPYCallback {


        public void onAction() {
            ActivityCompat.requestPermissions(SplashActivity.this, new String[]{SplashActivity.REQUEST_PERMISSION}, 1001);
        }
    }

    class Finish implements IYPYCallback {


        public void onAction() {
            SplashActivity.this.onDestroyData();
            SplashActivity.this.finish();
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_splash);
        this.mProgressBar = (CircularProgressBar) findViewById(R.id.progressBar1);
        ((TextView) findViewById(R.id.tv_loading)).setTypeface(this.mTypefaceNormal);
        YPYSettingManager.setOnline(this, true);
        DBLog.setDebug(false);
    }

    protected void onResume() {
        super.onResume();
        startInit();
    }

    private void startLoad() {
        if (this.mTotalMng.getDirectoryCached() == null) {
            createFullDialog(-1, R.string.title_info, R.string.title_settings, R.string.title_cancel, getString(R.string.info_error_sdcard), new StartActivity(), new OnFinish()).show();
            return;
        }
        this.mProgressBar.setVisibility(View.VISIBLE);
        if (!isNeedGrantPermission()) {
            startExecuteTask();
        }
    }

    private void startExecuteTask() {
        DBExecutorSupplier.getInstance().forBackgroundTasks().execute(new GoNext());
    }

    protected void onDestroy() {
        super.onDestroy();
        this.mHandler.removeCallbacksAndMessages(null);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void goToMainActivity() {
        showInterstitial(new ChageActivity());
    }

    private void startInit() {
        if (!this.isLoading) {
            this.isLoading = true;
//            this.mProgressBar.setVisibility(View.VISIBLE);
            startLoad();
        }
    }


    private boolean isNeedGrantPermission() {
        try {
            if (IOUtils.hasMarsallow() && ContextCompat.checkSelfPermission(this, REQUEST_PERMISSION) != 0) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, REQUEST_PERMISSION)) {
                    showFullDialog(R.string.title_confirm, String.format(getString(R.string.format_request_permision), new Object[]{getString(R.string.app_name)}), R.string.title_grant, R.string.title_cancel, new RequestPermissoin(), new Finish());
                } else {
                    ActivityCompat.requestPermissions(this, new String[]{REQUEST_PERMISSION}, 1001);
                }
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1001) {
            if (grantResults != null) {
                try {
                    if (grantResults.length > 0 && grantResults[0] == 0) {
                        startExecuteTask();
                        return;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    showToast((int) R.string.info_permission_denied);
                    onDestroyData();
                    finish();
                    return;
                }
            }
            showToast((int) R.string.info_permission_denied);
            onDestroyData();
            finish();
        }
    }
}
