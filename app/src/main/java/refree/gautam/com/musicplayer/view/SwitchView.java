package refree.gautam.com.musicplayer.view;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout.LayoutParams;

import refree.gautam.com.musicplayer.R;


public class SwitchView extends CustomView {
    int backgroundColor = Color.parseColor("#4CAF50");
    Ball ball;
    boolean check = false;
    private Paint mMainPaint;
    private Bitmap mTempBitmap;
    private Canvas mTempCanvas;
    private Paint mTransparentPaint;
    OnCheckListener onCheckListener;
    boolean placedBall = false;
    boolean press = false;

    public interface OnCheckListener {
        void onCheck(boolean z);
    }

    class C14851 implements OnClickListener {
        C14851() {
        }

        public void onClick(View arg0) {
            if (SwitchView.this.check) {
                SwitchView.this.setChecked(false);
            } else {
                SwitchView.this.setChecked(true);
            }
        }
    }

    class Ball extends View {
        float xCen;
        float xFin;
        float xIni;

        public Ball(Context context) {
            super(context);
            setBackgroundResource(R.drawable.background_switch_ball_uncheck);
        }

        public void changeBackground() {
            if (SwitchView.this.check) {
                setBackgroundResource(R.drawable.background_checkbox);
                ((GradientDrawable) ((LayerDrawable) getBackground()).findDrawableByLayerId(R.id.shape_bacground)).setColor(SwitchView.this.backgroundColor);
                return;
            }
            setBackgroundResource(R.drawable.background_switch_ball_uncheck);
        }

        public void animateCheck() {
            ObjectAnimator objectAnimator;
            changeBackground();
            if (SwitchView.this.check) {
                objectAnimator = ObjectAnimator.ofFloat(this, "x", new float[]{SwitchView.this.ball.xFin});
            } else {
                objectAnimator = ObjectAnimator.ofFloat(this, "x", new float[]{SwitchView.this.ball.xIni});
            }
            objectAnimator.setDuration(300);
            objectAnimator.start();
        }
    }

    public SwitchView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setAttributes(attrs);
        init();
        setOnClickListener(new C14851());
    }

    private void init() {
        this.mMainPaint = new Paint();
        this.mMainPaint.setAntiAlias(true);
        this.mMainPaint.setStrokeWidth((float) ViewUtils.dpToPx(2.0f, getResources()));
        this.mTransparentPaint = new Paint();
        this.mTransparentPaint.setAntiAlias(true);
        this.mTransparentPaint.setColor(getResources().getColor(17170445));
        this.mTransparentPaint.setXfermode(new PorterDuffXfermode(Mode.CLEAR));
    }

    protected void setAttributes(AttributeSet attrs) {
        setBackgroundResource(R.drawable.background_transparent);
        setMinimumHeight(ViewUtils.dpToPx(48.0f, getResources()));
        setMinimumWidth(ViewUtils.dpToPx(80.0f, getResources()));
        int bacgroundColor = attrs.getAttributeResourceValue("http://schemas.android.com/apk/res/android", "themes", -1);
        if (bacgroundColor != -1) {
            setBackgroundColor(getResources().getColor(bacgroundColor));
        } else {
            int background = attrs.getAttributeIntValue("http://schemas.android.com/apk/res/android", "themes", -1);
            if (background != -1) {
                setBackgroundColor(background);
            }
        }
        this.check = attrs.getAttributeBooleanValue("http://schemas.android.com/apk/res-auto", "check", false);
        this.ball = new Ball(getContext());
        LayoutParams params = new LayoutParams(ViewUtils.dpToPx(20.0f, getResources()), ViewUtils.dpToPx(20.0f, getResources()));
        params.addRule(15, -1);
        this.ball.setLayoutParams(params);
        addView(this.ball);
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (isEnabled()) {
            this.isLastTouch = true;
            if (event.getAction() == 0) {
                this.press = true;
            } else if (event.getAction() == 2) {
                float x = event.getX();
                if (x < this.ball.xIni) {
                    x = this.ball.xIni;
                }
                if (x > this.ball.xFin) {
                    x = this.ball.xFin;
                }
                if (x > this.ball.xCen) {
                    this.check = true;
                } else {
                    this.check = false;
                }
                this.ball.setX(x);
                this.ball.changeBackground();
                if (event.getX() <= ((float) getWidth()) && event.getX() >= 0.0f) {
                    this.isLastTouch = false;
                    this.press = false;
                }
            } else if (event.getAction() == 1 || event.getAction() == 3) {
                this.press = false;
                this.isLastTouch = false;
                if (this.onCheckListener != null) {
                    this.onCheckListener.onCheck(this.check);
                }
                if (event.getX() <= ((float) getWidth()) && event.getX() >= 0.0f) {
                    this.ball.animateCheck();
                }
            }
        }
        return true;
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.placedBall) {
            placeBall();
        }
        if (this.mTempCanvas == null) {
            this.mTempBitmap = Bitmap.createBitmap(canvas.getWidth(), canvas.getHeight(), Config.ARGB_8888);
            this.mTempCanvas = new Canvas(this.mTempBitmap);
        } else {
            if (this.mTempBitmap != null) {
                this.mTempBitmap.recycle();
                this.mTempBitmap = null;
            }
            this.mTempBitmap = Bitmap.createBitmap(canvas.getWidth(), canvas.getHeight(), Config.ARGB_8888);
            this.mTempCanvas.setBitmap(this.mTempBitmap);
        }
        this.mMainPaint.setColor(this.check ? this.backgroundColor : Color.parseColor("#B0B0B0"));
        this.mTempCanvas.drawLine((float) (getHeight() / 2), (float) (getHeight() / 2), (float) (getWidth() - (getHeight() / 2)), (float) (getHeight() / 2), this.mMainPaint);
        this.mTempCanvas.drawCircle(this.ball.getX() + ((float) (this.ball.getWidth() / 2)), this.ball.getY() + ((float) (this.ball.getHeight() / 2)), (float) (this.ball.getWidth() / 2), this.mTransparentPaint);
        canvas.drawBitmap(this.mTempBitmap, 0.0f, 0.0f, null);
        if (this.press) {
            this.mMainPaint.setColor(this.check ? makePressColor() : Color.parseColor("#446D6D6D"));
            canvas.drawCircle(this.ball.getX() + ((float) (this.ball.getWidth() / 2)), (float) (getHeight() / 2), (float) (getHeight() / 2), this.mMainPaint);
        }
        invalidate();
    }

    protected int makePressColor() {
        int r = (this.backgroundColor >> 16) & 255;
        int g = (this.backgroundColor >> 8) & 255;
        int b = (this.backgroundColor >> 0) & 255;
        return Color.argb(70, r + -30 < 0 ? 0 : r - 30, g + -30 < 0 ? 0 : g - 30, b + -30 < 0 ? 0 : b - 30);
    }

    private void placeBall() {
        this.ball.setX((float) ((getHeight() / 2) - (this.ball.getWidth() / 2)));
        this.ball.xIni = this.ball.getX();
        this.ball.xFin = (float) ((getWidth() - (getHeight() / 2)) - (this.ball.getWidth() / 2));
        this.ball.xCen = (float) ((getWidth() / 2) - (this.ball.getWidth() / 2));
        this.placedBall = true;
        this.ball.animateCheck();
    }

    public void setBackgroundColor(int color) {
        this.backgroundColor = color;
        if (isEnabled()) {
            this.beforeBackground = this.backgroundColor;
        }
        invalidate();
    }

    public void setChecked(boolean check) {
        this.check = check;
        invalidate();
        this.ball.animateCheck();
    }

    public boolean isCheck() {
        return this.check;
    }

    public void setOncheckListener(OnCheckListener onCheckListener) {
        this.onCheckListener = onCheckListener;
    }
}
