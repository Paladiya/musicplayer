package refree.gautam.com.musicplayer.utils;

import android.content.Context;
import android.net.ConnectivityManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DownloadUtils {
    private static final int BUFFER_SIZE = 8192;
    private static final int CONNECT_TIME_OUT = 15000;
    private static final int READ_TIME_OUT = 10000;
    private static final String TAG = DownloadUtils.class.getSimpleName();
    private static final Pattern URI_FILENAME_PATTERN = Pattern.compile("[^/]+$");
    private static Exception ioe22222,ioe2222;
    public static String getFileName(String mUrl) {
        Matcher mMatcher = URI_FILENAME_PATTERN.matcher(mUrl);
        if (mMatcher.find()) {
            return mMatcher.group();
        }
        throw new IllegalArgumentException("uri");
    }

    public static String downloadString(String url) {
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
            conn.setReadTimeout(READ_TIME_OUT);
            conn.setConnectTimeout(CONNECT_TIME_OUT);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            DBLog.LOGD(TAG, "The response is: " + response);
            if (response == 200) {
                return convertStreamToString(conn.getInputStream());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static InputStream download(String url) {
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
            conn.setReadTimeout(READ_TIME_OUT);
            conn.setConnectTimeout(CONNECT_TIME_OUT);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setUseCaches(false);
            conn.connect();
            int response = conn.getResponseCode();
            DBLog.LOGD(TAG, "The response is: " + response);
            if (response == 200) {
                return conn.getInputStream();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String convertStreamToString(InputStream is) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();
            while (true) {
                String line = reader.readLine();
                if (line != null) {
                    sb.append(line);
                } else {
                    is.close();
                    return sb.toString();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean downloadImg(String mRootDir, String nameFile, String url) throws IOException {
        IOException ioe;
        MalformedURLException mue;
        Throwable th;
        File mFile = new File(mRootDir, nameFile);
        if (mFile.exists() && mFile.isFile()) {
            mFile.delete();
        }
        InputStream is = null;
        try {
            URL u = new URL(url);
            URL url2;
            try {
                is = u.openStream();
                HttpURLConnection huc = (HttpURLConnection) u.openConnection();
                huc.setReadTimeout(READ_TIME_OUT);
                huc.setConnectTimeout(CONNECT_TIME_OUT);
                huc.setDoInput(true);
                huc.setUseCaches(false);
                huc.connect();
                int status = huc.getResponseCode();
                if (!(huc == null || is == null || status != 200)) {
                    int size = huc.getContentLength();
                    FileOutputStream fos = new FileOutputStream(mFile);
                    byte[] buffer = new byte[8192];
                    long total = 0;
                    while (true) {
                        int len1 = is.read(buffer);
                        if (len1 <= 0) {
                            break;
                        }
                        total += (long) len1;
                        fos.write(buffer, 0, len1);
                    }
                    if (fos != null) {
                        fos.close();
                    }
                    if (total >= ((long) size)) {
                        if (is != null) {
                            try {
                                is.close();
                            } catch (IOException ioe2) {
                                ioe2.printStackTrace();
                            }
                        }
                        url2 = u;
                        return true;
                    }
                }
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException ioe22) {
                        ioe22.printStackTrace();
                        url2 = u;
                    }
                }
                url2 = u;
            } catch (MalformedURLException e) {
                mue = e;
                url2 = u;
                try {
                    mue.printStackTrace();
                    if (is != null) {
                        try {
                            is.close();
                        } catch (IOException ioe222) {
                            ioe222.printStackTrace();
                        }
                    }
                    return false;
                } catch (Throwable th2) {
                    th = th2;
                    if (is != null) {
                        try {
                            is.close();
                        } catch (IOException ioe2222) {
                            ioe2222.printStackTrace();
                        }
                    }
                    try {
                        throw th;
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }
                }
            } catch (IOException e2) {
                ioe2222 = e2;
                url2 = u;
                ioe2222.printStackTrace();
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException ioe22222) {
                        ioe22222.printStackTrace();
                    }
                }
                return false;
            } catch (Throwable th3) {
                th = th3;
                url2 = u;
                if (is != null) {
                    is.close();
                }
                try {
                    throw th;
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        } catch (MalformedURLException e3) {
            mue = e3;
            mue.printStackTrace();
            if (is != null) {
                is.close();
            }
            return false;
        } catch (IOException e4) {
            ioe22222 = e4;
            ioe22222.printStackTrace();
            if (is != null) {
                is.close();
            }
            return false;
        }
        return false;
    }

    public static boolean isInternetOn(Context context) {

        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);

        // Check for network connections
        if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {


            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {


            return false;
        }
        return false;
    }
}
