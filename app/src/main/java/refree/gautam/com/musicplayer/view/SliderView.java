package refree.gautam.com.musicplayer.view;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import refree.gautam.com.musicplayer.R;
import refree.gautam.com.musicplayer.playservice.IYPYMusicConstant;

public class SliderView extends CustomView {
    Ball ball;
    int bgColor = Color.parseColor("#B0B0B0");
    private Paint mPaintActive;
    private Paint mPaintIndicator;
    private Paint mPaintNormal;
    private Bitmap mTempBitmap;
    private Canvas mTempCanvas;
    private Paint mTransparentPaint;
    int max = 100;
    int min = 0;
    NumberIndicator numberIndicator;
    OnValueChangedListener onValueChangedListener;
    boolean placedBall = false;
    boolean press = false;
    int processColor = Color.parseColor("#4CAF50");
    boolean showNumberIndicator = false;
    int value = 0;

    public interface OnValueChangedListener {
        void onValueChanged(int i);
    }

    class Ball extends View {
        float xCen;
        float xFin;
        float xIni;

        public Ball(Context context) {
            super(context);
            setBackgroundResource(R.drawable.background_switch_ball_uncheck);
        }

        public void changeBackground() {
            if (SliderView.this.value != SliderView.this.min) {
                setBackgroundResource(R.drawable.background_checkbox);
                ((GradientDrawable) ((LayerDrawable) getBackground()).findDrawableByLayerId(R.id.shape_bacground)).setColor(SliderView.this.processColor);
                return;
            }
            setBackgroundResource(R.drawable.background_switch_ball_uncheck);
        }
    }

    class Indicator extends RelativeLayout {
        boolean animate = true;
        float finalSize = 0.0f;
        float finalY = 0.0f;
        boolean numberIndicatorResize = false;
        private Paint paint;
        float size = 0.0f;
        float f1035x = 0.0f;
        float f1036y = 0.0f;

        public Indicator(Context context) {
            super(context);
            setBackgroundColor(getResources().getColor(R.color.colorAccent));
            this.paint = new Paint();
            this.paint.setAntiAlias(true);
            this.paint.setColor(SliderView.this.processColor);
        }

        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            if (!this.numberIndicatorResize) {
                LayoutParams params = (LayoutParams) SliderView.this.numberIndicator.numberIndicator.getLayoutParams();
                params.height = ((int) this.finalSize) * 2;
                params.width = ((int) this.finalSize) * 2;
                SliderView.this.numberIndicator.numberIndicator.setLayoutParams(params);
            }
            if (this.animate) {
                if (this.f1036y == 0.0f) {
                    this.f1036y = this.finalY + (this.finalSize * 2.0f);
                }
                this.f1036y -= (float) ViewUtils.dpToPx(6.0f, getResources());
                this.size += (float) ViewUtils.dpToPx(2.0f, getResources());
            }
            canvas.drawCircle((((float) ViewUtils.getRelativeLeft((View) SliderView.this.ball.getParent())) + SliderView.this.ball.getX()) + ((float) (SliderView.this.ball.getWidth() / 2)), this.f1036y, this.size, this.paint);
            if (this.animate && this.size >= this.finalSize) {
                this.animate = false;
            }
            if (!this.animate) {
                SliderView.this.numberIndicator.numberIndicator.setX(((((float) ViewUtils.getRelativeLeft((View) SliderView.this.ball.getParent())) + SliderView.this.ball.getX()) + ((float) (SliderView.this.ball.getWidth() / 2))) - this.size);
                SliderView.this.numberIndicator.numberIndicator.setY(this.f1036y - this.size);
                SliderView.this.numberIndicator.numberIndicator.setText(SliderView.this.value + "");
            }
            invalidate();
        }
    }

    class NumberIndicator extends Dialog {
        Indicator indicator;
        TextView numberIndicator;

        public NumberIndicator(Context context) {
            super(context, 16973839);
        }

        protected void onCreate(Bundle savedInstanceState) {
            requestWindowFeature(1);
            super.onCreate(savedInstanceState);
            setContentView(R.layout.number_indicator_spinner);
            setCanceledOnTouchOutside(false);
            RelativeLayout content = (RelativeLayout) findViewById(R.id.number_indicator_spinner_content);
            this.indicator = new Indicator(getContext());
            content.addView(this.indicator);
            this.numberIndicator = new TextView(getContext());
            this.numberIndicator.setTextColor(-1);
            this.numberIndicator.setGravity(17);
            content.addView(this.numberIndicator);
            this.indicator.setLayoutParams(new LayoutParams(-1, -1));
        }

        public void dismiss() {
            super.dismiss();
            this.indicator.f1036y = 0.0f;
            this.indicator.size = 0.0f;
            this.indicator.animate = true;
        }

        public void onBackPressed() {
        }
    }

    public SliderView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setAttributes(attrs);
        initBall();
        init();
    }

    public SliderView(Context context) {
        super(context);
        init();
        initBall();
    }

    private void init() {
        this.mPaintIndicator = new Paint();
        this.mPaintIndicator.setColor(this.processColor);
        this.mPaintIndicator.setAntiAlias(true);
        this.mPaintNormal = new Paint();
        this.mPaintNormal.setColor(this.bgColor);
        this.mPaintNormal.setStrokeWidth((float) ViewUtils.dpToPx(2.0f, getResources()));
        this.mPaintActive = new Paint();
        this.mPaintActive.setColor(this.processColor);
        this.mPaintActive.setStrokeWidth((float) ViewUtils.dpToPx(2.0f, getResources()));
        this.mTransparentPaint = new Paint();
        this.mTransparentPaint.setColor(getResources().getColor(R.color.colorAccent));
        this.mTransparentPaint.setXfermode(new PorterDuffXfermode(Mode.CLEAR));
        setBackgroundResource(R.drawable.background_transparent);
        setMinimumHeight(ViewUtils.dpToPx(48.0f, getResources()));
        setMinimumWidth(ViewUtils.dpToPx(80.0f, getResources()));
    }

    protected void setAttributes(AttributeSet attrs) {
        int bacgroundColor = attrs.getAttributeResourceValue("http://schemas.android.com/apk/res/android", "themes", -1);
        if (bacgroundColor != -1) {
            setBackgroundColor(getResources().getColor(bacgroundColor));
        } else {
            int background = attrs.getAttributeIntValue("http://schemas.android.com/apk/res/android", "themes", -1);
            if (background != -1) {
                setBackgroundColor(background);
            }
        }
        this.showNumberIndicator = attrs.getAttributeBooleanValue("http://schemas.android.com/apk/res-auto", "showNumberIndicator", false);
        this.min = attrs.getAttributeIntValue("http://schemas.android.com/apk/res-auto", "min", 0);
        this.max = attrs.getAttributeIntValue("http://schemas.android.com/apk/res-auto", "max", 0);
        this.value = attrs.getAttributeIntValue("http://schemas.android.com/apk/res-auto", IYPYMusicConstant.KEY_VALUE, this.min);
    }

    private void initBall() {
        this.ball = new Ball(getContext());
        LayoutParams params = new LayoutParams(ViewUtils.dpToPx(20.0f, getResources()), ViewUtils.dpToPx(20.0f, getResources()));
        params.addRule(15, -1);
        this.ball.setLayoutParams(params);
        addView(this.ball);
        if (this.showNumberIndicator) {
            this.numberIndicator = new NumberIndicator(getContext());
        }
    }

    public void invalidate() {
        if (this.ball != null) {
            this.ball.invalidate();
        }
        super.invalidate();
    }

    public void setOnReCalculate(boolean b) {
        this.placedBall = !b;
        invalidate();
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.placedBall) {
            placeBall();
        }
        if (this.value == this.min) {
            if (this.mTempBitmap == null) {
                this.mTempBitmap = Bitmap.createBitmap(canvas.getWidth(), canvas.getHeight(), Config.ARGB_8888);
                this.mTempCanvas = new Canvas(this.mTempBitmap);
            } else {
                if (this.mTempBitmap != null) {
                    this.mTempBitmap.recycle();
                    this.mTempBitmap = null;
                }
                this.mTempBitmap = Bitmap.createBitmap(canvas.getWidth(), canvas.getHeight(), Config.ARGB_8888);
                this.mTempCanvas.setBitmap(this.mTempBitmap);
            }
            this.mTempCanvas.drawLine((float) (getHeight() / 2), (float) (getHeight() / 2), (float) (getWidth() - (getHeight() / 2)), (float) (getHeight() / 2), this.mPaintNormal);
            this.mTempCanvas.drawCircle(this.ball.getX() + ((float) (this.ball.getWidth() / 2)), this.ball.getY() + ((float) (this.ball.getHeight() / 2)), (float) (this.ball.getWidth() / 2), this.mTransparentPaint);
            canvas.drawBitmap(this.mTempBitmap, 0.0f, 0.0f, null);
        } else {
            canvas.drawLine((float) (getHeight() / 2), (float) (getHeight() / 2), (float) (getWidth() - (getHeight() / 2)), (float) (getHeight() / 2), this.mPaintNormal);
            Canvas canvas2 = canvas;
            canvas2.drawLine((float) (getHeight() / 2), (float) (getHeight() / 2), ((float) (getHeight() / 2)) + (((float) (this.value - this.min)) * ((this.ball.xFin - this.ball.xIni) / ((float) (this.max - this.min)))), (float) (getHeight() / 2), this.mPaintActive);
        }
        if (this.press && !this.showNumberIndicator) {
            canvas.drawCircle(this.ball.getX() + ((float) (this.ball.getWidth() / 2)), (float) (getHeight() / 2), (float) (getHeight() / 3), this.mPaintIndicator);
        }
        invalidate();
    }

    public boolean onTouchEvent(MotionEvent event) {
        this.isLastTouch = true;
        if (isEnabled()) {
            if (event.getAction() == 0 || event.getAction() == 2) {
                if (!(this.numberIndicator == null || this.numberIndicator.isShowing())) {
                    this.numberIndicator.show();
                }
                if (event.getX() > ((float) getWidth()) || event.getX() < 0.0f) {
                    this.press = false;
                    this.isLastTouch = false;
                    if (this.numberIndicator != null) {
                        this.numberIndicator.dismiss();
                    }
                } else {
                    int newValue;
                    this.press = true;
                    float division = (this.ball.xFin - this.ball.xIni) / ((float) (this.max - this.min));
                    if (event.getX() > this.ball.xFin) {
                        newValue = this.max;
                    } else if (event.getX() < this.ball.xIni) {
                        newValue = this.min;
                    } else {
                        newValue = this.min + ((int) ((event.getX() - this.ball.xIni) / division));
                    }
                    if (this.value != newValue) {
                        this.value = newValue;
                        if (this.onValueChangedListener != null) {
                            this.onValueChangedListener.onValueChanged(newValue);
                        }
                    }
                    float x = event.getX();
                    if (x < this.ball.xIni) {
                        x = this.ball.xIni;
                    }
                    if (x > this.ball.xFin) {
                        x = this.ball.xFin;
                    }
                    this.ball.setX(x);
                    this.ball.changeBackground();
                    if (this.numberIndicator != null) {
                        this.numberIndicator.indicator.f1035x = x;
                        this.numberIndicator.indicator.finalY = (float) (ViewUtils.getRelativeTop(this) - (getHeight() / 2));
                        this.numberIndicator.indicator.finalSize = (float) (getHeight() / 2);
                        this.numberIndicator.numberIndicator.setText("");
                    }
                }
            } else if (event.getAction() == 1 || event.getAction() == 3) {
                if (this.numberIndicator != null) {
                    this.numberIndicator.dismiss();
                }
                this.isLastTouch = false;
                this.press = false;
            }
        }
        return true;
    }

    protected int makePressColor() {
        int r = (this.processColor >> 16) & 255;
        int g = (this.processColor >> 8) & 255;
        int b = (this.processColor >> 0) & 255;
        return Color.argb(70, r + -30 < 0 ? 0 : r - 30, g + -30 < 0 ? 0 : g - 30, b + -30 < 0 ? 0 : b - 30);
    }

    private void placeBall() {
        this.ball.setX((float) ((getHeight() / 2) - (this.ball.getWidth() / 2)));
        this.ball.xIni = this.ball.getX();
        this.ball.xFin = (float) ((getWidth() - (getHeight() / 2)) - (this.ball.getWidth() / 2));
        this.ball.xCen = (float) ((getWidth() / 2) - (this.ball.getWidth() / 2));
        this.placedBall = true;
    }

    public OnValueChangedListener getOnValueChangedListener() {
        return this.onValueChangedListener;
    }

    public void setOnValueChangedListener(OnValueChangedListener onValueChangedListener) {
        this.onValueChangedListener = onValueChangedListener;
    }

    public int getValue() {
        return this.value;
    }

    public void setValue(final int value) {
        if (this.placedBall) {
            this.value = value;
            this.ball.setX(((((float) value) * ((this.ball.xFin - this.ball.xIni) / ((float) this.max))) + ((float) (getHeight() / 2))) - ((float) (this.ball.getWidth() / 2)));
            this.ball.changeBackground();
            return;
        }
        post(new Runnable() {
            public void run() {
                SliderView.this.setValue(value);
            }
        });
    }

    public int getMax() {
        return this.max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getMin() {
        return this.min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public boolean isShowNumberIndicator() {
        return this.showNumberIndicator;
    }

    public void setShowNumberIndicator(boolean showNumberIndicator) {
        this.showNumberIndicator = showNumberIndicator;
        this.numberIndicator = showNumberIndicator ? new NumberIndicator(getContext()) : null;
    }

    public void setProcessColor(int color) {
        this.processColor = color;
        if (this.mPaintActive != null) {
            this.mPaintActive.setColor(this.processColor);
        }
        if (this.mPaintIndicator != null) {
            this.mPaintIndicator.setColor(this.processColor);
        }
        if (isEnabled()) {
            this.beforeBackground = this.processColor;
        }
        invalidate();
    }

    public void setBackgroundColor(int color) {
        this.bgColor = color;
        if (this.mPaintNormal != null) {
            this.mPaintNormal.setColor(this.bgColor);
        }
        invalidate();
    }
}
