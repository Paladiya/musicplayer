package refree.gautam.com.musicplayer.executor;

import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import refree.gautam.com.musicplayer.utils.DBLog;

public class DBExecutorSupplier {
    public static final int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();
    public static final String TAG = DBExecutorSupplier.class.getSimpleName();
    private static DBExecutorSupplier sInstance;
    private PriorityThreadFactory mBgThreadPiority = new PriorityThreadFactory(10);
    private ThreadPoolExecutor mForBackgroundTasks = new ThreadPoolExecutor(NUMBER_OF_CORES * 2, NUMBER_OF_CORES * 2, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), this.mBgThreadPiority);
    private ThreadPoolExecutor mForLightWeightBackgroundTasks = new ThreadPoolExecutor(NUMBER_OF_CORES * 2, NUMBER_OF_CORES * 2, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), this.mBgThreadPiority);
    private MainThreadExecutor mMainThreadExecutor = new MainThreadExecutor();

    public static DBExecutorSupplier getInstance() {
        if (sInstance == null) {
            synchronized (DBExecutorSupplier.class) {
                sInstance = new DBExecutorSupplier();
            }
        }
        return sInstance;
    }

    private DBExecutorSupplier() {
        DBLog.LOGD(TAG, "===================>number core=" + NUMBER_OF_CORES);
    }

    public ThreadPoolExecutor forBackgroundTasks() {
        return this.mForBackgroundTasks;
    }

    public ThreadPoolExecutor forLightWeightBackgroundTasks() {
        return this.mForLightWeightBackgroundTasks;
    }

    public Executor forMainThreadTasks() {
        return this.mMainThreadExecutor;
    }

    public void onDestroy() {
        try {
            if (this.mBgThreadPiority != null) {
                this.mBgThreadPiority.onDestroy();
                this.mBgThreadPiority = null;
            }
            if (this.mMainThreadExecutor != null) {
                this.mMainThreadExecutor.onDestroy();
                this.mMainThreadExecutor = null;
            }
            this.mForLightWeightBackgroundTasks = null;
            this.mForBackgroundTasks = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        sInstance = null;
    }
}
