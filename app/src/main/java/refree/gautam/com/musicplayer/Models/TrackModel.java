package refree.gautam.com.musicplayer.Models;

import android.content.ContentUris;
import android.content.Context;
import android.net.Uri;
import android.provider.MediaStore.Audio.Media;
import com.google.android.gms.ads.NativeExpressAdView;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import refree.gautam.com.musicplayer.DataManger.MusicNetUtils;
import refree.gautam.com.musicplayer.utils.ApplicationUtils;
import refree.gautam.com.musicplayer.utils.StringUtils;

public class TrackModel {
    @SerializedName("artwork_url")
    private String artworkUrl;
    private transient Date dateCreated;
    @SerializedName("duration")
    private long duration;
    @SerializedName("id")
    private long id;
    private transient boolean isNativeAds;
    private transient NativeExpressAdView nativeExpressAdView;
    @SerializedName("path")
    private String path;
    @SerializedName("permalink_url")
    private String permalinkUrl;
    @SerializedName("title")
    private String title;
    @SerializedName("user")
    private UserModel userObject;

    public TrackModel(boolean isNativeAds) {
        this.isNativeAds = isNativeAds;
    }

    public TrackModel(long id, long duration, String title, String artworkUrl) {
        this.id = id;
        this.duration = duration;
        this.title = title;
        this.artworkUrl = artworkUrl;
    }

    public TrackModel(String path, String title) {
        this.path = path;
        this.title = title;
    }

    public TrackModel() {

    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getDuration() {
        return this.duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtworkUrl() {
        if (StringUtils.isEmptyString(this.artworkUrl) && this.userObject != null) {
            this.artworkUrl = this.userObject.getAvatar();
        }
        if (!StringUtils.isEmptyString(this.artworkUrl) && this.artworkUrl.contains("large")) {
            this.artworkUrl = this.artworkUrl.replace("large", "crop");
        }
        return this.artworkUrl;
    }

    public void setArtworkUrl(String artworkUrl) {
        this.artworkUrl = artworkUrl;
        if (!StringUtils.isEmptyString(artworkUrl)) {
            this.artworkUrl = artworkUrl.replace("large", "crop");
        }
    }

    public TrackModel clone() {
        TrackModel mTrackObject;
        if (StringUtils.isEmptyString(this.path)) {
            mTrackObject = new TrackModel(this.id, this.duration, this.title, this.artworkUrl);
            if (this.userObject != null) {
                mTrackObject.setUserObject(this.userObject.cloneObject());
            }
            mTrackObject.setPermalinkUrl(this.permalinkUrl);
        } else {
            mTrackObject = new TrackModel(this.path, this.title);
            mTrackObject.setArtworkUrl(this.artworkUrl);
            if (this.userObject != null) {
                mTrackObject.setUserObject(this.userObject.cloneObject());
            }
            mTrackObject.setDuration(this.duration);
            mTrackObject.setDateCreated(this.dateCreated);
            mTrackObject.setId(this.id);
        }
        return mTrackObject;
    }

    public String getAuthor() {
        if (this.userObject != null) {
            return this.userObject.getUsername();
        }
        return null;
    }

    public String getPermalinkUrl() {
        return this.permalinkUrl;
    }

    public void setPermalinkUrl(String permalinkUrl) {
        this.permalinkUrl = permalinkUrl;
    }

    public Date getDateCreated() {
        return this.dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Uri getURI() {
        return ContentUris.withAppendedId(Media.EXTERNAL_CONTENT_URI, this.id);
    }

    public boolean isNativeAds() {
        return this.isNativeAds;
    }

    public void setNativeAds(boolean nativeAds) {
        this.isNativeAds = nativeAds;
    }

    public void setUserObject(UserModel userObject) {
        this.userObject = userObject;
    }

    public String startGetLinkStream(Context mContext) {
        if (!StringUtils.isEmptyString(this.path)) {
            return this.path;
        }
        if (ApplicationUtils.isOnline(mContext)) {
            return MusicNetUtils.getLinkStreamFromSoundCloud(this.id);
        }
        return null;
    }

    public NativeExpressAdView getNativeExpressAdView() {
        return this.nativeExpressAdView;
    }

    public void setNativeExpressAdView(NativeExpressAdView nativeExpressAdView) {
        this.nativeExpressAdView = nativeExpressAdView;
    }
}
