package refree.gautam.com.musicplayer.DataManger;

import android.text.TextUtils;
import android.util.JsonReader;
import com.bumptech.glide.load.Key;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;

import refree.gautam.com.musicplayer.InterFace.IXMusicConstants;
import refree.gautam.com.musicplayer.Models.ConfigureModel;
import refree.gautam.com.musicplayer.Models.GenreModel;
import refree.gautam.com.musicplayer.Models.PlaylistModel;
import refree.gautam.com.musicplayer.Models.ResultCollectionModel;
import refree.gautam.com.musicplayer.Models.TrackModel;
import refree.gautam.com.musicplayer.utils.DBLog;

public class JsonParsingUtils implements IXMusicConstants {
    public static final String TAG = JsonParsingUtils.class.getSimpleName();

    static class C14261 extends TypeToken<ArrayList<GenreModel>> {
        C14261() {
        }
    }

    static class C14272 extends TypeToken<ArrayList<PlaylistModel>> {
        C14272() {
        }
    }

    static class C14283 extends TypeToken<ArrayList<TrackModel>> {
        C14283() {
        }
    }

    public static ArrayList<GenreModel> parsingGenreObject(String data) {
        if (!TextUtils.isEmpty(data)) {
            try {
                return (ArrayList) new GsonBuilder().create().fromJson(data, new C14261().getType());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static ConfigureModel parsingConfigureModel(String data) {
        if (!TextUtils.isEmpty(data)) {
            try {
                return (ConfigureModel) new GsonBuilder().create().fromJson(data, ConfigureModel.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static ArrayList<PlaylistModel> parsingPlaylistObject(String mReader) {
        if (mReader != null) {
            try {
                ArrayList<PlaylistModel> arrayList = (ArrayList) new GsonBuilder().create().fromJson(mReader, new C14272().getType());
                if (arrayList == null || arrayList.size() <= 0) {
                    return arrayList;
                }
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    PlaylistModel mPlaylistObject = (PlaylistModel) it.next();
                    if (mPlaylistObject.getListTrackIds() == null) {
                        mPlaylistObject.setListTrackIds(new ArrayList());
                    }
                }
                return arrayList;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static ArrayList<TrackModel> parsingListTrackObjects(InputStream in) {
        if (in == null) {
            new Exception(TAG + " data can not null").printStackTrace();
            return null;
        }
        try {
            ArrayList<TrackModel> listVideo = (ArrayList) new GsonBuilder().create().fromJson(new InputStreamReader(in), new C14283().getType());
            if (in == null) {
                return listVideo;
            }
            try {
                in.close();
                return listVideo;
            } catch (IOException e) {
                e.printStackTrace();
                return listVideo;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            }
            return null;
        } catch (Throwable th) {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e32) {
                    e32.printStackTrace();
                }
            }
        }
        return null;
    }

    public static ArrayList<PlaylistModel> parsingListTopMusicObject(InputStream in) {
        if (in == null) {
            new Exception(TAG + " data can not null").printStackTrace();
            return null;
        }
        ArrayList<PlaylistModel> listTopObjects = new ArrayList();

        try {
            JsonReader reader = new JsonReader(new InputStreamReader(in, Key.STRING_CHARSET_NAME));
            reader.beginObject();
            while (reader.hasNext()) {
                if (reader.nextName().equals("feed")) {
                    reader.beginObject();
                    while (reader.hasNext()) {
                        if (reader.nextName().equals("entry")) {
                            reader.beginArray();
                            while (reader.hasNext()) {
                                reader.beginObject();
                                PlaylistModel mTrackObject = parsingTopMusicObject(reader);
                                if (mTrackObject != null) {
                                    listTopObjects.add(mTrackObject);
                                }
                                reader.endObject();
                            }
                            try {
                                reader.endArray();
                            } catch (Exception e) {
                                e.printStackTrace();
                                if (in != null) {
                                    try {
                                        in.close();
                                    } catch (IOException e2) {
                                        e2.printStackTrace();
                                    }
                                }
                                return null;
                            } catch (Throwable th) {
                                if (in != null) {
                                    try {
                                        in.close();
                                    } catch (IOException e22) {
                                        e22.printStackTrace();
                                    }
                                }
                            }
                        } else {
                            reader.skipValue();
                        }
                    }
                    reader.endObject();
                } else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            reader.close();
            DBLog.LOGD(TAG, "================>listTopObjects size=" + listTopObjects.size());
            if (in == null) {
                return listTopObjects;
            }
            in.close();
            return listTopObjects;
        }catch (IOException e222) {
            e222.printStackTrace();
            return listTopObjects;
        }
    }

    private static PlaylistModel parsingTopMusicObject(JsonReader reader) {
        IOException e;
        if (reader != null) {
            PlaylistModel mTopMusicObject = null;
            try {
                while (reader.hasNext()) {
                    PlaylistModel mTopMusicObject2;
                    try {
                        String name = reader.nextName();
                        if (name.equals("im:name")) {
                            mTopMusicObject2 = new PlaylistModel();
                            try {
                                reader.beginObject();
                                while (reader.hasNext()) {
                                    if (reader.nextName().equals("label")) {
                                        mTopMusicObject2.setName(reader.nextString());
                                    } else {
                                        reader.skipValue();
                                    }
                                }
                                reader.endObject();
                            } catch (IOException e2) {
                                e = e2;
                            }
                        } else if (name.equals("im:image")) {
                            reader.beginArray();
                            while (reader.hasNext()) {
                                reader.beginObject();
                                while (reader.hasNext()) {
                                    if (reader.nextName().equals("label")) {
                                        mTopMusicObject.setArtwork(reader.nextString());
                                    } else {
                                        reader.skipValue();
                                    }
                                }
                                reader.endObject();
                            }
                            reader.endArray();
                            mTopMusicObject2 = mTopMusicObject;
                        } else if (name.equals("im:artist")) {
                            reader.beginObject();
                            while (reader.hasNext()) {
                                if (reader.nextName().equals("label")) {
                                    mTopMusicObject.setArtist(reader.nextString());
                                } else {
                                    reader.skipValue();
                                }
                            }
                            reader.endObject();
                            mTopMusicObject2 = mTopMusicObject;
                        } else {
                            reader.skipValue();
                            mTopMusicObject2 = mTopMusicObject;
                        }
                        mTopMusicObject = mTopMusicObject2;
                    } catch (IOException e3) {
                        e = e3;
                        mTopMusicObject2 = mTopMusicObject;
                    }
                }
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return mTopMusicObject;
        }
        return null;
    }

    public static ArrayList<TrackModel> parsingListHotTrackObjects(InputStream in) {
        if (in == null) {
            new Exception(TAG + " data can not null").printStackTrace();
            return null;
        }
        try {
            ResultCollectionModel mResultCollections = (ResultCollectionModel) new GsonBuilder().create().fromJson(new InputStreamReader(in), ResultCollectionModel.class);
            if (mResultCollections != null) {
                ArrayList<TrackModel> mTrackObjects = mResultCollections.getListTrackObjects();
                DBLog.LOGD(TAG, "=========>parsingListHotTrackObjects=" + (mTrackObjects != null ? mTrackObjects.size() : 0));
                if (in == null) {
                    return mTrackObjects;
                }
                try {
                    in.close();
                    return mTrackObjects;
                } catch (IOException e) {
                    e.printStackTrace();
                    return mTrackObjects;
                }
            }
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
            return null;
        } catch (Exception e3) {
            e3.printStackTrace();
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e22) {
                    e22.printStackTrace();
                }
            }
        } catch (Throwable th) {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e222) {
                    e222.printStackTrace();
                }
            }
        }
        return null;
    }
}
