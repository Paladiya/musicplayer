package refree.gautam.com.musicplayer.utils;

import com.bumptech.glide.load.Key;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Locale;
import java.util.regex.Pattern;

public class StringUtils {
    public static final String REGEX_SPECIAL_CHARACTER = "[^a-zA-Z0-9_]";
    public static final String TAG = StringUtils.class.getSimpleName();

    public static String urlEncodeString(String data) {
        if (!(data == null || data.equals(""))) {
            try {
                return URLEncoder.encode(data, Key.STRING_CHARSET_NAME);
            } catch (UnsupportedEncodingException e) {
                DBLog.LOGD(TAG, "---------->encodeError=" + e.getMessage());
                e.printStackTrace();
            }
        }
        return data;
    }

    public static String urlDecodeString(String data) {
        if (!(data == null || data.equals(""))) {
            try {
                return URLDecoder.decode(data, Key.STRING_CHARSET_NAME);
            } catch (UnsupportedEncodingException e) {
                DBLog.LOGD(TAG, "---------->decodeError=" + e.getMessage());
                e.printStackTrace();
            }
        }
        return data;
    }

    public static String getSplitString(String mStrData, int maxLenght) {
        if (mStrData == null) {
            return null;
        }
        if (mStrData.length() > maxLenght) {
            return mStrData.substring(0, maxLenght) + "...";
        }
        return mStrData;
    }

    public static String formatHtmlBoldKeyword(String mStrOriginalData, String keyword) {
        if (!(mStrOriginalData == null || keyword == null || keyword.equals("") || !mStrOriginalData.contains(keyword))) {
            try {
                return mStrOriginalData.replace(keyword, "<b>" + keyword + "</b>");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mStrOriginalData;
    }

    public static boolean isNumber(String data) {
        return data.matches("[+-]?\\d*(\\.\\d+)?");
    }

    public static boolean isEmptyString(String mStr) {
        if (mStr == null || mStr.equals("")) {
            return true;
        }
        return false;
    }

    public static boolean isContainsSpecialCharacter(String mInput) {
        if (mInput == null || mInput.equals("") || !Pattern.compile(REGEX_SPECIAL_CHARACTER).matcher(mInput).find()) {
            return false;
        }
        return true;
    }

    public static float formatStringNumber(float number) {
        try {
            number = Float.parseFloat(String.format(Locale.US, "%.2f", new Object[]{Float.valueOf(number)}).replace(",", "."));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return number;
    }

    public static String formatVisualNumber(long numberValue, String demiter) {
        String value = String.valueOf(numberValue);
        if (value.length() > 3) {
            try {
                int number = (int) Math.floor((double) (value.length() / 3));
                int lenght = value.length();
                String total = "";
                for (int i = 0; i < number; i++) {
                    for (int j = 0; j < 3; j++) {
                        total = value.charAt((lenght - 1) - ((i * 3) + j)) + total;
                    }
                    if (i != number - 1) {
                        total = demiter + total;
                    } else if (lenght - (number * 3) > 0) {
                        total = demiter + total;
                    }
                }
                return value.substring(0, lenght - (number * 3)) + total;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return String.valueOf(value);
    }
}
