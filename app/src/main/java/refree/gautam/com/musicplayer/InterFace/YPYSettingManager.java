package refree.gautam.com.musicplayer.InterFace;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class YPYSettingManager implements IYPYSettingConstants {
    public static final String DOBAO_SHARPREFS = "ypyproductions_prefs";
    public static final String TAG = YPYSettingManager.class.getSimpleName();

    public static void saveSetting(Context mContext, String mKey, String mValue) {
        if (mContext != null) {
            try {
                SharedPreferences mSharedPreferences = mContext.getSharedPreferences(DOBAO_SHARPREFS, 0);
                if (mSharedPreferences != null) {
                    Editor editor = mSharedPreferences.edit();
                    editor.putString(mKey, mValue);
                    editor.commit();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static String getSetting(Context mContext, String mKey, String mDefValue) {
        if (mContext != null) {
            try {
                SharedPreferences mSharedPreferences = mContext.getSharedPreferences(DOBAO_SHARPREFS, 0);
                if (mSharedPreferences != null) {
                    mDefValue = mSharedPreferences.getString(mKey, mDefValue);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mDefValue;
    }

    public static boolean getOnline(Context mContext) {
        return Boolean.parseBoolean(getSetting(mContext, IYPYSettingConstants.KEY_ONLINE, "false"));
    }

    public static void setOnline(Context mContext, boolean mValue) {
        saveSetting(mContext, IYPYSettingConstants.KEY_ONLINE, String.valueOf(mValue));
    }

    public static void setShuffle(Context mContext, boolean mValue) {
        saveSetting(mContext, IYPYSettingConstants.KEY_SHUFFLE, String.valueOf(mValue));
    }

    public static boolean getShuffle(Context mContext) {
        return Boolean.parseBoolean(getSetting(mContext, IYPYSettingConstants.KEY_SHUFFLE, "true"));
    }

    public static void setRateApp(Context mContext, boolean mValue) {
        saveSetting(mContext, IYPYSettingConstants.KEY_RATE_APP, String.valueOf(mValue));
    }

    public static boolean getRateApp(Context mContext) {
        return Boolean.parseBoolean(getSetting(mContext, IYPYSettingConstants.KEY_RATE_APP, "false"));
    }

    public static boolean getEqualizer(Context mContext) {
        return Boolean.parseBoolean(getSetting(mContext, IYPYSettingConstants.KEY_EQUALIZER_ON, "false"));
    }

    public static void setEqualizer(Context mContext, boolean mValue) {
        saveSetting(mContext, IYPYSettingConstants.KEY_EQUALIZER_ON, String.valueOf(mValue));
    }

    public static String getEqualizerPreset(Context mContext) {
        return getSetting(mContext, IYPYSettingConstants.KEY_EQUALIZER_PRESET, "0");
    }

    public static void setEqualizerPreset(Context mContext, String mValue) {
        saveSetting(mContext, IYPYSettingConstants.KEY_EQUALIZER_PRESET, mValue);
    }

    public static String getEqualizerParams(Context mContext) {
        return getSetting(mContext, IYPYSettingConstants.KEY_EQUALIZER_PARAMS, "");
    }

    public static void setEqualizerParams(Context mContext, String mValue) {
        saveSetting(mContext, IYPYSettingConstants.KEY_EQUALIZER_PARAMS, mValue);
    }

    public static short getBassBoost(Context mContext) {
        return Short.parseShort(getSetting(mContext, IYPYSettingConstants.KEY_BASSBOOST, "0"));
    }

    public static void setBassBoost(Context mContext, short mValue) {
        saveSetting(mContext, IYPYSettingConstants.KEY_BASSBOOST, String.valueOf(mValue));
    }

    public static short getVirtualizer(Context mContext) {
        return Short.parseShort(getSetting(mContext, IYPYSettingConstants.KEY_VIRTUALIZER, "0"));
    }

    public static void setVirtualizer(Context mContext, short mValue) {
        saveSetting(mContext, IYPYSettingConstants.KEY_VIRTUALIZER, String.valueOf(mValue));
    }

    public static int getSleepMode(Context mContext) {
        return Integer.parseInt(getSetting(mContext, IYPYSettingConstants.KEY_TIME_SLEEP, "10"));
    }

    public static void setSleepMode(Context mContext, int mValue) {
        saveSetting(mContext, IYPYSettingConstants.KEY_TIME_SLEEP, String.valueOf(mValue));
    }

    public static void setNewRepeat(Context mContext, int mValue) {
        saveSetting(mContext, IYPYSettingConstants.KEY_REPEAT1, String.valueOf(mValue));
    }

    public static int getNewRepeat(Context mContext) {
        return Integer.parseInt(getSetting(mContext, IYPYSettingConstants.KEY_REPEAT1, "0"));
    }

    public static void setBackground(Context mContext, String mValue) {
        saveSetting(mContext, IYPYSettingConstants.KEY_BACKGROUND, mValue);
    }

    public static String getBackground(Context mContext) {
        return getSetting(mContext, IYPYSettingConstants.KEY_BACKGROUND, "");
    }
}
