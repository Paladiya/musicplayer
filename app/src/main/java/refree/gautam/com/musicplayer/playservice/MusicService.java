package refree.gautam.com.musicplayer.playservice;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.audiofx.BassBoost;
import android.media.audiofx.Equalizer;
import android.media.audiofx.Virtualizer;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaButtonReceiver;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.app.NotificationCompat;
//
//import android.support.v7.app.NotificationCompat.Builder;
//import android.support.v7.app.NotificationCompat.MediaStyle;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;

import refree.gautam.com.musicplayer.DataManger.MusicDataMng;
import refree.gautam.com.musicplayer.DataManger.TotalDataManager;
import refree.gautam.com.musicplayer.InterFace.IXMusicConstants;
import refree.gautam.com.musicplayer.InterFace.YPYSettingManager;
import refree.gautam.com.musicplayer.Models.TrackModel;
import refree.gautam.com.musicplayer.R;
import refree.gautam.com.musicplayer.MainActivity;
import refree.gautam.com.musicplayer.executor.DBExecutorSupplier;
import refree.gautam.com.musicplayer.imageloader.GlideImageLoader;
import refree.gautam.com.musicplayer.utils.DBLog;
import refree.gautam.com.musicplayer.utils.DownloadUtils;
import refree.gautam.com.musicplayer.utils.IOUtils;
import refree.gautam.com.musicplayer.utils.ImageProcessingUtils;
import refree.gautam.com.musicplayer.utils.StringUtils;

public class MusicService extends Service implements IYPYMusicConstant, IXMusicConstants, IMusicFocusableListener {
    public static final int NOTIFICATION_LOCK_ID = 512;
    public static final int STATE_ERROR = 5;
    public static final int STATE_PAUSE = 3;
    public static final int STATE_PLAYING = 2;
    public static final int STATE_PREPAIRING = 1;
    public static final int STATE_STOP = 4;
    public static final String TAG = MusicService.class.getSimpleName();
    private boolean isStartLoading;
    private AudioFocus mAudioFocus = AudioFocus.NO_FOCUS_NO_DUCK;
    private AudioFocusHelper mAudioFocusHelper = null;
    private AudioManager mAudioManager;
    private Bitmap mBitmapTrack;
    private int mCurrentState = 4;
    private TrackModel mCurrentTrack;
    private Handler mHandler = new Handler();
    private Handler mHandlerSleep = new Handler();
    private ComponentName mMediaButtonReceiverComponent;
    private MediaPlayer mMediaPlayer = null;
    private int mMinuteCount;
    private Notification mNotification = null;
    private NotificationManager mNotificationManager;
    private RemoteControlClientCompat mRemoteControlClientCompat;
    private MediaSessionCompat mSession;
    private Notification notificationLockScreen;
//    NotificationChannel mChannel;
//    int notifyID = 1;
//    String CHANNEL_ID = "my_channel_01";// The id of the channel.
//    CharSequence name = CHANNEL_ID;
//    int importance = NotificationManager.IMPORTANCE_HIGH;

    public void onCreate() {
        super.onCreate();
        this.mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        this.mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        this.mAudioFocusHelper = new AudioFocusHelper(getApplicationContext(), this);
        this.mMediaButtonReceiverComponent = new ComponentName(this, MusicIntentReceiver.class);
//        createNotificationChannel();
    }

//    private void createNotificationChannel() {
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
//            mNotificationManager.createNotificationChannel(mChannel);
//        }
//    }

    class C14671 implements Runnable {
        C14671() {
        }

        public void run() {
            MusicService.this.mMinuteCount = MusicService.this.mMinuteCount + NotificationManagerCompat.IMPORTANCE_UNSPECIFIED;
            if (MusicService.this.mMinuteCount <= 0) {
                try {
                    MusicService.this.processStopRequest(true);
                    if (!YPYSettingManager.getOnline(MusicService.this)) {
                        MusicDataMng.getInstance().onDestroy();
                        TotalDataManager.getInstance().onDestroy();
                        return;
                    }
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            }
            MusicService.this.startCountSleep();
        }
    }

    class C14692 implements Runnable {
        C14692() {
        }

        public void run() {
            if (StringUtils.isEmptyString(MusicService.this.mCurrentTrack.getArtworkUrl())) {
                MusicService.this.startGetLocalBitmap();
            } else {
                MusicService.this.startGetHttpBitmap();
            }
            MusicService.this.setUpNotification();
            final String uriStream = MusicService.this.mCurrentTrack.startGetLinkStream(MusicService.this);
            DBLog.LOGD(MusicService.TAG, "=========>uriStream=" + uriStream);
            DBExecutorSupplier.getInstance().forMainThreadTasks().execute(new Runnable() {
                public void run() {
                    MusicService.this.isStartLoading = false;
                    if (StringUtils.isEmptyString(uriStream)) {
                        MusicService.this.sendMusicBroadcast(IYPYMusicConstant.ACTION_DIMISS_LOADING);
                        MusicService.this.mCurrentState = 5;
                        MusicDataMng.getInstance().setLoading(false);
                        MusicService.this.processStopRequest(true);
                        return;
                    }
                    MusicService.this.setUpMediaForStream(uriStream);
                }
            });
        }
    }

    class C14703 implements OnPreparedListener {
        C14703() {
        }

        public void onPrepared(MediaPlayer mp) {
            MusicService.this.sendMusicBroadcast(IYPYMusicConstant.ACTION_DIMISS_LOADING);
            MusicDataMng.getInstance().setLoading(false);
            MusicService.this.mCurrentState = 2;
            MusicService.this.initAudioEffect();
            MusicService.this.configAndStartMediaPlayer();
            MusicService.this.setUpNotification();
        }
    }

    class C14714 implements OnCompletionListener {
        C14714() {
        }

        public void onCompletion(MediaPlayer mp) {
            MusicService.this.mCurrentState = 4;
            MusicService.this.processNextRequest(true);
            MusicService.this.sendMusicBroadcast(IYPYMusicConstant.ACTION_NEXT);
        }
    }

    class C14725 implements OnErrorListener {
        C14725() {
        }

        public boolean onError(MediaPlayer mp, int what, int extra) {
            try {
                MusicDataMng.getInstance().setLoading(false);
                DBLog.LOGE(MusicService.TAG, "Error: what=" + String.valueOf(what) + ", extra=" + String.valueOf(extra));
                MusicService.this.sendMusicBroadcast(IYPYMusicConstant.ACTION_DIMISS_LOADING);
                MusicService.this.mCurrentState = 5;
                MusicService.this.processStopRequest(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        }
    }

    class C14736 implements Runnable {
        C14736() {
        }

        public void run() {
            if (MusicService.this.mMediaPlayer != null && MusicService.this.mCurrentTrack != null) {
                try {
                    int current = MusicService.this.mMediaPlayer.getCurrentPosition();
                    MusicService.this.sendMusicBroadcast(IYPYMusicConstant.ACTION_UPDATE_POS, current);
                    if (((long) current) < MusicService.this.mCurrentTrack.getDuration()) {
                        MusicService.this.startUpdatePosition();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private enum AudioFocus {
        NO_FOCUS_NO_DUCK,
        NO_FOCUS_CAN_DUCK,
        FOCUSED
    }


    private void startSleepMode() {
        int minute = YPYSettingManager.getSleepMode(this);
        this.mHandlerSleep.removeCallbacksAndMessages(null);
        if (minute > 0) {
            this.mMinuteCount = 60000 * minute;
            startCountSleep();
        }
    }

    private void startCountSleep() {
        if (this.mMinuteCount > 0) {
            this.mHandlerSleep.postDelayed(new C14671(), 1000);
        }
    }

    @Nullable
    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        String packageName = IXMusicConstants.PREFIX_ACTION;
        String action = intent.getAction();
        if (!StringUtils.isEmptyString(action)) {
            if (action.equals(packageName + IYPYMusicConstant.ACTION_TOGGLE_PLAYBACK)) {
                processTogglePlaybackRequest();
            } else if (action.equals(packageName + IYPYMusicConstant.ACTION_PLAY)) {
                startSleepMode();
                processPlayRequest(true);
            } else if (action.equals(packageName + IYPYMusicConstant.ACTION_PAUSE)) {
                processPauseRequest();
            } else if (action.equals(packageName + IYPYMusicConstant.ACTION_NEXT)) {
                processNextRequest(false);
            } else if (action.equals(packageName + IYPYMusicConstant.ACTION_STOP)) {
                processStopRequest(true);
            } else if (action.equals(packageName + IYPYMusicConstant.ACTION_PREVIOUS)) {
                processPreviousRequest();
            } else if (action.equals(packageName + IYPYMusicConstant.ACTION_SEEK)) {
                processSeekBar(intent.getIntExtra(IYPYMusicConstant.KEY_VALUE, -1));
            } else if (action.equals(packageName + IYPYMusicConstant.ACTION_UPDATE_SLEEP_MODE)) {
                startSleepMode();
            } else if (action.equals(packageName + IYPYMusicConstant.ACTION_SHUFFLE)) {
                YPYSettingManager.setShuffle(this, intent.getBooleanExtra(IYPYMusicConstant.KEY_VALUE, false));
                sendMusicBroadcast(IYPYMusicConstant.ACTION_UPDATE_STATUS);
            } else if (action.equalsIgnoreCase(packageName + IYPYMusicConstant.ACTION_UPDATE_STATUS)) {
                updateNotification();
            }
        }
        return START_NOT_STICKY;
    }

    private void processPreviousRequest() {
        try {
            this.mCurrentTrack = MusicDataMng.getInstance().getPrevTrackObject(this);
            if (this.mCurrentTrack != null) {
                startPlayNewSong();
                return;
            }
            this.mCurrentState = 5;
            processStopRequest(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processTogglePlaybackRequest() {
        try {
            if (this.mCurrentState == 3 || this.mCurrentState == 4) {
                processPlayRequest(false);
            } else {
                processPauseRequest();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processPlayRequest(boolean isForces) {
        if (MusicDataMng.getInstance().getListPlayingTrackObjects() == null) {
            this.mCurrentState = 5;
            processStopRequest(true);
            return;
        }
        this.mCurrentTrack = MusicDataMng.getInstance().getCurrentTrackObject();
        if (this.mCurrentTrack == null) {
            this.mCurrentState = 5;
            processStopRequest(true);
            return;
        }
        if (this.mCurrentState == 4 || this.mCurrentState == 2 || isForces) {
            startPlayNewSong();
            sendMusicBroadcast(IYPYMusicConstant.ACTION_NEXT);
        } else if (this.mCurrentState == 3) {
            this.mCurrentState = 2;
            configAndStartMediaPlayer();
            setUpNotification();
        }
        if (this.mRemoteControlClientCompat != null && !IOUtils.hasLolipop()) {
            this.mRemoteControlClientCompat.setPlaybackState(3);
        }
    }

    private void setUpMediaSession() {
        if (IOUtils.hasLolipop()) {
            try {
                if (this.mSession != null) {
                    this.mSession.release();
                    this.mSession = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                ComponentName mEventReceiver = new ComponentName(getPackageName(), MediaButtonReceiver.class.getName());
                Intent mediaButtonIntent = new Intent("android.intent.action.MEDIA_BUTTON");
                mediaButtonIntent.setComponent(mEventReceiver);
                this.mSession = new MediaSessionCompat(this, TAG, mEventReceiver, PendingIntent.getBroadcast(this, 0, mediaButtonIntent, 0));
                this.mSession.setFlags(3);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        } else if (this.mCurrentTrack != null) {
            try {
                String artist = this.mCurrentTrack.getAuthor();
                if (StringUtils.isEmptyString(artist) || artist.equalsIgnoreCase(IXMusicConstants.PREFIX_UNKNOWN)) {
                    artist = getString(R.string.title_unknown);
                }
                MediaButtonHelper.registerMediaButtonEventReceiverCompat(this.mAudioManager, this.mMediaButtonReceiverComponent);
                if (this.mRemoteControlClientCompat == null) {
                    Intent intent = new Intent("android.intent.action.MEDIA_BUTTON");
                    intent.setComponent(this.mMediaButtonReceiverComponent);
                    this.mRemoteControlClientCompat = new RemoteControlClientCompat(PendingIntent.getBroadcast(this, 0, intent, 0));
                    RemoteControlHelper.registerRemoteControlClient(this.mAudioManager, this.mRemoteControlClientCompat);
                }
                this.mRemoteControlClientCompat.setPlaybackState(3);
                this.mRemoteControlClientCompat.setTransportControlFlags(181);
                RemoteControlClientCompat.MetadataEditorCompat mMediaData = this.mRemoteControlClientCompat.editMetadata(true);
                mMediaData.putString(2, artist).putString(3, artist).putString(7, this.mCurrentTrack.getTitle()).putLong(9, this.mCurrentTrack.getDuration());
                if (!(this.mBitmapTrack == null || this.mBitmapTrack.isRecycled())) {
                    mMediaData.putBitmap(100, this.mBitmapTrack);
                }
                mMediaData.apply();
            } catch (Exception e22) {
                e22.printStackTrace();
            }
        }
    }

    private synchronized void startPlayNewSong() {
        tryToGetAudioFocus();
        if (!this.isStartLoading) {
            this.mCurrentState = 4;
            this.isStartLoading = true;
            if (this.mCurrentTrack == null) {
                this.mCurrentState = 5;
                processStopRequest(true);
            } else {
                if (MusicDataMng.getInstance().isPlayingMusic()) {
                    releaseMedia();
                }
                startStreamMusic();
            }
        }
    }

    private synchronized void startStreamMusic() {
        if (this.mCurrentTrack != null) {
            try {
                if (!(this.mRemoteControlClientCompat == null || IOUtils.hasLolipop())) {
                    this.mRemoteControlClientCompat.setPlaybackState(1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            releaseMedia();
            onDestroyBitmap();
            setUpMediaSession();
            setUpNotification();
            sendMusicBroadcast(IYPYMusicConstant.ACTION_LOADING);
            MusicDataMng.getInstance().setLoading(true);
            DBExecutorSupplier.getInstance().forLightWeightBackgroundTasks().execute(new C14692());
        }
    }

    private boolean setUpMediaForStream(String path) {
        createMediaPlayer();
        try {
            if (this.mMediaPlayer != null) {
                this.mMediaPlayer.setDataSource(path);
                this.mCurrentState = 1;
                this.mMediaPlayer.prepareAsync();
                return true;
            }
        } catch (Exception ex) {
            Log.d(TAG, "IOException playing next song: " + ex.getMessage());
            ex.printStackTrace();
            processStopRequest(true);
        }
        return false;
    }

    private void startGetHttpBitmap() {
        String artWork = this.mCurrentTrack.getArtworkUrl();
        try {
            if (artWork.startsWith(GlideImageLoader.HTTP_PREFIX)) {
                try {
                    InputStream mInputStream = DownloadUtils.download(artWork);
                    if (mInputStream != null) {
                        this.mBitmapTrack = ImageProcessingUtils.decodePortraitBitmap(mInputStream, 100, 100);
                        mInputStream.close();
                        return;
                    }
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            }
            String path = artWork.replace("file://", "");
            File mFile = new File(path);
            if (mFile.exists() && mFile.isFile()) {
                Options mOptions = new Options();
                mOptions.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(path, mOptions);
                ImageProcessingUtils.calculateInSampleSize(mOptions, 100, 100);
                mOptions.inJustDecodeBounds = false;
                this.mBitmapTrack = BitmapFactory.decodeFile(path, mOptions);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void startGetLocalBitmap() {
        try {
            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            mmr.setDataSource(this, this.mCurrentTrack.getURI());
            byte[] rawArt = mmr.getEmbeddedPicture();
            ByteArrayInputStream mInputStream = null;
            if (rawArt == null || rawArt.length <= 0) {
                mmr.release();
            } else {
                mInputStream = new ByteArrayInputStream(rawArt);
                mmr.release();
            }
            if (mInputStream != null) {
                this.mBitmapTrack = ImageProcessingUtils.decodePortraitBitmap(mInputStream, 100, 100);
            } else {
                this.mBitmapTrack = BitmapFactory.decodeResource(getResources(), R.drawable.ic_rect_music_default);
            }
        } catch (Exception e) {
            e.printStackTrace();
            try {
                this.mBitmapTrack = BitmapFactory.decodeResource(getResources(), R.drawable.ic_rect_music_default);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }

    private void processPauseRequest() {
        if (this.mCurrentTrack == null || this.mMediaPlayer == null) {
            this.mCurrentState = 5;
            processStopRequest(true);
            return;
        }
        try {
            if (this.mCurrentState == 2) {
                this.mCurrentState = 3;
                this.mMediaPlayer.pause();
                setUpNotification();
                sendMusicBroadcast(IYPYMusicConstant.ACTION_PAUSE);
            }
        } catch (Exception e) {
            e.printStackTrace();
            processNextRequest(false);
        }
        if (this.mRemoteControlClientCompat != null && !IOUtils.hasLolipop()) {
            this.mRemoteControlClientCompat.setPlaybackState(2);
        }
    }

    private void processNextRequest(boolean isComplete) {
        try {
            this.mCurrentTrack = MusicDataMng.getInstance().getNextTrackObject(this, isComplete);
            if (this.mCurrentTrack != null) {
                startPlayNewSong();
                return;
            }
            this.mCurrentState = 5;
            processStopRequest(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processStopRequest(boolean isDestroyAll) {
        this.isStartLoading = false;
        try {
            releaseData(isDestroyAll);
            if (!(this.mRemoteControlClientCompat == null || IOUtils.hasLolipop())) {
                this.mRemoteControlClientCompat.setPlaybackState(1);
            }
            sendMusicBroadcast(IYPYMusicConstant.ACTION_STOP);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onDestroy() {
        super.onDestroy();
        releaseData(true);
        try {
            giveUpAudioFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.mNotification = null;
    }

    private void releaseData(boolean isDestroyAll) {
        this.mHandler.removeCallbacksAndMessages(null);
        onDestroyBitmap();
        this.mHandlerSleep.removeCallbacksAndMessages(null);
        releaseMedia();
        if (isDestroyAll) {
            try {
                stopForeground(true);
                MusicDataMng.getInstance().onDestroy();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (isDestroyAll) {
            try {
                if (this.mSession != null) {
                    this.mSession.release();
                    this.mSession = null;
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            try {
                if (this.notificationLockScreen != null) {
                    this.mNotificationManager.cancel(512);
                    this.notificationLockScreen = null;
                }
            } catch (Exception e22) {
                e22.printStackTrace();
            }
        }
    }

    private void onDestroyBitmap() {
        try {
            if (this.mBitmapTrack != null) {
                this.mBitmapTrack.recycle();
                this.mBitmapTrack = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void releaseMedia() {
        try {
            if (this.mMediaPlayer != null) {
                this.mMediaPlayer.reset();
                this.mMediaPlayer.release();
                MusicDataMng.getInstance().setPlayer(null);
                MusicDataMng.getInstance().releaseEffect();
                this.mMediaPlayer = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.mCurrentState = 4;
    }

    private void createMediaPlayer() {
        try {
            this.mMediaPlayer = new MediaPlayer();
            this.mMediaPlayer.setWakeMode(getApplicationContext(), 1);
            this.mMediaPlayer.setOnPreparedListener(new C14703());
            this.mMediaPlayer.setOnCompletionListener(new C14714());
            this.mMediaPlayer.setOnErrorListener(new C14725());
            this.mMediaPlayer.setAudioStreamType(3);
            MusicDataMng.getInstance().setPlayer(this.mMediaPlayer);
        } catch (Exception e) {
            e.printStackTrace();
            this.mCurrentState = 5;
            processStopRequest(true);
        }
    }

    public void onGainedAudioFocus() {
        try {
            this.mAudioFocus = AudioFocus.FOCUSED;
            if (this.mCurrentState == 2) {
                configAndStartMediaPlayer();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onLostAudioFocus(boolean canDuck) {
        AudioFocus audioFocus;
        if (canDuck) {
            try {
                audioFocus = AudioFocus.NO_FOCUS_CAN_DUCK;
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }
        audioFocus = AudioFocus.NO_FOCUS_NO_DUCK;
        this.mAudioFocus = audioFocus;
        if (this.mMediaPlayer != null && this.mMediaPlayer.isPlaying()) {
            configAndStartMediaPlayer();
        }
    }

    private void tryToGetAudioFocus() {
        try {
            if (this.mAudioFocus != null && this.mAudioFocus != AudioFocus.FOCUSED && this.mAudioFocusHelper != null && this.mAudioFocusHelper.requestFocus()) {
                this.mAudioFocus = AudioFocus.FOCUSED;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void giveUpAudioFocus() {
        try {
            if (this.mAudioFocus != null && this.mAudioFocus == AudioFocus.FOCUSED && this.mAudioFocusHelper != null && this.mAudioFocusHelper.abandonFocus()) {
                this.mAudioFocus = AudioFocus.NO_FOCUS_NO_DUCK;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void configAndStartMediaPlayer() {
        try {
            if (this.mMediaPlayer == null) {
                return;
            }
            if (this.mCurrentState != 2 && this.mCurrentState != 3) {
                return;
            }
            if (this.mAudioFocus != AudioFocus.NO_FOCUS_NO_DUCK) {
                if (this.mAudioFocus == AudioFocus.NO_FOCUS_CAN_DUCK) {
                    this.mMediaPlayer.setVolume(IYPYMusicConstant.DUCK_VOLUME, IYPYMusicConstant.DUCK_VOLUME);
                } else {
                    this.mMediaPlayer.setVolume(IYPYMusicConstant.MAX_VOLUME, IYPYMusicConstant.MAX_VOLUME);
                }
                if (!this.mMediaPlayer.isPlaying()) {
                    this.mMediaPlayer.start();
                    startUpdatePosition();
                    sendMusicBroadcast(IYPYMusicConstant.ACTION_PLAY);
                }
            } else if (this.mMediaPlayer.isPlaying()) {
                this.mMediaPlayer.pause();
                this.mHandler.removeCallbacksAndMessages(null);
                sendMusicBroadcast(IYPYMusicConstant.ACTION_PAUSE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startUpdatePosition() {
        this.mHandler.postDelayed(new C14736(), 1000);
    }

    public void sendMusicBroadcast(String action, int value) {
        try {
            Intent mIntent = new Intent("super.android.musiconline.stream.action.ACTION_BROADCAST_PLAYER");
            if (value != -1) {
                mIntent.putExtra(IYPYMusicConstant.KEY_VALUE, value);
            }
            mIntent.putExtra(IYPYMusicConstant.KEY_ACTION, IXMusicConstants.PREFIX_ACTION + action);
            sendBroadcast(mIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendMusicBroadcast(String action) {
        sendMusicBroadcast(action, -1);
    }

    private void updateNotification() {
        try {
            if (this.mNotification != null) {
                this.mNotificationManager.notify(1, this.mNotification);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUpNotification() {
        if (this.mCurrentTrack == null) {
            return;
        }
        if (this.mSession != null || !IOUtils.hasLolipop()) {
            try {
                if (!(!IOUtils.hasLolipop() || this.mSession == null || this.mSession.isActive())) {
                    this.mSession.setActive(true);
                }
                String packageName = getPackageName();
                Intent mIntent = new Intent(getApplicationContext(), MainActivity.class);
                mIntent.addFlags(268435456);
                PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 1, mIntent, 268435456);
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this,IXMusicConstants.NOTIFICATION_CHANNEL_ID);
                mBuilder.setVisibility(1);
                mBuilder.setSmallIcon(R.drawable.ic_notification_24dp);
                mBuilder.setColor(getResources().getColor(R.color.notification_color));
                mBuilder.setShowWhen(false);
                String artist = this.mCurrentTrack.getAuthor();
                if (StringUtils.isEmptyString(artist) || artist.equalsIgnoreCase(IXMusicConstants.PREFIX_UNKNOWN)) {
                    artist = getString(R.string.title_unknown);
                }
                if (IOUtils.hasLolipop()) {
                    this.mSession.setMetadata(new MediaMetadataCompat.Builder().putBitmap(MediaMetadataCompat.METADATA_KEY_ALBUM_ART, this.mBitmapTrack).putBitmap(MediaMetadataCompat.METADATA_KEY_DISPLAY_ICON, this.mBitmapTrack).putString(MediaMetadataCompat.METADATA_KEY_TITLE, this.mCurrentTrack.getTitle()).putString(MediaMetadataCompat.METADATA_KEY_ARTIST, artist).build());
                }
                Intent nextIntent = new Intent(this, MusicIntentReceiver.class);
                nextIntent.setAction(packageName + IYPYMusicConstant.ACTION_NEXT);
                PendingIntent pendingNextIntent = PendingIntent.getBroadcast(this, 100, nextIntent, 0);
                Intent stopIntent = new Intent(this, MusicIntentReceiver.class);
                stopIntent.setAction(packageName + IYPYMusicConstant.ACTION_STOP);
                PendingIntent stopPendingIntent = PendingIntent.getBroadcast(this, 100, stopIntent, 0);
                Intent intent = new Intent(this, MusicIntentReceiver.class);
                intent.setAction(packageName + IYPYMusicConstant.ACTION_TOGGLE_PLAYBACK);
                mBuilder.addAction(MusicDataMng.getInstance().isPlayingMusic() ? R.drawable.ic_pause_white_36dp : R.drawable.ic_play_arrow_white_36dp, "Pause", PendingIntent.getBroadcast(this, 100, intent, 0));
                mBuilder.addAction(R.drawable.ic_skip_next_white_36dp, "Next", pendingNextIntent);
                mBuilder.addAction(R.drawable.ic_close_white_36dp, "Close", stopPendingIntent);
                android.support.v4.media.app.NotificationCompat.MediaStyle mMediaStyle = new android.support.v4.media.app.NotificationCompat.MediaStyle();
                if (IOUtils.hasLolipop() && this.mSession != null) {
                    mMediaStyle.setMediaSession(this.mSession.getSessionToken());
                }
                int[] iArr = new int[3];
                mMediaStyle.setShowActionsInCompactView(0, 1, 2);
                mBuilder.setStyle(mMediaStyle);
                mBuilder.setContentTitle(getString(R.string.app_name));
                mBuilder.setContentText(this.mCurrentTrack.getTitle());
                mBuilder.setSubText(artist);
                if (this.mBitmapTrack != null) {
                    mBuilder.setLargeIcon(this.mBitmapTrack);
                }
                mBuilder.setPriority(0);
                this.mNotification = mBuilder.build();
                this.mNotification.contentIntent = pi;
                Notification notification = this.mNotification;
                notification.flags |= 32;
                startForeground(1, this.mNotification);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void processSeekBar(int currentPos) {
        try {
            if ((this.mCurrentState == 2 || this.mCurrentState == 3) && currentPos > 0 && this.mMediaPlayer != null) {
                this.mMediaPlayer.seekTo(currentPos);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initAudioEffect() {
        boolean b = YPYSettingManager.getEqualizer(this);
        try {
            Equalizer mEqualizer = new Equalizer(0, this.mMediaPlayer.getAudioSessionId());
            mEqualizer.setEnabled(b);
            setUpParams(mEqualizer);
            MusicDataMng.getInstance().setEqualizer(mEqualizer);
            setUpBassBoostVir();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUpBassBoostVir() {
        try {
            boolean b = YPYSettingManager.getEqualizer(this);
            BassBoost mBassBoost = new BassBoost(0, this.mMediaPlayer.getAudioSessionId());
            Virtualizer mVirtualizer = new Virtualizer(0, this.mMediaPlayer.getAudioSessionId());
            if (mBassBoost.getStrengthSupported() && mVirtualizer.getStrengthSupported()) {
                short bass = YPYSettingManager.getBassBoost(this);
                short vir = YPYSettingManager.getVirtualizer(this);
                mBassBoost.setEnabled(b);
                mVirtualizer.setEnabled(b);
                mBassBoost.setStrength((short) (bass * 10));
                mVirtualizer.setStrength((short) (vir * 10));
                MusicDataMng.getInstance().setBassBoost(mBassBoost);
                MusicDataMng.getInstance().setVirtualizer(mVirtualizer);
                return;
            }
            try {
                mBassBoost.release();
                mVirtualizer.release();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void setUpParams(Equalizer mEqualizer) {
        if (mEqualizer != null) {
            String presetStr = YPYSettingManager.getEqualizerPreset(this);
            if (!StringUtils.isEmptyString(presetStr) && StringUtils.isNumber(presetStr)) {
                short preset = Short.parseShort(presetStr);
                short numberPreset = mEqualizer.getNumberOfPresets();
                if (numberPreset > (short) 0 && preset < numberPreset - 1 && preset >= (short) 0) {
                    try {
                        mEqualizer.usePreset(preset);
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                }
            }
            setUpEqualizerCustom(mEqualizer);
        }
    }

    private void setUpEqualizerCustom(Equalizer mEqualizer) {
        if (mEqualizer != null) {
            String params = YPYSettingManager.getEqualizerParams(this);
            if (!StringUtils.isEmptyString(params)) {
                String[] mEqualizerParams = params.split(":");
                if (mEqualizerParams != null && mEqualizerParams.length > 0) {
                    int size = mEqualizerParams.length;
                    for (int i = 0; i < size; i++) {
                        mEqualizer.setBandLevel((short) i, Short.parseShort(mEqualizerParams[i]));
                    }
                    YPYSettingManager.setEqualizerPreset(this, String.valueOf(mEqualizer.getNumberOfPresets()));
                }
            }
        }
    }
}
