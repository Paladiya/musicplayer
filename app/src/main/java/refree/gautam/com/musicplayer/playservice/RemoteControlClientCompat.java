package refree.gautam.com.musicplayer.playservice;

import android.app.PendingIntent;
import android.graphics.Bitmap;
import android.os.Looper;
import android.util.Log;
import java.lang.reflect.Method;

public class RemoteControlClientCompat {
    private static final String TAG = RemoteControlClientCompat.class.getSimpleName();
    private static boolean sHasRemoteControlAPIs;
    private static Method sRCCEditMetadataMethod;
    private static Method sRCCSetPlayStateMethod;
    private static Method sRCCSetTransportControlFlags;
    private static Class sRemoteControlClientClass;
    private Object mActualRemoteControlClient;

    public class MetadataEditorCompat {
        public static final int METADATA_KEY_ARTWORK = 100;
        private Object mActualMetadataEditor;
        private Method mApplyMethod;
        private Method mClearMethod;
        private Method mPutBitmapMethod;
        private Method mPutLongMethod;
        private Method mPutStringMethod;

        private MetadataEditorCompat(Object actualMetadataEditor) {
            if (RemoteControlClientCompat.sHasRemoteControlAPIs && actualMetadataEditor == null) {
                throw new IllegalArgumentException("Remote Control API's exist, should not be given a null MetadataEditor");
            }
            if (RemoteControlClientCompat.sHasRemoteControlAPIs) {
                Class metadataEditorClass = actualMetadataEditor.getClass();
                try {
                    this.mPutStringMethod = metadataEditorClass.getMethod("putString", new Class[]{Integer.TYPE, String.class});
                    this.mPutBitmapMethod = metadataEditorClass.getMethod("putBitmap", new Class[]{Integer.TYPE, Bitmap.class});
                    this.mPutLongMethod = metadataEditorClass.getMethod("putLong", new Class[]{Integer.TYPE, Long.TYPE});
                    this.mClearMethod = metadataEditorClass.getMethod("clear", new Class[0]);
                    this.mApplyMethod = metadataEditorClass.getMethod("apply", new Class[0]);
                } catch (Exception e) {
                    throw new RuntimeException(e.getMessage(), e);
                }
            }
            this.mActualMetadataEditor = actualMetadataEditor;
        }

        public MetadataEditorCompat putString(int key, String value) {
            if (RemoteControlClientCompat.sHasRemoteControlAPIs) {
                try {
                    this.mPutStringMethod.invoke(this.mActualMetadataEditor, new Object[]{Integer.valueOf(key), value});
                } catch (Exception e) {
                    throw new RuntimeException(e.getMessage(), e);
                }
            }
            return this;
        }

        public MetadataEditorCompat putBitmap(int key, Bitmap bitmap) {
            if (RemoteControlClientCompat.sHasRemoteControlAPIs) {
                try {
                    this.mPutBitmapMethod.invoke(this.mActualMetadataEditor, new Object[]{Integer.valueOf(key), bitmap});
                } catch (Exception e) {
                    throw new RuntimeException(e.getMessage(), e);
                }
            }
            return this;
        }

        public MetadataEditorCompat putLong(int key, long value) {
            if (RemoteControlClientCompat.sHasRemoteControlAPIs) {
                try {
                    this.mPutLongMethod.invoke(this.mActualMetadataEditor, new Object[]{Integer.valueOf(key), Long.valueOf(value)});
                } catch (Exception e) {
                    throw new RuntimeException(e.getMessage(), e);
                }
            }
            return this;
        }

        public void clear() {
            if (RemoteControlClientCompat.sHasRemoteControlAPIs) {
                try {
                    this.mClearMethod.invoke(this.mActualMetadataEditor, (Object[]) null);
                } catch (Exception e) {
                    throw new RuntimeException(e.getMessage(), e);
                }
            }
        }

        public void apply() {
            if (RemoteControlClientCompat.sHasRemoteControlAPIs) {
                try {
                    this.mApplyMethod.invoke(this.mActualMetadataEditor, (Object[]) null);
                } catch (Exception e) {
                    throw new RuntimeException(e.getMessage(), e);
                }
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */


    public static Class getActualRemoteControlClientClass(ClassLoader classLoader) throws ClassNotFoundException {
        return classLoader.loadClass("android.media.RemoteControlClient");
    }

    public RemoteControlClientCompat(PendingIntent pendingIntent) {
        if (sHasRemoteControlAPIs) {
            try {
                this.mActualRemoteControlClient = sRemoteControlClientClass.getConstructor(new Class[]{PendingIntent.class}).newInstance(new Object[]{pendingIntent});
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public RemoteControlClientCompat(PendingIntent pendingIntent, Looper looper) {
        if (sHasRemoteControlAPIs) {
            try {
                this.mActualRemoteControlClient = sRemoteControlClientClass.getConstructor(new Class[]{PendingIntent.class, Looper.class}).newInstance(new Object[]{pendingIntent, looper});
            } catch (Exception e) {
                Log.e(TAG, "Error creating new instance of " + sRemoteControlClientClass.getName(), e);
            }
        }
    }

    public MetadataEditorCompat editMetadata(boolean startEmpty) {
        Object invoke;
        if (sHasRemoteControlAPIs) {
            try {
                invoke = sRCCEditMetadataMethod.invoke(this.mActualRemoteControlClient, new Object[]{Boolean.valueOf(startEmpty)});
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        invoke = null;
        return new MetadataEditorCompat(invoke);
    }

    public void setPlaybackState(int state) {
        if (sHasRemoteControlAPIs) {
            try {
                sRCCSetPlayStateMethod.invoke(this.mActualRemoteControlClient, new Object[]{Integer.valueOf(state)});
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void setTransportControlFlags(int transportControlFlags) {
        if (sHasRemoteControlAPIs) {
            try {
                sRCCSetTransportControlFlags.invoke(this.mActualRemoteControlClient, new Object[]{Integer.valueOf(transportControlFlags)});
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public final Object getActualRemoteControlClientObject() {
        return this.mActualRemoteControlClient;
    }
}
