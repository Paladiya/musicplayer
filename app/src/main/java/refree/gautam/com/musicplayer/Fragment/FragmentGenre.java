package refree.gautam.com.musicplayer.Fragment;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;

import refree.gautam.com.musicplayer.Adapter.GenreDataAdapter;
import refree.gautam.com.musicplayer.InterFace.IXMusicConstants;
import refree.gautam.com.musicplayer.Models.ConfigureModel;
import refree.gautam.com.musicplayer.Models.GenreModel;
import refree.gautam.com.musicplayer.R;
import refree.gautam.com.musicplayer.MainActivity;
import refree.gautam.com.musicplayer.abtractclass.fragment.DBFragment;
import refree.gautam.com.musicplayer.executor.DBExecutorSupplier;
import refree.gautam.com.musicplayer.view.CircularProgressBar;

public class FragmentGenre extends DBFragment implements IXMusicConstants {
    public static final String TAG = FragmentGenre.class.getSimpleName();
    private MainActivity mContext;
    private GenreDataAdapter mGenreDataAdapter;
    private ArrayList<GenreModel> mListGenres;
    private CircularProgressBar mProgressBar;
    private RecyclerView mRecyclerViewTrack;
    private TextView mTvNoResult;
    private int mTypeUI;

    class C14431 implements Runnable {
        C14431() {
        }

        public void run() {
            ArrayList<GenreModel> mList = FragmentGenre.this.mContext.mTotalMng.getListGenreObjects();
            if (mList == null) {
                FragmentGenre.this.mContext.mTotalMng.readGenreData(FragmentGenre.this.mContext);
                mList = FragmentGenre.this.mContext.mTotalMng.getListGenreObjects();
            }
            final ArrayList<GenreModel> finalMList = mList;
            FragmentGenre.this.mContext.runOnUiThread(new Runnable() {
                public void run() {
                    FragmentGenre.this.mProgressBar.setVisibility(View.GONE);
                    FragmentGenre.this.setUpInfo(finalMList);
                }
            });
        }
    }

    class C14442 implements GenreDataAdapter.OnGenreListener {
    C14442() {
        }

        public void goToDetail(GenreModel mGenreObject) {
            FragmentGenre.this.mContext.goToGenre(mGenreObject);
        }
    }

    public View onInflateLayout(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        return inflater.inflate(R.layout.fragment_recyclerview, container, false);
    }

    public void findView() {
        this.mContext = (MainActivity) getActivity();
        this.mContext = (MainActivity) getActivity();
        this.mTvNoResult = (TextView) this.mRootView.findViewById(R.id.tv_no_result);
        this.mTvNoResult.setTypeface(this.mContext.mTypefaceNormal);
        this.mRecyclerViewTrack = (RecyclerView) this.mRootView.findViewById(R.id.list_datas);
        this.mProgressBar = (CircularProgressBar) this.mRootView.findViewById(R.id.progressBar1);
        ConfigureModel configureModel = this.mContext.mTotalMng.getConfigureModel();
        this.mTypeUI = configureModel != null ? configureModel.getTypeGenre() : 1;
        if (this.mTypeUI == 1) {
            this.mContext.setUpRecyclerViewAsListView(this.mRecyclerViewTrack, null);
        } else {
            this.mContext.setUpRecyclerViewAsGridView(this.mRecyclerViewTrack, 2);
        }
        if (isFirstInTab()) {
            startLoadData();
        }
    }

    public void startLoadData() {
        if (!isLoadingData() && this.mContext != null) {
            setLoadingData(true);
            startGetData();
        }
    }

    private void startGetData() {
        this.mProgressBar.setVisibility(View.VISIBLE);
        this.mTvNoResult.setVisibility(View.GONE);
        DBExecutorSupplier.getInstance().forBackgroundTasks().execute(new C14431());
    }

    private void setUpInfo(ArrayList<GenreModel> mListTracks) {
        this.mRecyclerViewTrack.setAdapter(null);
        if (this.mListGenres != null) {
            this.mListGenres.clear();
            this.mListGenres = null;
        }
        this.mListGenres = mListTracks;
        if (mListTracks != null && mListTracks.size() > 0) {
            this.mGenreDataAdapter = new GenreDataAdapter(this.mContext, mListTracks, this.mContext.mTypefaceBold, this.mTypeUI);
            this.mRecyclerViewTrack.setAdapter(this.mGenreDataAdapter);
            this.mGenreDataAdapter.setOnGenreListener(new C14442());
        }
        updateInfo();
    }

    private void updateInfo() {
        int i = 0;
        if (this.mTvNoResult != null) {
            boolean b;
            if (this.mListGenres == null || this.mListGenres.size() <= 0) {
                b = false;
            } else {
                b = true;
            }
            TextView textView = this.mTvNoResult;
            if (b) {
                i = 8;
            }
            textView.setVisibility(i);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.mListGenres != null) {
            this.mListGenres.clear();
            this.mListGenres = null;
        }
    }
}
