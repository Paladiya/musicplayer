package refree.gautam.com.musicplayer.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import refree.gautam.com.musicplayer.R;

public class PresetDataAdapter extends ArrayAdapter<String> {
    private Context mContext;
    private LayoutInflater mInflater ;
    private String[] mListString;
    private Typeface mTypeFace;
    private IPresetListener presetListener;

    public interface IPresetListener {
        void onSelectItem(int i);
    }

    private static class ViewDropHolder {
        public TextView mTvName;

        private ViewDropHolder() {
        }
    }

    private static class ViewHolder {
        public TextView mTvName;

        private ViewHolder() {
        }
    }

    public PresetDataAdapter(Context context, int resource, String[] objects, Typeface mTypeFace) {
        super(context, resource, objects);
        this.mContext = context;
        mInflater = ((LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE));
        this.mListString = objects;
        this.mTypeFace = mTypeFace;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder mHolder;
        if (convertView == null) {
            mHolder = new ViewHolder();
            convertView = this.mInflater.inflate(R.layout.item_preset_name, null);
            convertView.setTag(mHolder);
            mHolder.mTvName = (TextView) convertView.findViewById(R.id.tv_name);
            mHolder.mTvName.setTypeface(this.mTypeFace);
        } else {
            mHolder = (ViewHolder) convertView.getTag();
        }
        mHolder.mTvName.setText(this.mListString[position]);
        return convertView;
    }

    public View getDropDownView(final int position, View convertView, ViewGroup parent) {
        ViewDropHolder mViewDropHolder;
        if (convertView == null) {
            mViewDropHolder = new ViewDropHolder();
            convertView = this.mInflater.inflate(R.layout.item_preset_name, null);
            convertView.setTag(mViewDropHolder);
            mViewDropHolder.mTvName = (TextView) convertView.findViewById(R.id.tv_name);
            mViewDropHolder.mTvName.setTypeface(this.mTypeFace);
        } else {
            mViewDropHolder = (ViewDropHolder) convertView.getTag();
        }
        mViewDropHolder.mTvName.setText(this.mListString[position]);
        mViewDropHolder.mTvName.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                View root = v.getRootView();
                root.dispatchKeyEvent(new KeyEvent(0, 4));
                root.dispatchKeyEvent(new KeyEvent(1, 4));
                if (PresetDataAdapter.this.presetListener != null) {
                    PresetDataAdapter.this.presetListener.onSelectItem(position);
                }
            }
        });
        return convertView;
    }

    public void setPresetListener(IPresetListener presetListener) {
        this.presetListener = presetListener;
    }
}
