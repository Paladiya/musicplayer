package refree.gautam.com.musicplayer;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import refree.gautam.com.musicplayer.BaseActivity.FragmentActivity;
import refree.gautam.com.musicplayer.InterFace.IXMusicConstants;
import refree.gautam.com.musicplayer.utils.DBLog;
import refree.gautam.com.musicplayer.utils.StringUtils;

public class PrivacyActivity extends FragmentActivity implements FragmentActivity.INetworkListener {
    public static final String TAG = PrivacyActivity.class.getSimpleName();
    private String mNameHeader;
    private ProgressBar mProgressBar;
    private String mUrl;
    private WebView mWebViewShowPage;

    class FinishActivity extends WebViewClient {

        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            PrivacyActivity.this.mProgressBar.setVisibility(View.GONE);
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_show_url);
        setUpCustomizeActionBar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setColorForActionBar(0);
        getSupportActionBar().setHomeAsUpIndicator(this.mBackDrawable);
        Intent args = getIntent();
        if (args != null) {
            this.mUrl = args.getStringExtra(IXMusicConstants.KEY_SHOW_URL);
            this.mNameHeader = args.getStringExtra(IXMusicConstants.KEY_HEADER);
            DBLog.LOGD(TAG, "===========>url=" + this.mUrl);
        }
        if (!StringUtils.isEmptyString(this.mNameHeader)) {
            setActionBarTitle(this.mNameHeader);
        }
        this.mProgressBar = (ProgressBar) findViewById(R.id.progressBar1);
        this.mProgressBar.setVisibility(View.VISIBLE);
        this.mWebViewShowPage = (WebView) findViewById(R.id.webview);
        this.mWebViewShowPage.getSettings().setJavaScriptEnabled(true);
        this.mWebViewShowPage.setWebViewClient(new FinishActivity());
        this.mWebViewShowPage.loadUrl(this.mUrl);
        setUpLayoutAdmob();
        registerNetworkBroadcastReceiver(this);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                backToHome();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.mWebViewShowPage != null) {
            this.mWebViewShowPage.destroy();
        }
    }

    private void backToHome() {
        finish();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        if (this.mWebViewShowPage.canGoBack()) {
            this.mWebViewShowPage.goBack();
        } else {
            backToHome();
        }
        return true;
    }

    public void onNetworkState(boolean isNetworkOn) {
        if (isNetworkOn) {
            setUpLayoutAdmob();
        }
    }
}