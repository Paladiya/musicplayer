package refree.gautam.com.musicplayer.imageloader.target;

import android.graphics.drawable.Drawable;
import android.view.ViewGroup;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.animation.GlideAnimation.ViewAdapter;
import com.bumptech.glide.request.target.ViewTarget;

public abstract class GlideGroupTarget<Z> extends ViewTarget<ViewGroup, Z> implements ViewAdapter {
    protected abstract void setResource(Z z);

    public GlideGroupTarget(ViewGroup view) {
        super(view);
    }

    public Drawable getCurrentDrawable() {
        return ((ViewGroup) this.view).getBackground();
    }

    public void setDrawable(Drawable drawable) {
        ((ViewGroup) this.view).setBackground(drawable);
    }

    public void onLoadStarted(Drawable placeholder) {
        ((ViewGroup) this.view).setBackground(placeholder);
    }

    public void onLoadFailed(Exception e, Drawable errorDrawable) {
        ((ViewGroup) this.view).setBackground(errorDrawable);
    }

    public void onLoadCleared(Drawable placeholder) {
        ((ViewGroup) this.view).setBackground(placeholder);
    }

    public void onResourceReady(Z resource, GlideAnimation<? super Z> glideAnimation) {
        setResource(resource);
    }
}
