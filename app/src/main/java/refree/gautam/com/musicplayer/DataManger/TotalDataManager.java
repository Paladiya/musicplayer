package refree.gautam.com.musicplayer.DataManger;

import android.content.Context;
import android.database.Cursor;
import android.os.Environment;
import android.provider.MediaStore.Audio.Media;
import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

import refree.gautam.com.musicplayer.BaseActivity.FragmentActivity;
import refree.gautam.com.musicplayer.InterFace.IXMusicConstants;
import refree.gautam.com.musicplayer.InterFace.IYPYCallback;
import refree.gautam.com.musicplayer.InterFace.IYPYSettingConstants;
import refree.gautam.com.musicplayer.InterFace.YPYSettingManager;
import refree.gautam.com.musicplayer.Models.ConfigureModel;
import refree.gautam.com.musicplayer.Models.GenreModel;
import refree.gautam.com.musicplayer.Models.PlaylistModel;
import refree.gautam.com.musicplayer.Models.TrackModel;
import refree.gautam.com.musicplayer.Models.UserModel;
import refree.gautam.com.musicplayer.R;
import refree.gautam.com.musicplayer.executor.DBExecutorSupplier;
import refree.gautam.com.musicplayer.utils.ApplicationUtils;
import refree.gautam.com.musicplayer.utils.DBLog;
import refree.gautam.com.musicplayer.utils.IOUtils;
import refree.gautam.com.musicplayer.utils.StringUtils;

public class TotalDataManager implements IXMusicConstants, IYPYSettingConstants {
    public static final String TAG = TotalDataManager.class.getSimpleName();
    private static TotalDataManager totalDataManager;
    private ConfigureModel configureModel;
    private GenreModel genreObject;
    private ArrayList<GenreModel> listGenreObjects;
    private ArrayList<TrackModel> listLibraryTrackObjects;
    private ArrayList<PlaylistModel> listPlaylistObjects;
    private ArrayList<TrackModel> listSavedTrackObjects;
    private ArrayList<TrackModel> listTopMusicObjects;
    private PlaylistModel playlistObject;

    class C14291 implements Runnable {
        C14291() {
        }

        public void run() {
            TotalDataManager.this.savePlaylistObjects();
        }
    }

    class C14302 implements Runnable {
        C14302() {
        }

        public void run() {
            TotalDataManager.this.savePlaylistObjects();
        }
    }

    class C14324 extends TypeToken<ArrayList<PlaylistModel>> {
        C14324() {
        }
    }

    class C14346 implements Runnable {
        C14346() {
        }

        public void run() {
            TotalDataManager.this.savePlaylistObjects();
            TotalDataManager.this.saveDataInCached(5);
        }
    }

    class C14379 extends TypeToken<ArrayList<TrackModel>> {
        C14379() {
        }
    }

    public static TotalDataManager getInstance() {
        if (totalDataManager == null) {
            totalDataManager = new TotalDataManager();
        }
        return totalDataManager;
    }

    private TotalDataManager() {
    }

    public void onDestroy() {
        if (this.listGenreObjects != null) {
            this.listGenreObjects.clear();
            this.listGenreObjects = null;
        }
        if (this.listPlaylistObjects != null) {
            this.listPlaylistObjects.clear();
            this.listPlaylistObjects = null;
        }
        if (this.listLibraryTrackObjects != null) {
            this.listLibraryTrackObjects.clear();
            this.listLibraryTrackObjects = null;
        }
        if (this.listTopMusicObjects != null) {
            this.listTopMusicObjects.clear();
            this.listTopMusicObjects = null;
        }
        totalDataManager = null;
    }

    public ArrayList<GenreModel> getListGenreObjects() {
        return this.listGenreObjects;
    }

    public void setListGenreObjects(ArrayList<GenreModel> listGenreObjects) {
        this.listGenreObjects = listGenreObjects;
    }

    public ArrayList<PlaylistModel> getListPlaylistObjects() {
        return this.listPlaylistObjects;
    }

    public void setListPlaylistObjects(ArrayList<PlaylistModel> listPlaylistObjects) {
        this.listPlaylistObjects = listPlaylistObjects;
    }

    public void addPlaylistObject(PlaylistModel mPlaylistObject) {
        if (this.listPlaylistObjects != null && mPlaylistObject != null) {
            synchronized (this.listPlaylistObjects) {
                this.listPlaylistObjects.add(mPlaylistObject);
            }
            DBExecutorSupplier.getInstance().forBackgroundTasks().execute(new C14291());
        }
    }

    public boolean isPlaylistNameExisted(String name) {
        if (!(StringUtils.isEmptyString(name) || this.listPlaylistObjects == null || this.listPlaylistObjects.size() <= 0)) {
            Iterator it = this.listPlaylistObjects.iterator();
            while (it.hasNext()) {
                if (((PlaylistModel) it.next()).getName().equals(name)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void editPlaylistObject(PlaylistModel mPlaylistObject, String newName) {
        if (this.listPlaylistObjects != null && mPlaylistObject != null && !StringUtils.isEmptyString(newName)) {
            mPlaylistObject.setName(newName);
            DBExecutorSupplier.getInstance().forBackgroundTasks().execute(new C14302());
        }
    }

    public void removePlaylistObject(PlaylistModel mPlaylistObject) {
        if (this.listPlaylistObjects != null && this.listPlaylistObjects.size() > 0) {
            this.listPlaylistObjects.remove(mPlaylistObject);
            ArrayList<TrackModel> mListTrack = mPlaylistObject.getListTrackObjects();
            boolean isNeedToSaveTrack = false;
            if (mListTrack != null && mListTrack.size() > 0) {
                Iterator it = mListTrack.iterator();
                while (it.hasNext()) {
                    TrackModel mTrackObject = (TrackModel) it.next();
                    boolean isAllowRemoveToList = true;
                    Iterator it2 = this.listPlaylistObjects.iterator();
                    while (it2.hasNext()) {
                        if (((PlaylistModel) it2.next()).isSongAlreadyExited(mTrackObject.getId())) {
                            isAllowRemoveToList = false;
                            break;
                        }
                    }
                    if (isAllowRemoveToList) {
                        this.listSavedTrackObjects.remove(mTrackObject);
                        isNeedToSaveTrack = true;
                    }
                }
                mListTrack.clear();
            }
            final boolean isGlobal = isNeedToSaveTrack;
            DBExecutorSupplier.getInstance().forBackgroundTasks().execute(new Runnable() {
                public void run() {
                    TotalDataManager.this.savePlaylistObjects();
                    if (isGlobal) {
                        TotalDataManager.this.saveDataInCached(5);
                    }
                }
            });
        }
    }

    public synchronized void savePlaylistObjects() {
        File mFile = getDirectoryTemp();
        if (!(mFile == null || this.listPlaylistObjects == null)) {
            String data = new GsonBuilder().create().toJson(this.listPlaylistObjects, new C14324().getType());
            DBLog.LOGD(TAG, "=============>savePlaylistObjects=" + data + "==>path=" + mFile.getAbsolutePath());
            IOUtils.writeString(mFile.getAbsolutePath(), IXMusicConstants.FILE_PLAYLIST, data);
        }
    }

    public ArrayList<TrackModel> getListTopMusicObjects() {
        return this.listTopMusicObjects;
    }

    public void setListTopMusicObjects(ArrayList<TrackModel> listTopMusicObjects) {
        if (this.listTopMusicObjects != null) {
            this.listTopMusicObjects.clear();
            this.listTopMusicObjects = null;
        }
        this.listTopMusicObjects = listTopMusicObjects;
    }

    public TrackModel getTrack(int type, long id) {
        ArrayList<TrackModel> mLisTracks = getListTracks(type);
        if (mLisTracks != null && mLisTracks.size() > 0) {
            Iterator it = mLisTracks.iterator();
            while (it.hasNext()) {
                TrackModel mTrackObject = (TrackModel) it.next();
                if (mTrackObject.getId() == id) {
                    return mTrackObject;
                }
            }
        }
        return null;
    }

    public void deleteSong(TrackModel mTrackObject, IYPYCallback mCallback) {
        File mFile = null;
        try {
            String path = mTrackObject.getPath();
            if (!StringUtils.isEmptyString(path)) {
                mFile = new File(path);
            }
            if (mFile != null && mFile.exists() && mFile.isFile()) {
                try {
                    if (mFile.delete()) {
                        removeSongFromList(MusicDataMng.getInstance().getListPlayingTrackObjects(), mTrackObject.getId());
                        removeSongFromList(this.listLibraryTrackObjects, mTrackObject.getId());
                        if (this.listPlaylistObjects != null && this.listPlaylistObjects.size() > 0) {
                            Iterator it = this.listPlaylistObjects.iterator();
                            while (it.hasNext()) {
                                if (removeTrackToPlaylistNoThread(mTrackObject, (PlaylistModel) it.next(), null, true)) {
                                    break;
                                }
                            }
                        }
                        if (mCallback != null) {
                            mCallback.onAction();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public ArrayList<TrackModel> startSearchSong(String keyword) {
        ArrayList<TrackModel> mListTracks = this.listLibraryTrackObjects;
        if (TextUtils.isEmpty(keyword)) {
            if (mListTracks != null && mListTracks.size() > 0) {
                return (ArrayList) mListTracks.clone();
            }
        } else if (mListTracks != null && mListTracks.size() > 0) {
            ArrayList<TrackModel> mListTrackObjects = new ArrayList();
            synchronized (mListTracks) {
                int size = mListTracks.size();
                if (size > 0) {
                    for (int i = 0; i < size; i++) {
                        TrackModel mTrackObject = (TrackModel) mListTracks.get(i);
                        if (mTrackObject.getTitle().toLowerCase(Locale.US).contains(keyword)) {
                            mListTrackObjects.add(mTrackObject.clone());
                        }
                    }
                }
            }
            return mListTrackObjects;
        }
        return null;
    }

    public boolean isLibraryTracks(TrackModel mTrackObject) {
        if (this.listLibraryTrackObjects != null && this.listLibraryTrackObjects.size() > 0) {
            Iterator it = this.listLibraryTrackObjects.iterator();
            while (it.hasNext()) {
                if (((TrackModel) it.next()).getId() == mTrackObject.getId()) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean removeSongFromList(ArrayList<TrackModel> listTrackObjects, long id) {
        if (listTrackObjects != null && listTrackObjects.size() > 0) {
            synchronized (listTrackObjects) {
                Iterator<TrackModel> mIterator = listTrackObjects.iterator();
                while (mIterator.hasNext()) {
                    if (((TrackModel) mIterator.next()).getId() == id) {
                        mIterator.remove();
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public void readPlaylistCached() {
        File mFile = getDirectoryTemp();
        if (mFile != null) {
            ArrayList<PlaylistModel> mListPlaylist = JsonParsingUtils.parsingPlaylistObject(IOUtils.readString(mFile.getAbsolutePath(), IXMusicConstants.FILE_PLAYLIST));
            if (mListPlaylist == null || mListPlaylist.size() <= 0) {
                mListPlaylist = new ArrayList();
                setListPlaylistObjects(mListPlaylist);
            } else {
                setListPlaylistObjects(mListPlaylist);
            }
            if (mListPlaylist.size() > 0) {
                Iterator it = mListPlaylist.iterator();
                while (it.hasNext()) {
                    filterSongOfPlaylistId((PlaylistModel) it.next());
                }
            }
        }
    }

    public File getDirectoryCached() {
        if (!ApplicationUtils.hasSDcard()) {
            return null;
        }
        try {
            File mFile = new File(Environment.getExternalStorageDirectory(), IXMusicConstants.DIR_CACHE);
            if (mFile.exists()) {
                return mFile;
            }
            mFile.mkdirs();
            return mFile;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private File getDirectoryTemp() {
        File mRoot = getDirectoryCached();
        if (mRoot == null) {
            return null;
        }
        File mFile = new File(mRoot, IXMusicConstants.DIR_TEMP);
        if (mFile.exists()) {
            return mFile;
        }
        mFile.mkdirs();
        return mFile;
    }

    public ArrayList<TrackModel> getListTracks(int type) {
        if (type == 5) {
            return this.listSavedTrackObjects;
        }
        return null;
    }

    public String getFileNameSaved(int type) {
        if (type == 5) {
            return IXMusicConstants.FILE_SAVED_TRACK;
        }
        return null;
    }

    public void saveListTrack(int type, ArrayList<TrackModel> listTrack) {
        if (type == 5) {
            this.listSavedTrackObjects = listTrack;
        }
    }

    public void readCached(int type) {
        ArrayList<TrackModel> mListTrackObject = getListTracks(type);
        if (mListTrackObject == null || mListTrackObject.size() <= 0) {
            File mFileCache = getDirectoryTemp();
            if (mFileCache != null) {
                File mFileData = new File(mFileCache, getFileNameSaved(type));
                if (mFileData.exists() && mFileData.isFile()) {
                    try {
                        ArrayList<TrackModel> mListTracks = JsonParsingUtils.parsingListTrackObjects(new FileInputStream(mFileData));
                        DBLog.LOGD(TAG, "=========>readCached=" + (mListTracks != null ? mListTracks.size() : 0));
                        if (mListTracks != null && mListTracks.size() > 0) {
                            saveListTrack(type, mListTracks);
                            return;
                        }
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                saveListTrack(type, new ArrayList());
            }
        }
    }

    private void filterSongOfPlaylistId(PlaylistModel mPlaylistObject) {
        if (this.listSavedTrackObjects != null && this.listSavedTrackObjects.size() > 0) {
            ArrayList<Long> mListId = mPlaylistObject.getListTrackIds();
            if (mListId != null && mListId.size() > 0) {
                Iterator it = mListId.iterator();
                while (it.hasNext()) {
                    Long mId = (Long) it.next();
                    Iterator it2 = this.listSavedTrackObjects.iterator();
                    while (it2.hasNext()) {
                        TrackModel mTrackObject = (TrackModel) it2.next();
                        if (mTrackObject.getId() == mId.longValue()) {
                            mPlaylistObject.addTrackObject(mTrackObject, false);
                            break;
                        }
                    }
                }
            }
        }
    }

    public synchronized void addTrackToPlaylist(final FragmentActivity mContext, final TrackModel mParentTrackObject, final PlaylistModel mPlaylistObject, boolean isShowMsg, IYPYCallback mCallback) {
        if (!(mParentTrackObject == null || mPlaylistObject == null)) {
            if (!mPlaylistObject.isSongAlreadyExited(mParentTrackObject.getId())) {
                TrackModel mTrackObject = mParentTrackObject.clone();
                mPlaylistObject.addTrackObject(mTrackObject, true);
                boolean isAllowAddToList = true;
                Iterator it = this.listSavedTrackObjects.iterator();
                while (it.hasNext()) {
                    if (((TrackModel) it.next()).getId() == mTrackObject.getId()) {
                        isAllowAddToList = false;
                        break;
                    }
                }
                if (isAllowAddToList) {
                    this.listSavedTrackObjects.add(mTrackObject);
                }
                if (mCallback != null) {
                    mCallback.onAction();
                }
                mContext.runOnUiThread(new Runnable() {
                    public void run() {
                        mContext.showToast(String.format(mContext.getString(R.string.info_add_playlist), new Object[]{mParentTrackObject.getTitle(), mPlaylistObject.getName()}));
                    }
                });
                DBExecutorSupplier.getInstance().forBackgroundTasks().execute(new C14346());
            } else if (isShowMsg) {
                mContext.runOnUiThread(new Runnable() {
                    public void run() {
                        mContext.showToast((int) R.string.info_song_already_playlist);
                    }
                });
            }
        }
    }

    public synchronized boolean removeTrackToPlaylistNoThread(TrackModel mTrackObject, PlaylistModel mPlaylistObject, IYPYCallback mCallback, boolean isNeedSave) {
        boolean z;
        if (mTrackObject == null || mPlaylistObject == null) {
            z = false;
        } else {
            mPlaylistObject.removeTrackObject(mTrackObject);
            z = true;
            Iterator it = this.listPlaylistObjects.iterator();
            while (it.hasNext()) {
                if (((PlaylistModel) it.next()).isSongAlreadyExited(mTrackObject.getId())) {
                    z = false;
                    break;
                }
            }
            if (mCallback != null) {
                mCallback.onAction();
            }
            DBLog.LOGD(TAG, "============>removeTrackToPlaylist=" + z);
            if (z) {
                this.listSavedTrackObjects.remove(mTrackObject);
                if (isNeedSave) {
                    savePlaylistObjects();
                    saveDataInCached(5);
                }
            }
        }
        return z;
    }

    public synchronized void removeTrackToPlaylist(final TrackModel mTrackObject, final PlaylistModel mPlaylistObject, final IYPYCallback mCallback) {
        DBExecutorSupplier.getInstance().forBackgroundTasks().execute(new Runnable() {
            public void run() {
                TotalDataManager.this.removeTrackToPlaylistNoThread(mTrackObject, mPlaylistObject, mCallback, true);
            }
        });
    }

    public void readGenreData(Context mContext) {
        ArrayList<GenreModel> mListGenres = JsonParsingUtils.parsingGenreObject(IOUtils.readStringFromAssets(mContext, IXMusicConstants.FILE_GENRE));
        DBLog.LOGD(TAG, "==========>size genres=" + (mListGenres != null ? mListGenres.size() : 0));
        if (mListGenres != null) {
            setListGenreObjects(mListGenres);
        }
    }

    public void readConfigure(Context mContext) {
        this.configureModel = JsonParsingUtils.parsingConfigureModel(IOUtils.readStringFromAssets(mContext, IXMusicConstants.FILE_CONFIGURE));
        if (this.configureModel != null) {
            YPYSettingManager.setBackground(mContext, this.configureModel.getBg());
        }
    }

    public synchronized void saveDataInCached(int type) {
        File mFile = getDirectoryTemp();
        if (mFile != null) {
            ArrayList<TrackModel> mListTracks = getListTracks(type);
            String data = "[]";
            if (mListTracks != null && mListTracks.size() > 0) {
                Gson mGson = new GsonBuilder().create();
                ArrayList<TrackModel> mListSave = new ArrayList();
                try {
                    Iterator it = mListTracks.iterator();
                    while (it.hasNext()) {
                        TrackModel mTrackObject = (TrackModel) it.next();
                        if (!mTrackObject.isNativeAds()) {
                            mListSave.add(mTrackObject);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                data = mGson.toJson(mListSave, new C14379().getType());
            }
            DBLog.LOGD(TAG, "===============>saveTrackDataInCached=" + data);
            IOUtils.writeString(mFile.getAbsolutePath(), getFileNameSaved(type), data);
        }
    }

    public PlaylistModel getPlaylistObject() {
        return this.playlistObject;
    }

    public void setPlaylistObject(PlaylistModel playlistObject) {
        this.playlistObject = playlistObject;
    }

    public GenreModel getGenreObject() {
        return this.genreObject;
    }

    public void setGenreObject(GenreModel genreObject) {
        this.genreObject = genreObject;
    }

    public void readLibraryTrack(Context mContext) {
        if (this.listLibraryTrackObjects == null || this.listLibraryTrackObjects.size() <= 0) {
            this.listLibraryTrackObjects = getListMusicsFromLibrary(mContext);
            sortListSongs(this.listLibraryTrackObjects);
        }
    }

    private ArrayList<TrackModel> getListMusicsFromLibrary(Context mContext) {
        Cursor cur = mContext.getContentResolver().query(Media.EXTERNAL_CONTENT_URI, null, "is_music = 1", null, null);
        DBLog.LOGD(TAG, "Query finished. " + (cur == null ? "Returned NULL." : "Returned a cursor."));
        if (cur == null) {
            DBLog.LOGD(TAG, "Failed to retrieve music: cursor is null :-(");
            return null;
        } else if (cur.moveToFirst()) {
            int artistColumn = cur.getColumnIndex("artist");
            int titleColumn = cur.getColumnIndex("title");
            int durationColumn = cur.getColumnIndex("duration");
            int idColumn = cur.getColumnIndex("_id");
            int dataColumn = cur.getColumnIndex("_data");
            int dateColumn = cur.getColumnIndex("date_modified");
            ArrayList<TrackModel> listTrackObjects = new ArrayList();
            do {
                long id = cur.getLong(idColumn);
                String singer = cur.getString(artistColumn);
                String title = cur.getString(titleColumn);
                long duration = cur.getLong(durationColumn);
                String path = cur.getString(dataColumn);
                Date date = new Date(cur.getLong(dateColumn) * 1000);
                if (!StringUtils.isEmptyString(path)) {
                    File file = new File(path);
                    if (file.exists() && file.isFile()) {
                        UserModel userModel = new UserModel(singer);
                        TrackModel trackModel = new TrackModel(path, title);
                        trackModel.setId(id);
                        trackModel.setUserObject(userModel);
                        trackModel.setDateCreated(date);
                        trackModel.setDuration(duration);
                        listTrackObjects.add(trackModel);
                    }
                }
            } while (cur.moveToNext());
            return listTrackObjects;
        } else {
            DBLog.LOGD(TAG, "Failed to move cursor to first row (no query results).");
            return null;
        }
    }

    public boolean sortListSongs(ArrayList<TrackModel> mListLibrary) {
        if (mListLibrary != null && mListLibrary.size() > 0) {
            try {
                Collections.sort(mListLibrary, new Comparator<TrackModel>() {
                    public int compare(TrackModel lhs, TrackModel rhs) {
                        return rhs.getDateCreated().compareTo(lhs.getDateCreated());
                    }
                });
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public ArrayList<TrackModel> getListLibraryTrackObjects() {
        return this.listLibraryTrackObjects;
    }

    public ArrayList<TrackModel> getAllTrackWithAdmob(Context mContext, ArrayList<TrackModel> mListTracks, int typeUI) {
        if (mListTracks == null) {
            return null;
        }
        ArrayList<TrackModel> arrayList = (ArrayList) mListTracks.clone();
        int size = arrayList.size();
        if (!false || size <= 0 || !ApplicationUtils.isOnline(mContext)) {
            return arrayList;
        }
        int len = ADS_FREQ.length;
        for (int i = 0; i < len; i++) {
            if (size >= ADS_FREQ[i] + 1) {
                TrackModel mObject = new TrackModel();
                mObject.setNativeAds(true);
                try {
                    arrayList.add(ADS_FREQ[i], mObject);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return arrayList;
    }

    public ArrayList<PlaylistModel> getAllPlaylistObjectWithAdmob(Context mContext, ArrayList<PlaylistModel> mListPlaylist, int typeUI) {
        if (mListPlaylist == null) {
            return null;
        }
        ArrayList<PlaylistModel> arrayList = (ArrayList) mListPlaylist.clone();
        int size = arrayList.size();
        if (!false || size <= 0 || !ApplicationUtils.isOnline(mContext)) {
            return arrayList;
        }
        int len = ADS_FREQ.length;
        for (int i = 0; i < len; i++) {
            if (size >= ADS_FREQ[i] + 1) {
                PlaylistModel mObject = new PlaylistModel();
                mObject.setNativeAds(true);
                try {
                    arrayList.add(ADS_FREQ[i], mObject);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return arrayList;
    }

    public ConfigureModel getConfigureModel() {
        return this.configureModel;
    }
}
