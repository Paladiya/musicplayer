package refree.gautam.com.musicplayer.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;

import refree.gautam.com.musicplayer.BaseActivity.FragmentActivity;
import refree.gautam.com.musicplayer.InterFace.IXMusicConstants;
import refree.gautam.com.musicplayer.Models.GenreModel;
import refree.gautam.com.musicplayer.R;
import refree.gautam.com.musicplayer.abtractclass.RecyclerViewAdapter;
import refree.gautam.com.musicplayer.imageloader.GlideImageLoader;
import refree.gautam.com.musicplayer.utils.FBAds;

public class GenreDataAdapter extends RecyclerViewAdapter implements IXMusicConstants {
    public static final String TAG = GenreDataAdapter.class.getSimpleName();
    private LayoutInflater mInflater;
    private int mType;
    private Typeface mTypefaceBold;
    private OnGenreListener onGenreListener;

    public class GenreHolder extends ViewHolder {
        public CardView mCardView;
        public ImageView mImgGenre;
        public View mLayoutRoot;
        public TextView mTvGenreName;

        public GenreHolder(View convertView) {
            super(convertView);
            this.mImgGenre = (ImageView) convertView.findViewById(R.id.img_genre);
            this.mTvGenreName = (TextView) convertView.findViewById(R.id.tv_genre_name);
            this.mCardView = (CardView) convertView.findViewById(R.id.card_view);
            this.mLayoutRoot = convertView.findViewById(R.id.layout_root);
            this.mTvGenreName.setTypeface(GenreDataAdapter.this.mTypefaceBold);
        }
    }

    public interface OnGenreListener {
        void goToDetail(GenreModel genreModel);
    }

    public GenreDataAdapter(FragmentActivity mContext, ArrayList<GenreModel> mListObjects, Typeface mTypefaceBold, int type) {
        super(mContext, mListObjects);
        this.mTypefaceBold = mTypefaceBold;
        this.mListObjects = mListObjects;
        this.mType = type;
        this.mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setOnGenreListener(OnGenreListener onTrackListener) {
        this.onGenreListener = onTrackListener;
    }

    public void onBindNormalViewHolder(ViewHolder holder, int position) {
        final GenreModel mGenreObject = (GenreModel) this.mListObjects.get(position);
        GenreHolder mGenreHolder = (GenreHolder) holder;
        mGenreHolder.mTvGenreName.setText(mGenreObject.getName());
        String artwork = mGenreObject.getImg();
        if (TextUtils.isEmpty(artwork)) {
            mGenreHolder.mImgGenre.setImageResource(R.drawable.ic_rect_music_default);
        } else {
            GlideImageLoader.displayImage(this.mContext, mGenreHolder.mImgGenre, artwork, (int) R.drawable.ic_rect_music_default);
        }
        if (mGenreHolder.mCardView != null) {
            mGenreHolder.mCardView.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    if (GenreDataAdapter.this.onGenreListener != null) {
                        GenreDataAdapter.this.onGenreListener.goToDetail(mGenreObject);
                        Log.d(TAG,"Load ad");
                        FBAds.showFbInterstitial(mContext);
                    }
                }
            });
        } else {
            mGenreHolder.mLayoutRoot.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    if (GenreDataAdapter.this.onGenreListener != null) {
                        GenreDataAdapter.this.onGenreListener.goToDetail(mGenreObject);
                        Log.d(TAG,"Load ad");
                        FBAds.showFbInterstitial(mContext);
                    }
                }
            });
        }
    }

    public ViewHolder onCreateNormalViewHolder(ViewGroup v, int viewType) {
        return new GenreHolder(this.mInflater.inflate(this.mType == 2 ? R.layout.item_grid_genre : R.layout.item_list_genre, v, false));
    }
}
