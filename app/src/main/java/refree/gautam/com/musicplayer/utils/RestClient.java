package refree.gautam.com.musicplayer.utils;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

public class RestClient {
    public static final String BOUNDARY = "*****";
    public static final String LINE_END = "\r\n";
    public static final int MAX_BUFFER_SIZE = 4096;
    private static final int READ_TIMEOUT = 15000;
    private static final String TAG = RestClient.class.getSimpleName();
    private static final int TIMEOUT_CONNECTION = 15000;
    public static final String TWO_HYPHENS = "--";
    private ArrayList<KeyValueObject> headers;
    private InputStream inputStreamRes;
    private boolean isAppendParamsInGET = true;
    private boolean isEncodeParams = true;
    private boolean isUploadFile;
    private String mUrlApiEndpoint;
    private String message;
    private String mimeType;
    private String nameFileParams;
    private ArrayList<KeyValueObject> params;
    private String response;
    private int responseCode;
    private String uploadFilePath;

    public class KeyValueObject {
        public String key;
        public String value;

        public KeyValueObject(String key, String value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return this.key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return this.value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    public enum RequestMethod {
        GET,
        POST
    }

    public RestClient(String url) {
        this.mUrlApiEndpoint = url;
        this.params = new ArrayList();
        this.headers = new ArrayList();
    }

    public RestClient(String url, boolean isEncodeParams) {
        this.mUrlApiEndpoint = url;
        this.isEncodeParams = isEncodeParams;
        this.params = new ArrayList();
        this.headers = new ArrayList();
    }

    public void setIsUploadFile(boolean isUploadFile) {
        this.isUploadFile = isUploadFile;
        if (isUploadFile) {
            this.headers.add(new KeyValueObject("Connection", "Keep-Alive"));
            this.headers.add(new KeyValueObject("ENCTYPE", "multipart/form-data"));
            this.headers.add(new KeyValueObject("Content-Type", "multipart/form-data;BOUNDARY=*****"));
        }
    }

    public void setUploadFilePath(String uploadFilePath, String nameParams, String mimeType) {
        this.uploadFilePath = uploadFilePath;
        this.nameFileParams = nameParams;
        this.mimeType = mimeType;
        if (this.isUploadFile) {
            this.headers.add(new KeyValueObject(this.nameFileParams, uploadFilePath));
        }
    }

    public void addParams(String name, String value) {
        this.params.add(new KeyValueObject(name, value));
    }

    public void addHeader(String name, String value) {
        this.headers.add(new KeyValueObject(name, value));
    }

    public void setIsAppendParamsInGET(boolean isAppendParamsInGET) {
        this.isAppendParamsInGET = isAppendParamsInGET;
    }

    public String getmUrlApiEndpoint() {
        return this.mUrlApiEndpoint;
    }

    public void setmUrlApiEndpoint(String mUrlApiEndpoint) {
        this.mUrlApiEndpoint = mUrlApiEndpoint;
    }

    public String getResponse() {
        return this.response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getResponseCode() {
        return this.responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public void excute(RequestMethod resMethod, boolean isGetInput) throws UnsupportedEncodingException {
        switch (resMethod) {
            case GET:
                String params = appendParams();
                String mUriEndpoint = this.mUrlApiEndpoint;
                if (this.isAppendParamsInGET) {
                    if (!StringUtils.isEmptyString(params)) {
                        mUriEndpoint = mUriEndpoint + params;
                    }
                    DBLog.LOGD(TAG, "==========>mUriEndpoint=" + mUriEndpoint);
                    executeRequest("GET", mUriEndpoint, null, isGetInput);
                    return;
                }
                executeRequest("GET", mUriEndpoint, params, isGetInput);
                return;
            case POST:
                if (this.isUploadFile) {
                    executeUploadRequest("POST", this.mUrlApiEndpoint, isGetInput);
                    return;
                }
                executeRequest("POST", this.mUrlApiEndpoint, appendParams(), isGetInput);
                return;
            default:
                return;
        }
    }

    private String appendParams() {
        if (this.params == null || this.params.size() <= 0) {
            return null;
        }
        StringBuilder mStringBuilder = new StringBuilder();
        int size = this.params.size();
        for (int i = 0; i < size; i++) {
            KeyValueObject mKeyValueObject = (KeyValueObject) this.params.get(i);
            mStringBuilder.append(mKeyValueObject.getKey());
            mStringBuilder.append("=");
            mStringBuilder.append(this.isEncodeParams ? StringUtils.urlEncodeString(mKeyValueObject.getValue()) : mKeyValueObject.getValue());
            if (i != size - 1) {
                mStringBuilder.append("&");
            }
        }
        return mStringBuilder.toString();
    }

    private void executeRequest(String resMethod, String mUrlApiEndpoint, String params, boolean isGetInput) {
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(mUrlApiEndpoint).openConnection();
            conn.setConnectTimeout(15000);
            conn.setReadTimeout(15000);
            conn.setDoInput(true);
            conn.setUseCaches(false);
            conn.setRequestMethod(resMethod);
            if (this.headers != null && this.headers.size() > 0) {
                Iterator it = this.headers.iterator();
                while (it.hasNext()) {
                    KeyValueObject mKeyValueObject = (KeyValueObject) it.next();
                    conn.addRequestProperty(mKeyValueObject.getKey(), mKeyValueObject.getValue());
                }
            }
            if (!StringUtils.isEmptyString(params)) {
                new DataOutputStream(conn.getOutputStream()).write(params.getBytes());
            }
            conn.connect();
            int serverResponseCode = conn.getResponseCode();
            DBLog.LOGD(TAG, "executeRequest httcode= : " + serverResponseCode);
            InputStream inputStream = conn.getInputStream();
            if (serverResponseCode != 200) {
                return;
            }
            if (isGetInput) {
                this.inputStreamRes = inputStream;
                return;
            }
            this.response = convertStreamToString(inputStream);
            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void executeUploadRequest(String resMethod, String mUrlApiEndpoint, boolean isGetInput) {
        try {
            if (!StringUtils.isEmptyString(this.uploadFilePath)) {
                File mFile = new File(this.uploadFilePath);
                if (mFile.exists() && mFile.isFile()) {
                    Iterator it;
                    KeyValueObject mKeyValueObject;
                    FileInputStream fileInputStream = new FileInputStream(mFile);
                    HttpURLConnection conn = (HttpURLConnection) new URL(mUrlApiEndpoint).openConnection();
                    conn.setConnectTimeout(15000);
                    conn.setReadTimeout(15000);
                    conn.setDoInput(true);
                    conn.setDoOutput(true);
                    conn.setUseCaches(false);
                    conn.setRequestMethod(resMethod);
                    if (this.headers != null && this.headers.size() > 0) {
                        it = this.headers.iterator();
                        while (it.hasNext()) {
                            mKeyValueObject = (KeyValueObject) it.next();
                            conn.setRequestProperty(mKeyValueObject.getKey(), mKeyValueObject.getValue());
                        }
                    }
                    DataOutputStream dos = new DataOutputStream(conn.getOutputStream());
                    if (this.params != null && this.params.size() > 0) {
                        it = this.params.iterator();
                        while (it.hasNext()) {
                            mKeyValueObject = (KeyValueObject) it.next();
                            dos.writeBytes("--*****\r\n");
                            dos.writeBytes("Content-Disposition: form-data; name=\"" + mKeyValueObject.getKey() + "\"" + LINE_END);
                            dos.writeBytes(LINE_END);
                            dos.writeBytes(mKeyValueObject.getValue());
                            dos.writeBytes(LINE_END);
                        }
                    }
                    dos.writeBytes("--*****\r\n");
                    dos.writeBytes("Content-Disposition: form-data; name=\"" + this.nameFileParams + "\";filename=\"" + this.uploadFilePath + "\"" + LINE_END);
                    if (!StringUtils.isEmptyString(this.mimeType)) {
                        dos.writeBytes("Content-Type: " + this.mimeType + LINE_END);
                        dos.writeBytes(LINE_END);
                    }
                    dos.flush();
                    byte[] buffer = new byte[4096];
                    while (true) {
                        int bytesRead = fileInputStream.read(buffer);
                        if (bytesRead == -1) {
                            break;
                        }
                        dos.write(buffer, 0, bytesRead);
                    }
                    dos.flush();
                    dos.writeBytes(LINE_END);
                    dos.writeBytes("--*****--\r\n");
                    int serverResponseCode = conn.getResponseCode();
                    InputStream inputStream = conn.getInputStream();
                    if (serverResponseCode == 200) {
                        if (isGetInput) {
                            this.inputStreamRes = inputStream;
                        } else {
                            this.response = convertStreamToString(inputStream);
                            inputStream.close();
                        }
                    }
                    fileInputStream.close();
                    dos.flush();
                    dos.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public InputStream getInputStreamRes() {
        return this.inputStreamRes;
    }

    public void setInputStreamRes(InputStream inputStreamRes) {
        this.inputStreamRes = inputStreamRes;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private String convertStreamToString(InputStream r7) {
        /*
        r6 = this;
        r2 = new java.io.BufferedReader;
        r4 = new java.io.InputStreamReader;
        r4.<init>(r7);
        r2.<init>(r4);
        r3 = new java.lang.StringBuilder;
        r3.<init>();
        r1 = 0;
    L_0x0010:
        r1 = r2.readLine();	 Catch:{ IOException -> 0x002d }
        if (r1 == 0) goto L_0x0039;
    L_0x0016:
        r4 = new java.lang.StringBuilder;	 Catch:{ IOException -> 0x002d }
        r4.<init>();	 Catch:{ IOException -> 0x002d }
        r4 = r4.append(r1);	 Catch:{ IOException -> 0x002d }
        r5 = "\n";
        r4 = r4.append(r5);	 Catch:{ IOException -> 0x002d }
        r4 = r4.toString();	 Catch:{ IOException -> 0x002d }
        r3.append(r4);	 Catch:{ IOException -> 0x002d }
        goto L_0x0010;
    L_0x002d:
        r0 = move-exception;
        r0.printStackTrace();	 Catch:{ all -> 0x0047 }
        r7.close();	 Catch:{ IOException -> 0x0042 }
    L_0x0034:
        r4 = r3.toString();
        return r4;
    L_0x0039:
        r7.close();	 Catch:{ IOException -> 0x003d }
        goto L_0x0034;
    L_0x003d:
        r0 = move-exception;
        r0.printStackTrace();
        goto L_0x0034;
    L_0x0042:
        r0 = move-exception;
        r0.printStackTrace();
        goto L_0x0034;
    L_0x0047:
        r4 = move-exception;
        r7.close();	 Catch:{ IOException -> 0x004c }
    L_0x004b:
        throw r4;
    L_0x004c:
        r0 = move-exception;
        r0.printStackTrace();
        goto L_0x004b;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.twomedfreetools.joox_music.utils.RestClient.convertStreamToString(java.io.InputStream):java.lang.String");
    }

    public ArrayList<KeyValueObject> getHeaders() {
        return this.headers;
    }

    public ArrayList<KeyValueObject> getParams() {
        return this.params;
    }
}
