package refree.gautam.com.musicplayer.executor;

import android.os.Process;
import java.util.concurrent.ThreadFactory;

public class PriorityThreadFactory implements ThreadFactory {
    private Thread mThread;
    private final int mThreadPriority;

    public PriorityThreadFactory(int threadPriority) {
        this.mThreadPriority = threadPriority;
    }

    public Thread newThread(final Runnable runnable) {
        this.mThread = new Thread(new Runnable() {
            public void run() {
                try {
                    Process.setThreadPriority(PriorityThreadFactory.this.mThreadPriority);
                } catch (Throwable t) {
                    t.printStackTrace();
                }
                runnable.run();
            }
        });
        return this.mThread;
    }

    public void onDestroy() {
        try {
            if (this.mThread != null) {
                this.mThread.interrupt();
                this.mThread = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
