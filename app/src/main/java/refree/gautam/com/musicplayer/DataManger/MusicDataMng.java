package refree.gautam.com.musicplayer.DataManger;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.audiofx.BassBoost;
import android.media.audiofx.Equalizer;
import android.media.audiofx.Virtualizer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import refree.gautam.com.musicplayer.InterFace.YPYSettingManager;
import refree.gautam.com.musicplayer.Models.TrackModel;
import refree.gautam.com.musicplayer.utils.DBLog;

public class MusicDataMng {
    private static final String TAG = MusicDataMng.class.getSimpleName();
    private static MusicDataMng instance;
    private BassBoost bassBoost;
    private int currentIndex = -1;
    private long currentPlayingId;
    private TrackModel currentTrackObject;
    private Equalizer equalizer;
    private boolean isLoading;
    private ArrayList<TrackModel> listTrackObjects;
    private Random mRandom = new Random();
    private MediaPlayer player;
    private Virtualizer virtualizer;

    private MusicDataMng() {
    }

    public static MusicDataMng getInstance() {
        if (instance == null) {
            instance = new MusicDataMng();
        }
        return instance;
    }

    public void onDestroy() {
        DBLog.LOGD(TAG, "====================>onDestroy");
        if (this.listTrackObjects != null) {
            this.listTrackObjects.clear();
            this.listTrackObjects = null;
        }
        this.currentTrackObject = null;
        this.mRandom = null;
        this.player = null;
        instance = null;
    }

    public void releaseEffect() {
        try {
            if (this.equalizer != null) {
                this.equalizer.release();
                this.equalizer = null;
            }
            if (this.bassBoost != null) {
                this.bassBoost.release();
                this.bassBoost = null;
            }
            if (this.virtualizer != null) {
                this.virtualizer.release();
                this.virtualizer = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onResetData() {
        if (this.listTrackObjects != null) {
            this.listTrackObjects.clear();
            this.listTrackObjects = null;
        }
        this.isLoading = false;
        this.currentIndex = -1;
        this.currentTrackObject = null;
    }

    public void setListPlayingTrackObjects(ArrayList<TrackModel> listTrackObjects) {
        if (this.listTrackObjects != null) {
            this.listTrackObjects.clear();
            this.listTrackObjects = null;
        }
        this.isLoading = false;
        if (listTrackObjects != null) {
            if (listTrackObjects.size() > 0) {
                boolean isNeedToReset = true;
                if (this.currentTrackObject != null) {
                    Iterator it = listTrackObjects.iterator();
                    while (it.hasNext()) {
                        TrackModel mTrackObject = (TrackModel) it.next();
                        if (this.currentTrackObject.getId() == mTrackObject.getId()) {
                            isNeedToReset = false;
                            this.currentIndex = listTrackObjects.indexOf(mTrackObject);
                            break;
                        }
                    }
                }
                if (isNeedToReset) {
                    this.currentIndex = 0;
                }
            } else {
                this.currentIndex = -1;
            }
        }
        this.listTrackObjects = listTrackObjects;
    }

    public int getCurrentIndex() {
        return this.currentIndex;
    }

    public boolean setCurrentIndex(TrackModel mTrackObject) {
        if (this.listTrackObjects == null || this.listTrackObjects.size() <= 0 || mTrackObject == null) {
            return false;
        }
        this.currentTrackObject = mTrackObject;
        Iterator it = this.listTrackObjects.iterator();
        while (it.hasNext()) {
            TrackModel mTrackObject1 = (TrackModel) it.next();
            if (mTrackObject1.getId() == mTrackObject.getId()) {
                this.currentIndex = this.listTrackObjects.indexOf(mTrackObject1);
                break;
            }
        }
        DBLog.LOGD(TAG, "===========>mTrackObject=" + mTrackObject.getId() + "===>currentIndex=" + this.currentIndex);
        if (this.currentIndex >= 0) {
            return true;
        }
        this.currentIndex = 0;
        return false;
    }

    public ArrayList<TrackModel> getListPlayingTrackObjects() {
        return this.listTrackObjects;
    }

    public TrackModel getTrackObject(long id) {
        if (this.listTrackObjects != null && this.listTrackObjects.size() > 0) {
            Iterator it = this.listTrackObjects.iterator();
            while (it.hasNext()) {
                TrackModel mTrackObject = (TrackModel) it.next();
                if (mTrackObject.getId() == id) {
                    return mTrackObject;
                }
            }
        }
        return null;
    }

    public TrackModel getCurrentTrackObject() {
        return this.currentTrackObject;
    }

    public TrackModel getNextTrackObject(Context mContext, boolean isComplete) {
        if (this.listTrackObjects != null) {
            int size = this.listTrackObjects.size();
            DBLog.LOGD(TAG, "==========>currentIndex=" + this.currentIndex);
            if (size > 0 && this.currentIndex >= 0 && this.currentIndex < size) {
                int typeRepeat = YPYSettingManager.getNewRepeat(mContext);
                if (typeRepeat == 1 && isComplete && this.currentTrackObject != null) {
                    return this.currentTrackObject;
                }
                if (YPYSettingManager.getShuffle(mContext)) {
                    this.currentIndex = this.mRandom.nextInt(size);
                    this.currentTrackObject = (TrackModel) this.listTrackObjects.get(this.currentIndex);
                    return this.currentTrackObject;
                }
                this.currentIndex++;
                if (this.currentIndex >= size) {
                    this.currentIndex = 0;
                    this.currentTrackObject = (TrackModel) this.listTrackObjects.get(this.currentIndex);
                    return typeRepeat == 2 ? this.currentTrackObject : null;
                } else {
                    this.currentTrackObject = (TrackModel) this.listTrackObjects.get(this.currentIndex);
                    return this.currentTrackObject;
                }
            }
        }
        return null;
    }

    public TrackModel getPrevTrackObject(Context mContext) {
        if (this.listTrackObjects != null) {
            int size = this.listTrackObjects.size();
            if (size > 0 && this.currentIndex >= 0 && this.currentIndex <= size) {
                int typeRepeat = YPYSettingManager.getNewRepeat(mContext);
                if (YPYSettingManager.getShuffle(mContext)) {
                    this.currentIndex = this.mRandom.nextInt(size);
                    this.currentTrackObject = (TrackModel) this.listTrackObjects.get(this.currentIndex);
                    return this.currentTrackObject;
                }
                this.currentIndex--;
                if (this.currentIndex < 0) {
                    this.currentIndex = size - 1;
                    this.currentTrackObject = (TrackModel) this.listTrackObjects.get(this.currentIndex);
                    return typeRepeat == 2 ? this.currentTrackObject : null;
                } else {
                    this.currentTrackObject = (TrackModel) this.listTrackObjects.get(this.currentIndex);
                    return this.currentTrackObject;
                }
            }
        }
        return null;
    }

    public MediaPlayer getPlayer() {
        return this.player;
    }

    public void setPlayer(MediaPlayer player) {
        this.player = player;
    }

    public long getCurrentPlayingId() {
        return this.currentPlayingId;
    }

    public void setCurrentPlayingId(long currentPlayingId) {
        this.currentPlayingId = currentPlayingId;
    }

    public boolean isLoading() {
        return this.isLoading;
    }

    public void setLoading(boolean loading) {
        this.isLoading = loading;
    }

    public boolean isPlayingMusic() {
        try {
            if (isPrepaireDone()) {
                return this.player.isPlaying();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isPrepaireDone() {
        int sessionId = 0;
        try {
            if (this.player != null) {
                sessionId = this.player.getAudioSessionId();
            }
            if (sessionId != 0) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public Equalizer getEqualizer() {
        return this.equalizer;
    }

    public void setEqualizer(Equalizer equalizer) {
        this.equalizer = equalizer;
    }

    public BassBoost getBassBoost() {
        return this.bassBoost;
    }

    public void setBassBoost(BassBoost bassBoost) {
        this.bassBoost = bassBoost;
    }

    public Virtualizer getVirtualizer() {
        return this.virtualizer;
    }

    public void setVirtualizer(Virtualizer virtualizer) {
        this.virtualizer = virtualizer;
    }
}
