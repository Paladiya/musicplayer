package refree.gautam.com.musicplayer.abtractclass.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import java.util.ArrayList;

public class DBFragmentAdapter extends FragmentPagerAdapter {
    public static final String TAG = DBFragmentAdapter.class.getSimpleName();
    private ArrayList<Fragment> listFragments;

    public DBFragmentAdapter(FragmentManager fm, ArrayList<Fragment> listFragments) {
        super(fm);
        this.listFragments = listFragments;
    }

    public Fragment getItem(int position) {
        return (Fragment) this.listFragments.get(position);
    }

    public int getCount() {
        return this.listFragments.size();
    }

    public void destroyItem(View pView, int pIndex, Object pObject) {
        try {
            ((ViewPager) pView).removeView((View) pObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
