package refree.gautam.com.musicplayer;

import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.StateListDrawable;
import android.media.MediaPlayer;
import android.media.audiofx.BassBoost;
import android.media.audiofx.Equalizer;
import android.media.audiofx.Virtualizer;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import com.triggertrap.seekarc.SeekArc;
import com.triggertrap.seekarc.SeekArc.OnSeekArcChangeListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import refree.gautam.com.musicplayer.Adapter.PresetDataAdapter;
import refree.gautam.com.musicplayer.BaseActivity.FragmentActivity;
import refree.gautam.com.musicplayer.DataManger.MusicDataMng;
import refree.gautam.com.musicplayer.InterFace.IDBMusicPlayerListener;
import refree.gautam.com.musicplayer.InterFace.YPYSettingManager;
import refree.gautam.com.musicplayer.utils.DBLog;
import refree.gautam.com.musicplayer.utils.FBAds;
import refree.gautam.com.musicplayer.utils.StringUtils;
import refree.gautam.com.musicplayer.view.SwitchView;
import refree.gautam.com.musicplayer.view.VerticalSeekBar;

public class EqualizerActivity extends FragmentActivity implements IDBMusicPlayerListener {
    public static final String TAG = EqualizerActivity.class.getSimpleName();
    private short bands;
    private boolean isCreateLocal;
    private ArrayList<VerticalSeekBar> listSeekBars = new ArrayList();
    private BassBoost mBassBoost;
    private SeekArc mCircularBass;
    private SeekArc mCircularVir;
    private Equalizer mEqualizer;
    private String[] mEqualizerParams;
    private LinearLayout mLayoutBands;
    private LinearLayout mLayoutBassVir;
    private String[] mLists;
    private MediaPlayer mMediaPlayer;
    private Spinner mSpinnerPresents;
    private SwitchView mSwitchBtn;
    private TextView mTvBass;
    private TextView mTvEqualizer;
    private TextView mTvInfoBass;
    private TextView mTvInfoVirtualizer;
    private TextView mTvVirtualizer;
    private Virtualizer mVirtualizer;
    private short maxEQLevel;
    private short minEQLevel;

    class OnSeekBarChange implements OnSeekArcChangeListener {

        public void onProgressChanged(SeekArc seekArc, int progress, boolean fromUser) {
            try {
                EqualizerActivity.this.mTvInfoBass.setText(String.valueOf(progress));
                if (fromUser && EqualizerActivity.this.mBassBoost != null) {
                    YPYSettingManager.setBassBoost(EqualizerActivity.this, (short) progress);
                    EqualizerActivity.this.mBassBoost.setStrength((short) (progress * 10));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void onStartTrackingTouch(SeekArc seekArc) {
        }

        public void onStopTrackingTouch(SeekArc seekArc) {
        }
    }

    class OnSeekbarArcChange implements OnSeekArcChangeListener {

        public void onProgressChanged(SeekArc seekArc, int progress, boolean fromUser) {
            try {
                EqualizerActivity.this.mTvInfoVirtualizer.setText(String.valueOf(progress));
                if (fromUser && EqualizerActivity.this.mVirtualizer != null) {
                    YPYSettingManager.setVirtualizer(EqualizerActivity.this, (short) progress);
                    EqualizerActivity.this.mVirtualizer.setStrength((short) (progress * 10));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void onStartTrackingTouch(SeekArc seekArc) {
        }

        public void onStopTrackingTouch(SeekArc seekArc) {
        }
    }

    class OnCheckChange implements SwitchView.OnCheckListener {

        public void onCheck(boolean check) {
            YPYSettingManager.setEqualizer(EqualizerActivity.this, check);
            EqualizerActivity.this.startCheckEqualizer();
        }
    }

    class C13815 implements PresetDataAdapter.IPresetListener {
        C13815() {
        }

        public void onSelectItem(int position) {
            EqualizerActivity.this.mSpinnerPresents.setSelection(position);
        }
    }

    class OnItemChange implements OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            YPYSettingManager.setEqualizerPreset(EqualizerActivity.this, String.valueOf(position));
            try {
                if (position < EqualizerActivity.this.mLists.length - 1) {
                    EqualizerActivity.this.mEqualizer.usePreset((short) position);
                } else {
                    EqualizerActivity.this.setUpEqualizerCustom();
                }
                for (short i = (short) 0; i < EqualizerActivity.this.bands; i = (short) (i + 1)) {
                    ((VerticalSeekBar) EqualizerActivity.this.listSeekBars.get(i)).setProgress(EqualizerActivity.this.mEqualizer.getBandLevel(i) - EqualizerActivity.this.minEQLevel);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_equalizer);
        setUpCustomizeActionBar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setColorForActionBar(0);
        getSupportActionBar().setHomeAsUpIndicator(this.mBackDrawable);
        this.mLayoutBands = (LinearLayout) findViewById(R.id.layout_bands);
        this.mSpinnerPresents = (Spinner) findViewById(R.id.list_preset);
        this.mSwitchBtn = (SwitchView) findViewById(R.id.switch1);
        this.mSwitchBtn.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        this.mTvBass = (TextView) findViewById(R.id.tv_bass);
        this.mTvBass.setTypeface(this.mTypefaceBold);
        this.mTvEqualizer = (TextView) findViewById(R.id.tv_equalizer);
        this.mTvEqualizer.setTypeface(this.mTypefaceBold);
        this.mTvVirtualizer = (TextView) findViewById(R.id.tv_virtualizer);
        this.mTvVirtualizer.setTypeface(this.mTypefaceBold);
        this.mTvInfoVirtualizer = (TextView) findViewById(R.id.tv_info_virtualizer);
        this.mTvInfoVirtualizer.setTypeface(this.mTypefaceNormal);
        this.mTvInfoBass = (TextView) findViewById(R.id.tv_info_bass);
        this.mTvInfoBass.setTypeface(this.mTypefaceNormal);
        this.mLayoutBassVir = (LinearLayout) findViewById(R.id.layout_bass_vir);
        this.mCircularBass = (SeekArc) findViewById(R.id.seekBass);
        this.mCircularBass.setProgressColor(getResources().getColor(R.color.colorAccent));
        this.mCircularBass.setArcColor(getResources().getColor(R.color.main_color_hint_text));
        this.mCircularBass.setOnSeekArcChangeListener(new OnSeekBarChange());
        this.mCircularVir = (SeekArc) findViewById(R.id.seekVir);
        this.mCircularVir.setProgressColor(getResources().getColor(R.color.colorAccent));
        this.mCircularVir.setArcColor(getResources().getColor(R.color.main_color_hint_text));
        this.mCircularVir.setOnSeekArcChangeListener(new OnSeekbarArcChange());
        this.mSwitchBtn.setOncheckListener(new OnCheckChange());
        registerMusicPlayerBroadCastReceiver(this);
        setUpEffects(true);
        ((LinearLayout)findViewById(R.id.layout_ads)).addView(FBAds.GetFbAdView(this));
        ((LinearLayout)findViewById(R.id.layout_ads)).addView(FBAds.GetFbAdView(this));
        FBAds.showFbInterstitial(this);
    }

    private void setUpEqualizerParams() {
        if (this.mEqualizer != null) {
            String presetStr = YPYSettingManager.getEqualizerPreset(this);
            if (!StringUtils.isEmptyString(presetStr) && StringUtils.isNumber(presetStr)) {
                short preset = Short.parseShort(presetStr);
                short numberPreset = this.mEqualizer.getNumberOfPresets();
                if (numberPreset > (short) 0 && preset < numberPreset - 1 && preset >= (short) 0) {
                    this.mEqualizer.usePreset(preset);
                    this.mSpinnerPresents.setSelection(preset);
                    return;
                }
            }
            setUpEqualizerCustom();
        }
    }

    private void setUpEqualizerCustom() {
        try {
            if (this.mEqualizer != null) {
                String params = YPYSettingManager.getEqualizerParams(this);
                if (!StringUtils.isEmptyString(params)) {
                    this.mEqualizerParams = params.split(":");
                    if (this.mEqualizerParams != null && this.mEqualizerParams.length > 0) {
                        int size = this.mEqualizerParams.length;
                        for (int i = 0; i < size; i++) {
                            this.mEqualizer.setBandLevel((short) i, Short.parseShort(this.mEqualizerParams[i]));
                            ((VerticalSeekBar) this.listSeekBars.get(i)).setProgress(Short.parseShort(this.mEqualizerParams[i]) - this.minEQLevel);
                        }
                        this.mSpinnerPresents.setSelection(this.mLists.length - 1);
                        YPYSettingManager.setEqualizerPreset(this, String.valueOf(this.mLists.length - 1));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveEqualizerParams() {
        try {
            if (this.mEqualizer != null && this.bands > (short) 0) {
                String data = "";
                for (short i = (short) 0; i < this.bands; i = (short) (i + 1)) {
                    if (i < this.bands - 1) {
                        data = data + this.mEqualizer.getBandLevel(i) + ":";
                    }
                }
                YPYSettingManager.setEqualizerPreset(this, String.valueOf(this.mLists.length - 1));
                YPYSettingManager.setEqualizerParams(this, data);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startCheckEqualizer() {
        try {
            boolean b = YPYSettingManager.getEqualizer(this);
            this.mSpinnerPresents.setEnabled(b);
            if (this.mEqualizer != null) {
                this.mEqualizer.setEnabled(b);
            }
            if (this.listSeekBars.size() > 0) {
                for (int i = 0; i < this.listSeekBars.size(); i++) {
                    ((VerticalSeekBar) this.listSeekBars.get(i)).setEnabled(b);
                }
            }
            this.mCircularBass.setEnabled(b);
            this.mCircularVir.setEnabled(b);
            this.mSwitchBtn.setChecked(b);
            if (this.mBassBoost != null) {
                this.mBassBoost.setEnabled(b);
            }
            if (this.mVirtualizer != null) {
                this.mVirtualizer.setEnabled(b);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupEqualizerFxAndUI(boolean isFirstTime) {
        long pivotTime = System.currentTimeMillis();
        this.mEqualizer = MusicDataMng.getInstance().getEqualizer();
        if (this.mEqualizer == null) {
            this.mEqualizer = new Equalizer(0, this.mMediaPlayer.getAudioSessionId());
            this.mEqualizer.setEnabled(YPYSettingManager.getEqualizer(this));
        }
        try {
            this.bands = this.mEqualizer.getNumberOfBands();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (this.bands == (short) 0) {
            backToHome();
            return;
        }
        short[] bandRange = null;
        try {
            bandRange = this.mEqualizer.getBandLevelRange();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        if (bandRange == null || bandRange.length < 2) {
            backToHome();
            return;
        }
        this.minEQLevel = bandRange[0];
        this.maxEQLevel = bandRange[1];
        if (isFirstTime) {
            for (short i = (short) 0; i < this.bands; i = (short) (i + 1)) {
                final short band = i;
                View mView = LayoutInflater.from(this).inflate(R.layout.item_equalizer, null);
                TextView minDbTextView = (TextView) mView.findViewById(R.id.tv_min_db);
                minDbTextView.setText((this.minEQLevel / 100) + " dB");
                minDbTextView.setTypeface(this.mTypefaceNormal);
                TextView maxDbTextView = (TextView) mView.findViewById(R.id.tv_max_db);
                maxDbTextView.setText((this.maxEQLevel / 100) + " dB");
                maxDbTextView.setTypeface(this.mTypefaceNormal);
                VerticalSeekBar mSliderView = (VerticalSeekBar) mView.findViewById(R.id.mySeekBar);
                mSliderView.setMax(this.maxEQLevel - this.minEQLevel);
                mSliderView.setProgress(this.mEqualizer.getBandLevel(band) - this.minEQLevel);
                Drawable mDrawable = mSliderView.getProgressDrawable();
                if (mDrawable != null) {
                    if (mDrawable instanceof LayerDrawable) {
                        LayerDrawable mLayerDrawable = (LayerDrawable) mDrawable;
                        if (mLayerDrawable.findDrawableByLayerId(16908288) != null) {
                            mDrawable.setColorFilter(getResources().getColor(R.color.main_color_hint_text), Mode.SRC_ATOP);
                        }
                        Drawable mDrawableProgress = mLayerDrawable.findDrawableByLayerId(16908301);
                        if (mDrawableProgress != null) {
                            mDrawableProgress.setColorFilter(getResources().getColor(R.color.colorAccent), Mode.SRC_ATOP);
                        }
                        mSliderView.postInvalidate();
                    } else if (mDrawable instanceof StateListDrawable) {
                        StateListDrawable mStateListDrawable = (StateListDrawable) mDrawable;
                        try {
                            int[] currentState = new int[]{16842910};
                            int[] currentState1 = new int[]{-16842910};
                            Method getStateDrawableIndex = StateListDrawable.class.getMethod("getStateDrawableIndex", new Class[]{int[].class});
                            Method getStateDrawable = StateListDrawable.class.getMethod("getStateDrawable", new Class[]{Integer.TYPE});
                            int index = ((Integer) getStateDrawableIndex.invoke(mStateListDrawable, new Object[]{currentState})).intValue();
                            int index1 = ((Integer) getStateDrawableIndex.invoke(mStateListDrawable, new Object[]{currentState1})).intValue();
                            ((Drawable) getStateDrawable.invoke(mStateListDrawable, new Object[]{Integer.valueOf(index)})).setColorFilter(getResources().getColor(R.color.colorAccent), Mode.SRC_ATOP);
                            ((Drawable) getStateDrawable.invoke(mStateListDrawable, new Object[]{Integer.valueOf(index1)})).setColorFilter(getResources().getColor(R.color.main_color_hint_text), Mode.SRC_ATOP);
                        } catch (NoSuchMethodException e3) {
                            e3.printStackTrace();
                        } catch (InvocationTargetException e4) {
                            e4.printStackTrace();
                        } catch (IllegalAccessException e5) {
                            e5.printStackTrace();
                        }
                        mSliderView.postInvalidate();
                    }
                    Drawable mThumb = getResources().getDrawable(R.drawable.thumb_default);
                    mThumb.setColorFilter(getResources().getColor(R.color.colorAccent), Mode.SRC_ATOP);
                    mSliderView.setThumb(mThumb);
                }
                mSliderView.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if (fromUser) {
                            try {
                                EqualizerActivity.this.mEqualizer.setBandLevel(band, (short) (EqualizerActivity.this.minEQLevel + progress));
                                EqualizerActivity.this.saveEqualizerParams();
                                EqualizerActivity.this.mSpinnerPresents.setSelection(EqualizerActivity.this.mLists.length - 1);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    public void onStartTrackingTouch(SeekBar seekBar) {
                    }

                    public void onStopTrackingTouch(SeekBar seekBar) {
                    }
                });
                this.listSeekBars.add(mSliderView);
                this.mLayoutBands.addView(mView, new LayoutParams(-2, -2));
            }
        }
        DBLog.LOGD(TAG, "==========>deltaTime=" + String.valueOf((System.currentTimeMillis() - pivotTime) / 1000));
    }

    private void setUpBassVirtualizer() {
        BassBoost mBassBoost = MusicDataMng.getInstance().getBassBoost();
        if (mBassBoost == null) {
            mBassBoost = new BassBoost(0, this.mMediaPlayer.getAudioSessionId());
        }
        try {
            if (mBassBoost.getStrengthSupported()) {
                Virtualizer mVirtualizer = MusicDataMng.getInstance().getVirtualizer();
                if (mVirtualizer == null) {
                    mVirtualizer = new Virtualizer(0, this.mMediaPlayer.getAudioSessionId());
                }
                if (mVirtualizer.getStrengthSupported()) {
                    short mCurrentShort = YPYSettingManager.getBassBoost(this);
                    mBassBoost.setStrength((short) (mCurrentShort * 10));
                    mBassBoost.setEnabled(YPYSettingManager.getEqualizer(this));
                    short mCurrentVir = YPYSettingManager.getVirtualizer(this);
                    mVirtualizer.setStrength((short) (mCurrentVir * 10));
                    mVirtualizer.setEnabled(YPYSettingManager.getEqualizer(this));
                    this.mCircularBass.setProgress(mCurrentShort);
                    this.mCircularVir.setProgress(mCurrentVir);
                    this.mBassBoost = mBassBoost;
                    this.mVirtualizer = mVirtualizer;
                    return;
                }
                this.mLayoutBassVir.setVisibility(View.GONE);
                return;
            }
            this.mLayoutBassVir.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
            this.mLayoutBassVir.setVisibility(View.GONE);
        }
    }

    private void setUpPresetName() {
        if (this.mLists == null) {
            if (this.mEqualizer != null) {
                short numberPreset = this.mEqualizer.getNumberOfPresets();
                if (numberPreset > (short) 0) {
                    this.mLists = new String[(numberPreset + 1)];
                    for (short i = (short) 0; i < numberPreset; i = (short) (i + 1)) {
                        this.mLists[i] = this.mEqualizer.getPresetName(i);
                        Log.d("equilizer",this.mEqualizer.getPresetName(i));
                    }
                    this.mLists[numberPreset] = getString(R.string.title_custom);
                    PresetDataAdapter dataAdapter = new PresetDataAdapter(this, R.layout.item_preset_name, this.mLists, this.mTypefaceNormal);
                    this.mSpinnerPresents.setAdapter(dataAdapter);
                    dataAdapter.setPresetListener(new C13815());
                    this.mSpinnerPresents.setOnItemSelectedListener(new OnItemChange());
                    return;
                }
                this.mSpinnerPresents.setVisibility(View.INVISIBLE);
                return;
            }
            this.mSpinnerPresents.setVisibility(View.INVISIBLE);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        backToHome();
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                backToHome();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void backToHome() {
        finish();
    }

    protected void onDestroy() {
        super.onDestroy();
        if (this.listSeekBars != null) {
            this.listSeekBars.clear();
            this.listSeekBars = null;
        }
        if (this.isCreateLocal) {
            try {
                if (this.mMediaPlayer != null) {
                    this.mMediaPlayer.release();
                    this.mMediaPlayer = null;
                }
                if (this.mEqualizer != null) {
                    this.mEqualizer.release();
                    this.mEqualizer = null;
                }
                if (this.mBassBoost != null) {
                    this.mBassBoost.release();
                    this.mBassBoost = null;
                }
                if (this.mVirtualizer != null) {
                    this.mVirtualizer.release();
                    this.mVirtualizer = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void onPlayerUpdateState(boolean isPlay) {
        if (isPlay) {
            setUpEffects(false);
        }
    }

    public void onPlayerStop() {
    }

    public void onPlayerLoading() {
        showProgressDialog();
    }

    public void onPlayerStopLoading() {
        dimissProgressDialog();
    }

    public void onPlayerUpdatePos(int currentPos) {
    }

    public void onPlayerError() {
        dimissProgressDialog();
        backToHome();
    }

    public void onPlayerUpdateStatus() {
    }

    private synchronized void setUpEffects(boolean isFirstTime) {
        try {
            this.mMediaPlayer = MusicDataMng.getInstance().getPlayer();
            if (this.mMediaPlayer == null || !this.mMediaPlayer.isPlaying()) {
                this.isCreateLocal = true;
                this.mMediaPlayer = new MediaPlayer();
            }
            setupEqualizerFxAndUI(isFirstTime);
            setUpBassVirtualizer();
            setUpPresetName();
            if (isFirstTime) {
                startCheckEqualizer();
                setUpEqualizerParams();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
