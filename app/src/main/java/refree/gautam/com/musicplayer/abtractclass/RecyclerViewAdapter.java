package refree.gautam.com.musicplayer.abtractclass;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

public abstract class RecyclerViewAdapter extends Adapter<ViewHolder> {
    public static final String TAG = RecyclerViewAdapter.class.getSimpleName();
    public static final int TYPE_HEADER_VIEW = -1;
    public boolean isHasHeader;
    public Context mContext;
    private View mHeaderView;
    public ArrayList<? extends Object> mListObjects;

    public class ViewHeaderHolder extends ViewHolder {
        public ViewHeaderHolder(View convertView) {
            super(convertView);
        }
    }

    public abstract void onBindNormalViewHolder(ViewHolder viewHolder, int i);

    public abstract ViewHolder onCreateNormalViewHolder(ViewGroup viewGroup, int i);

    public RecyclerViewAdapter(Context mContext, ArrayList<? extends Object> listObjects) {
        this.mContext = mContext;
        this.mListObjects = listObjects;
    }

    public RecyclerViewAdapter(Context mContext, ArrayList<? extends Object> listObjects, View mHeaderView) {
        this.mContext = mContext;
        this.mListObjects = listObjects;
        this.isHasHeader = true;
        this.mHeaderView = mHeaderView;
    }

    public void onBindViewHolder(ViewHolder holder, int position) {
        if (!this.isHasHeader) {
            onBindNormalViewHolder(holder, position);
        } else if (getItemViewType(position) != -1) {
            onBindNormalViewHolder(holder, position - 1);
        }
    }

    public int getItemCount() {
        int size = this.mListObjects != null ? this.mListObjects.size() : 0;
        if (this.isHasHeader) {
            return size + 1;
        }
        return size;
    }

    public int getItemViewType(int position) {
        if (this.isHasHeader && position == 0) {
            return -1;
        }
        return super.getItemViewType(position);
    }

    public ViewHolder onCreateViewHolder(ViewGroup v, int viewType) {
        if (viewType == -1) {
            return new ViewHeaderHolder(this.mHeaderView);
        }
        return onCreateNormalViewHolder(v, viewType);
    }

    public void setListObjects(ArrayList<? extends Object> mListObjects, boolean isDestroyOldData) {
        if (mListObjects != null) {
            if (this.mListObjects != null && isDestroyOldData) {
                this.mListObjects.clear();
                this.mListObjects = null;
            }
            this.mListObjects = mListObjects;
            notifyDataSetChanged();
        }
    }

    public ArrayList<? extends Object> getListObjects() {
        return this.mListObjects;
    }
}
