package refree.gautam.com.musicplayer.Fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;

import refree.gautam.com.musicplayer.Adapter.TrackDataAdapter;
import refree.gautam.com.musicplayer.InterFace.IXMusicConstants;
import refree.gautam.com.musicplayer.Models.ConfigureModel;
import refree.gautam.com.musicplayer.Models.TrackModel;
import refree.gautam.com.musicplayer.R;
import refree.gautam.com.musicplayer.MainActivity;
import refree.gautam.com.musicplayer.abtractclass.fragment.DBFragment;
import refree.gautam.com.musicplayer.executor.DBExecutorSupplier;
import refree.gautam.com.musicplayer.view.CircularProgressBar;

public class FragmentMyMusic extends DBFragment implements IXMusicConstants {
    public static final String TAG = FragmentMyMusic.class.getSimpleName();
    private boolean isSearching;
    private MainActivity mContext;
    private Handler mHandler = new Handler();
    private ArrayList<TrackModel> mListTracks;
    private CircularProgressBar mProgressBar;
    private RecyclerView mRecyclerViewTrack;
    private TrackDataAdapter mTrackDataAdapter;
    private TextView mTvNoResult;
    private int mTypeUI;

    class C14461 implements Runnable {
        C14461() {
        }

        public void run() {
            ArrayList<TrackModel> mTracks = FragmentMyMusic.this.mContext.mTotalMng.getListLibraryTrackObjects();
            if (mTracks == null) {
                FragmentMyMusic.this.mContext.mTotalMng.readLibraryTrack(FragmentMyMusic.this.mContext);
                mTracks = FragmentMyMusic.this.mContext.mTotalMng.getListLibraryTrackObjects();
            }
            final ArrayList<TrackModel> mListLibrary = FragmentMyMusic.this.mContext.mTotalMng.getAllTrackWithAdmob(FragmentMyMusic.this.mContext, mTracks, FragmentMyMusic.this.mTypeUI);
            FragmentMyMusic.this.mContext.runOnUiThread(new Runnable() {
                public void run() {
                    FragmentMyMusic.this.mProgressBar.setVisibility(View.GONE);
                    FragmentMyMusic.this.setUpInfo(mListLibrary);
                }
            });
        }
    }

    class C14493 implements TrackDataAdapter.OnTrackListener {
        C14493() {
        }

        public void onListenTrack(TrackModel mTrackObject) {
            FragmentMyMusic.this.mContext.hiddenKeyBoardForSearchView();
            FragmentMyMusic.this.mContext.startPlayingList(mTrackObject, FragmentMyMusic.this.mContext.mTotalMng.getListLibraryTrackObjects());
        }

        public void onShowMenu(View mView, TrackModel mTrackObject) {
            FragmentMyMusic.this.mContext.showPopupMenu(mView, mTrackObject);
        }
    }

    public View onInflateLayout(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_recyclerview, container, false);
    }

    public void findView() {
        this.mContext = (MainActivity) getActivity();
        this.mTvNoResult = (TextView) this.mRootView.findViewById(R.id.tv_no_result);
        this.mTvNoResult.setTypeface(this.mContext.mTypefaceNormal);
        this.mRecyclerViewTrack = (RecyclerView) this.mRootView.findViewById(R.id.list_datas);
        this.mProgressBar = (CircularProgressBar) this.mRootView.findViewById(R.id.progressBar1);
        ConfigureModel configureModel = this.mContext.mTotalMng.getConfigureModel();
        this.mTypeUI = configureModel != null ? configureModel.getTypeMyMusic() : 1;
        if (this.mTypeUI == 1) {
            this.mContext.setUpRecyclerViewAsListView(this.mRecyclerViewTrack, null);
        } else {
            this.mContext.setUpRecyclerViewAsGridView(this.mRecyclerViewTrack, 2);
        }
        if (isFirstInTab()) {
            startLoadData();
        }
    }

    public void startLoadData() {
        super.startLoadData();
        if ((!isLoadingData() || this.isSearching) && this.mContext != null) {
            setLoadingData(true);
            this.isSearching = false;
            startGetData();
        }
    }

    private void startGetData() {
        this.mTvNoResult.setVisibility(View.GONE);
        this.mProgressBar.setVisibility(View.VISIBLE);
        DBExecutorSupplier.getInstance().forBackgroundTasks().execute(new C14461());
    }

    public void startSearchData(final String keyword) {
        if (this.mContext != null && this.mRecyclerViewTrack != null) {
            this.isSearching = true;
            DBExecutorSupplier.getInstance().forLightWeightBackgroundTasks().execute(new Runnable() {
                public void run() {
                    final ArrayList<TrackModel> mTracks = FragmentMyMusic.this.mContext.mTotalMng.startSearchSong(keyword);
                    FragmentMyMusic.this.mContext.runOnUiThread(new Runnable() {
                        public void run() {
                            FragmentMyMusic.this.setUpInfo(mTracks);
                        }
                    });
                }
            });
        }
    }

    private void setUpInfo(ArrayList<TrackModel> mListTracks) {
        this.mRecyclerViewTrack.setAdapter(null);
        if (this.mListTracks != null) {
            this.mListTracks.clear();
            this.mListTracks = null;
        }
        this.mListTracks = mListTracks;
        if (mListTracks != null && mListTracks.size() > 0) {
            this.mTrackDataAdapter = new TrackDataAdapter(this.mContext, mListTracks, this.mContext.mTypefaceBold, this.mContext.mTypefaceLight, this.mTypeUI);
            this.mRecyclerViewTrack.setAdapter(this.mTrackDataAdapter);
            this.mTrackDataAdapter.setOnTrackListener(new C14493());
        }
        updateInfo();
    }

    public void onResume() {
        super.onResume();
        updateInfo();
    }

    private void updateInfo() {
        int i = 0;
        if (this.mTvNoResult != null) {
            boolean b;
            if (this.mListTracks == null || this.mListTracks.size() <= 0) {
                b = false;
            } else {
                b = true;
            }
            TextView textView = this.mTvNoResult;
            if (b) {
                i = 8;
            }
            textView.setVisibility(i);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        this.mHandler.removeCallbacksAndMessages(null);
        if (this.mListTracks != null) {
            this.mListTracks.clear();
            this.mListTracks = null;
        }
    }

    public void notifyData() {
        startGetData();
    }
}
