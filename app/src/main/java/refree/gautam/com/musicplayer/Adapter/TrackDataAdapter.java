package refree.gautam.com.musicplayer.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.android.gms.ads.AdRequest.Builder;
import com.google.android.gms.ads.NativeExpressAdView;
import java.util.ArrayList;
import java.util.Locale;

import refree.gautam.com.musicplayer.BaseActivity.FragmentActivity;
import refree.gautam.com.musicplayer.InterFace.IXMusicConstants;
import refree.gautam.com.musicplayer.Models.TrackModel;
import refree.gautam.com.musicplayer.R;
import refree.gautam.com.musicplayer.abtractclass.RecyclerViewAdapter;
import refree.gautam.com.musicplayer.imageloader.GlideImageLoader;
import refree.gautam.com.musicplayer.utils.FBAds;
import refree.gautam.com.musicplayer.utils.StringUtils;
import refree.gautam.com.musicplayer.view.MaterialIconView;

public class TrackDataAdapter extends RecyclerViewAdapter implements IXMusicConstants {
    public static final String TAG = TrackDataAdapter.class.getSimpleName();
    private LayoutInflater mInflater;
    private int mType;
    private Typeface mTypefaceBold;
    private Typeface mTypefaceLight;
    private OnTrackListener onTrackListener;

    public interface OnTrackListener {
        void onListenTrack(TrackModel trackModel);

        void onShowMenu(View view, TrackModel trackModel);
    }

    public class TrackHolder extends ViewHolder {
        public CardView mCardView;
        public MaterialIconView mImgMenu;
        public ImageView mImgSongs;
        public View mLayoutRoot;
        public TextView mTvSinger;
        public TextView mTvSongName;

        public TrackHolder(View convertView) {
            super(convertView);
            this.mImgSongs = (ImageView) convertView.findViewById(R.id.img_songs);
            this.mImgMenu = (MaterialIconView) convertView.findViewById(R.id.img_menu);
            this.mTvSongName = (TextView) convertView.findViewById(R.id.tv_song);
            this.mTvSinger = (TextView) convertView.findViewById(R.id.tv_singer);
            this.mCardView = (CardView) convertView.findViewById(R.id.card_view);
            this.mLayoutRoot = convertView.findViewById(R.id.layout_root);
            this.mTvSongName.setTypeface(TrackDataAdapter.this.mTypefaceBold);
            this.mTvSinger.setTypeface(TrackDataAdapter.this.mTypefaceLight);
        }
    }

    public class ViewNativeHolder extends ViewHolder {
        public RelativeLayout mRootLayoutAds;

        public ViewNativeHolder(View convertView) {
            super(convertView);
            this.mRootLayoutAds = (RelativeLayout) convertView.findViewById(R.id.layout_ad_root);
        }
    }

    public TrackDataAdapter(FragmentActivity mContext, ArrayList<TrackModel> mListObjects, Typeface mTypefaceBold, Typeface mTypefaceLight, int type) {
        super(mContext, mListObjects);
        this.mTypefaceBold = mTypefaceBold;
        this.mListObjects = mListObjects;
        this.mTypefaceLight = mTypefaceLight;
        this.mType = type;
        this.mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setOnTrackListener(OnTrackListener onTrackListener) {
        this.onTrackListener = onTrackListener;
    }

    public void onBindNormalViewHolder(ViewHolder holder, int position) {
        final TrackModel mTrackObject = (TrackModel) this.mListObjects.get(position);
        if (holder instanceof TrackHolder) {
            final TrackHolder mTrackHolder = (TrackHolder) holder;
            mTrackHolder.mTvSongName.setText(mTrackObject.getTitle());
            String author = mTrackObject.getAuthor();
            if (StringUtils.isEmptyString(author) || author.toLowerCase(Locale.US).contains(IXMusicConstants.PREFIX_UNKNOWN)) {
                author = this.mContext.getString(R.string.title_unknown);
            }
            mTrackHolder.mTvSinger.setText(author);
            String artwork = mTrackObject.getArtworkUrl();
            Log.d("artwork",String.valueOf(TextUtils.isEmpty(artwork)));
            if (TextUtils.isEmpty(artwork)) {
                Uri mUri = mTrackObject.getURI();
                Log.d("uri1",String.valueOf(mUri));
                if (mUri != null) {
                    GlideImageLoader.displayImageFromMediaStore(this.mContext, mTrackHolder.mImgSongs, mUri, R.drawable.ic_disk);
                } else {
                    mTrackHolder.mImgSongs.setImageResource(R.drawable.ic_rect_music_default);
                }
            } else {
                GlideImageLoader.displayImage(this.mContext, mTrackHolder.mImgSongs, artwork, (int) R.drawable.ic_rect_music_default);
            }


            if (mTrackHolder.mCardView != null) {
                Log.d("track","card");
                mTrackHolder.mCardView.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        if (TrackDataAdapter.this.onTrackListener != null) {
                            Log.d("track","card_call");
                            TrackDataAdapter.this.onTrackListener.onListenTrack(mTrackObject);
                            Log.d(TAG,"Load ad");
                            FBAds.showFbInterstitial(mContext);
                        }
                    }
                });
            } else {
                mTrackHolder.mLayoutRoot.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        if (TrackDataAdapter.this.onTrackListener != null) {
                            TrackDataAdapter.this.onTrackListener.onListenTrack(mTrackObject);
                            Log.d(TAG,"Load ad");
                            FBAds.showFbInterstitial(mContext);
                        }
                    }
                });
            }
            mTrackHolder.mImgMenu.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    if (TrackDataAdapter.this.onTrackListener != null) {
                        TrackDataAdapter.this.onTrackListener.onShowMenu(mTrackHolder.mImgMenu, mTrackObject);
                    }
                }
            });
        } else if (holder instanceof ViewNativeHolder) {
            ViewNativeHolder mHolder = (ViewNativeHolder) holder;
            mHolder.mRootLayoutAds.removeAllViews();
            NativeExpressAdView mAdView;
            if (mTrackObject.getNativeExpressAdView() == null) {
                mAdView = (NativeExpressAdView) LayoutInflater.from(this.mContext).inflate(R.layout.item_native, null);
                mTrackObject.setNativeExpressAdView(mAdView);
                mAdView.loadAd(new Builder().addTestDevice("553BA7A9C0A22C16AC4F04E02467018E").build());
            } else {
                mAdView = mTrackObject.getNativeExpressAdView();
                if (mAdView.getParent() != null) {
                    ((ViewGroup) mAdView.getParent()).removeAllViews();
                }
            }
            mHolder.mRootLayoutAds.addView(mTrackObject.getNativeExpressAdView());
        }
        holder.itemView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    public ViewHolder onCreateNormalViewHolder(ViewGroup v, int viewType) {
        if (viewType == 0) {
            return new TrackHolder(this.mInflater.inflate(this.mType == 2 ? R.layout.item_grid_track : R.layout.item_list_track, v, false));
        } else if (viewType == 1) {
            return new ViewNativeHolder(this.mInflater.inflate(R.layout.item_native_ads, v, false));
        } else {
            return null;
        }
    }

    public int getItemViewType(int position) {
        if (((TrackModel) this.mListObjects.get(position)).isNativeAds()) {
            return 1;
        }
        return 0;
    }
}
