package refree.gautam.com.musicplayer.playservice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;

import java.util.ArrayList;

import refree.gautam.com.musicplayer.DataManger.MusicDataMng;
import refree.gautam.com.musicplayer.DataManger.TotalDataManager;
import refree.gautam.com.musicplayer.InterFace.IXMusicConstants;
import refree.gautam.com.musicplayer.InterFace.YPYSettingManager;
import refree.gautam.com.musicplayer.Models.TrackModel;
import refree.gautam.com.musicplayer.utils.StringUtils;

public class MusicIntentReceiver extends BroadcastReceiver implements IYPYMusicConstant {
    public static final String TAG = MusicIntentReceiver.class.getSimpleName();
    private ArrayList<TrackModel> mListTrack;

    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            String action = intent.getAction();
            if (!StringUtils.isEmptyString(action)) {
                this.mListTrack = MusicDataMng.getInstance().getListPlayingTrackObjects();
                String packageName = context.getPackageName();
                if (action.equals("android.media.AUDIO_BECOMING_NOISY")) {
                    startService(context, IYPYMusicConstant.ACTION_PAUSE);
                } else if (action.equals(packageName + IYPYMusicConstant.ACTION_NEXT)) {
                    startService(context, IYPYMusicConstant.ACTION_NEXT);
                } else if (action.equals(packageName + IYPYMusicConstant.ACTION_TOGGLE_PLAYBACK)) {
                    startService(context, IYPYMusicConstant.ACTION_TOGGLE_PLAYBACK);
                } else if (action.equals(packageName + IYPYMusicConstant.ACTION_PREVIOUS)) {
                    startService(context, IYPYMusicConstant.ACTION_PREVIOUS);
                } else if (action.equals(packageName + IYPYMusicConstant.ACTION_STOP)) {
                    startService(context, IYPYMusicConstant.ACTION_STOP);
                    if (!YPYSettingManager.getOnline(context)) {
                        try {
                            MusicDataMng.getInstance().onDestroy();
                            TotalDataManager.getInstance().onDestroy();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (action.equals(packageName + IYPYMusicConstant.ACTION_SHUFFLE)) {
                    startService(context, IYPYMusicConstant.ACTION_SHUFFLE, !YPYSettingManager.getShuffle(context));
                } else if (!action.equals("android.intent.action.MEDIA_BUTTON")) {
                } else {
                    if (this.mListTrack == null || this.mListTrack.size() == 0) {
                        MusicDataMng.getInstance().onDestroy();
                        startService(context, IYPYMusicConstant.ACTION_STOP);
                        return;
                    }
                    KeyEvent keyEvent = (KeyEvent) intent.getExtras().get("android.intent.extra.KEY_EVENT");
                    if (keyEvent.getAction() == 0) {
                        switch (keyEvent.getKeyCode()) {
                            case 79:
                            case 85:
                                if (YPYSettingManager.getOnline(context)) {
                                    startService(context, IYPYMusicConstant.ACTION_TOGGLE_PLAYBACK);
                                    return;
                                } else {
                                    startService(context, IYPYMusicConstant.ACTION_STOP);
                                    return;
                                }
                            case 86:
                                startService(context, IYPYMusicConstant.ACTION_STOP);
                                return;
                            case 87:
                                startService(context, IYPYMusicConstant.ACTION_NEXT);
                                return;
                            case 88:
                                startService(context, IYPYMusicConstant.ACTION_PREVIOUS);
                                return;
                            case 126 /*126*/:
                                startService(context, IYPYMusicConstant.ACTION_PLAY);
                                return;
                            case 127 /*127*/:
                                if (YPYSettingManager.getOnline(context)) {
                                    startService(context, IYPYMusicConstant.ACTION_PAUSE);
                                    return;
                                } else {
                                    startService(context, IYPYMusicConstant.ACTION_STOP);
                                    return;
                                }
                            default:
                                return;
                        }
                    }
                }
            }
        }
    }

    private void startService(Context context, String action) {
        Intent mIntent1 = new Intent(context, MusicService.class);
        mIntent1.setAction(IXMusicConstants.PREFIX_ACTION + action);
        context.startService(mIntent1);
    }

    private void startService(Context context, String action, boolean value) {
        Intent mIntent1 = new Intent(context, MusicService.class);
        mIntent1.setAction(IXMusicConstants.PREFIX_ACTION + action);
        mIntent1.putExtra(IYPYMusicConstant.KEY_VALUE, value);
        context.startService(mIntent1);
    }
}
