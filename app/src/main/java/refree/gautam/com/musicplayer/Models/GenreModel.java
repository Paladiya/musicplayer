package refree.gautam.com.musicplayer.Models;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

public class GenreModel {
    @SerializedName("id")
    private String id;
    @SerializedName("img")
    private String img;
    @SerializedName("keyword")
    private String keyword;
    private transient ArrayList<TrackModel> listTrackObjects;
    @SerializedName("name")
    private String name;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<TrackModel> getListTrackObjects() {
        return this.listTrackObjects;
    }

    public void setListTrackObjects(ArrayList<TrackModel> listTrackObjects) {
        this.listTrackObjects = listTrackObjects;
    }

    public String getKeyword() {
        return this.keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getImg() {
        return this.img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
