package refree.gautam.com.musicplayer;

import android.graphics.BitmapFactory;
import android.provider.MediaStore;
import android.support.design.widget.TabLayout;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import refree.gautam.com.musicplayer.Adapter.SuggestionDataAdapter;
import refree.gautam.com.musicplayer.BaseActivity.FragmentActivity;
import refree.gautam.com.musicplayer.DataManger.MusicDataMng;
import refree.gautam.com.musicplayer.DataManger.TotalDataManager;
import refree.gautam.com.musicplayer.DataManger.XMLParsingData;
import refree.gautam.com.musicplayer.Fragment.FragmentDetailTracks;
import refree.gautam.com.musicplayer.Fragment.FragmentGenre;
import refree.gautam.com.musicplayer.Fragment.FragmentMyMusic;
import refree.gautam.com.musicplayer.Fragment.FragmentPlayerListenMusic;
import refree.gautam.com.musicplayer.Fragment.FragmentPlaylist;
import refree.gautam.com.musicplayer.Fragment.FragmentSearchTrack;
import refree.gautam.com.musicplayer.Fragment.FragmentTopChart;
import refree.gautam.com.musicplayer.InterFace.IDBMusicPlayerListener;
import refree.gautam.com.musicplayer.InterFace.IDBSearchViewInterface;
import refree.gautam.com.musicplayer.InterFace.IXMusicConstants;
import refree.gautam.com.musicplayer.InterFace.YPYSettingManager;
import refree.gautam.com.musicplayer.Models.GenreModel;
import refree.gautam.com.musicplayer.Models.PlaylistModel;
import refree.gautam.com.musicplayer.Models.TrackModel;
import refree.gautam.com.musicplayer.abtractclass.fragment.DBFragment;
import refree.gautam.com.musicplayer.abtractclass.fragment.DBFragmentAdapter;
import refree.gautam.com.musicplayer.executor.DBExecutorSupplier;
import refree.gautam.com.musicplayer.imageloader.GlideImageLoader;
import refree.gautam.com.musicplayer.playservice.IYPYMusicConstant;
import refree.gautam.com.musicplayer.utils.ApplicationUtils;
import refree.gautam.com.musicplayer.utils.DBLog;
import refree.gautam.com.musicplayer.utils.DownloadUtils;
import refree.gautam.com.musicplayer.utils.FBAds;
import refree.gautam.com.musicplayer.utils.ShareActionUtils;
import refree.gautam.com.musicplayer.utils.StringUtils;
import refree.gautam.com.musicplayer.view.CircularProgressBar;
import refree.gautam.com.musicplayer.view.DBViewPager;

import android.content.Context;
import android.content.Intent;
import android.database.MatrixCursor;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetBehavior.BottomSheetCallback;
import android.support.design.widget.TabLayout.OnTabSelectedListener;
import android.support.design.widget.TabLayout.Tab;
import android.support.design.widget.TabLayout.TabLayoutOnPageChangeListener;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.SearchView.OnSuggestionListener;
import android.text.Html;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.io.InputStream;
import java.util.Iterator;
import java.util.Locale;

public class MainActivity extends FragmentActivity implements FragmentActivity.INetworkListener, IDBMusicPlayerListener, OnClickListener {

    public static final String TAG = MainActivity.class.getSimpleName();
    private AppBarLayout mAppBarLayout;
    private BottomSheetBehavior<View> mBottomSheetBehavior;
    private ImageView mBtnSmallNext;
    private ImageView mBtnSmallPlay;
    private ImageView mBtnSmallPrev;
    private String[] mColumns;
    private MatrixCursor mCursor;
    private FragmentGenre mFragmentGenre;
    private FragmentPlayerListenMusic mFragmentMusic;
    private FragmentMyMusic mFragmentMyMusic;
    private FragmentPlaylist mFragmentPlaylist;
    private FragmentTopChart mFragmentTopChart;
    private Drawable mHomeIconDrawable;
    private CircleImageView mImgSmallSong;
    private FrameLayout mLayoutContainer;
    private LinearLayout mLayoutControlMusic;
    private FrameLayout mLayoutDetailListenMusic;
    private View mLayoutListenMusic;
    private ArrayList<Fragment> mListHomeFragments = new ArrayList();
    private ArrayList<String> mListSuggestionStr;
    private Menu mMenu;
    private CircularProgressBar mProgressLoadingMusic;
    private int mStartHeight;
    private DBFragmentAdapter mTabAdapters;
    private TabLayout mTabLayout;
    private Object[] mTempData;
    private TextView mTvSmallSinger;
    private TextView mTvSmallSong;
    private DBViewPager mViewpager;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_app_bar_main);
        this.mHomeIconDrawable = getResources().getDrawable(R.drawable.ic_home_24dp);
        this.mHomeIconDrawable.setColorFilter(this.mContentActionColor, Mode.SRC_ATOP);
        setUpActionBar();
        setIsAllowPressMoreToExit(true);
        YPYSettingManager.setOnline(this, true);
        createArrayFragment();
        this.mFragmentMusic = (FragmentPlayerListenMusic) getSupportFragmentManager().findFragmentById(R.id.fragment_listen_music);
        this.mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
        this.mTabLayout.setTabTextColors(getResources().getColor(R.color.tab_text_normal_color), getResources().getColor(R.color.tab_text_focus_color));
        this.mTabLayout.setTabMode(1);
        this.mTabLayout.setTabGravity(0);
        ViewCompat.setElevation(this.mTabLayout, 0.0f);
        this.mAppBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        this.mViewpager = (DBViewPager) findViewById(R.id.view_pager);
        this.mViewpager.setPagingEnabled(true);
        this.mLayoutContainer = (FrameLayout) findViewById(R.id.container);
        findViewById(R.id.img_touch).setOnTouchListener(new C13971());
        this.isNeedProcessOther = true;
        setUpSmallMusicLayout();
        registerMusicPlayerBroadCastReceiver(this);
        if (!ApplicationUtils.isOnline(this)) {
            registerNetworkBroadcastReceiver(this);
        } else if (true) {
            setUpLayoutAdmob();
        }
        checkConfigure();
    }

    class C13971 implements OnTouchListener {
        C13971() {
        }

        public boolean onTouch(View v, MotionEvent event) {
            return true;
        }
    }

    class C13992 implements Runnable {

        class C13981 implements Runnable {
            C13981() {
            }

            public void run() {
                MainActivity.this.dimissProgressDialog();
                MainActivity.this.setUpTab();
                MainActivity.this.showAppRate();
            }
        }

        C13992() {
        }

        public void run() {
            MainActivity.this.mTotalMng.readConfigure(MainActivity.this);
            MainActivity.this.mTotalMng.readGenreData(MainActivity.this);
            MainActivity.this.mTotalMng.readCached(5);
            MainActivity.this.mTotalMng.readPlaylistCached();
            MainActivity.this.mTotalMng.readLibraryTrack(MainActivity.this);
            MainActivity.this.runOnUiThread(new C13981());
        }
    }

    class C14003 implements OnClickListener {
        C14003() {
        }

        public void onClick(View v) {
            if (MainActivity.this.mBottomSheetBehavior.getState() == 4) {
                MainActivity.this.mBottomSheetBehavior.setState(3);
            }
        }
    }

    class C14014 extends BottomSheetCallback {
        public boolean isHidden;
        public float mSlideOffset;

        C14014() {
        }

        public void onStateChanged(View bottomSheet, int newState) {
            if (newState == 3) {
                MainActivity.this.showAppBar(false);
                MainActivity.this.showHeaderMusicPlayer(true);
                Log.d("satate", "3");
                mViewpager.setVisibility(View.GONE);
                mAppBarLayout.setVisibility(View.GONE);

            } else if (newState == 4) {
                this.isHidden = false;
                MainActivity.this.showAppBar(true);
                MainActivity.this.showHeaderMusicPlayer(false);
                Log.d("satate", "4");
                mViewpager.setVisibility(View.VISIBLE);
                mAppBarLayout.setVisibility(View.VISIBLE);
            }
        }

        public void onSlide(View bottomSheet, float slideOffset) {
            if (this.mSlideOffset > 0.0f && slideOffset > this.mSlideOffset && !this.isHidden) {
                MainActivity.this.showAppBar(false);
                this.isHidden = true;
            }
            this.mSlideOffset = slideOffset;
            MainActivity.this.mLayoutControlMusic.setVisibility(View.VISIBLE);
            MainActivity.this.mLayoutDetailListenMusic.setVisibility(View.VISIBLE);
            MainActivity.this.mLayoutControlMusic.setAlpha(IYPYMusicConstant.MAX_VOLUME - slideOffset);
            MainActivity.this.mLayoutDetailListenMusic.setAlpha(slideOffset);
        }
    }

    class C14036 implements OnTabSelectedListener {
        C14036() {
        }

        public void onTabSelected(Tab tab) {
            MainActivity.this.hiddenKeyBoardForSearchView();
            MainActivity.this.mViewpager.setCurrentItem(tab.getPosition());
            ((DBFragment) MainActivity.this.mListHomeFragments.get(tab.getPosition())).startLoadData();
        }

        public void onTabUnselected(Tab tab) {
        }

        public void onTabReselected(Tab tab) {
        }
    }

    public class C14047 implements IDBSearchViewInterface {
        C14047() {
        }

        public void onStartSuggestion(String keyword) {
            if (MainActivity.this.mFragmentMyMusic != null && MainActivity.this.mViewpager.getCurrentItem() == MainActivity.this.mListHomeFragments.indexOf(MainActivity.this.mFragmentMyMusic)) {
                MainActivity.this.mFragmentMyMusic.startSearchData(keyword);
            } else if (!StringUtils.isEmptyString(keyword)) {
                MainActivity.this.startSuggestion(keyword);
            }
        }

        public void onProcessSearchData(String keyword) {
            if (MainActivity.this.mFragmentMyMusic != null && MainActivity.this.mViewpager.getCurrentItem() == MainActivity.this.mListHomeFragments.indexOf(MainActivity.this.mFragmentMyMusic)) {
                MainActivity.this.mFragmentMyMusic.startSearchData(keyword);
            } else if (!StringUtils.isEmptyString(keyword)) {
                MainActivity.this.goToSearchMusic(keyword);
            }
        }

        public void onClickSearchView() {
            if (MainActivity.this.mFragmentMyMusic != null && MainActivity.this.mViewpager.getCurrentItem() != MainActivity.this.mListHomeFragments.indexOf(MainActivity.this.mFragmentMyMusic)) {
                MainActivity.this.searchView.setQuery("", false);
            }
        }

        public void onCloseSearchView() {
        }
    }

    class C14058 implements OnSuggestionListener {
        C14058() {
        }

        public boolean onSuggestionSelect(int position) {
            return false;
        }

        public boolean onSuggestionClick(int position) {
            if (MainActivity.this.mListSuggestionStr != null && MainActivity.this.mListSuggestionStr.size() > 0) {
                MainActivity.this.searchView.setQuery((CharSequence) MainActivity.this.mListSuggestionStr.get(position), false);
                MainActivity.this.goToSearchMusic((String) MainActivity.this.mListSuggestionStr.get(position));
            }
            return false;
        }
    }


    private void checkConfigure() {
        if (this.mTotalMng.getConfigureModel() == null) {
            showProgressDialog();
            DBExecutorSupplier.getInstance().forBackgroundTasks().execute(new C13992());
            return;
        }
        setUpTab();
        showAppRate();
    }

    private void setUpActionBar() {
        setUpCustomizeActionBar();
        removeEvalationActionBar();
        setColorForActionBar(0);
        setActionBarTitle(getResources().getString(R.string.actionbar_text));
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        getSupportActionBar().setIcon(this.mHomeIconDrawable);
    }

    public void onDestroyData() {
        super.onDestroyData();
        try {
            if (MusicDataMng.getInstance().getPlayer() == null) {
                MusicDataMng.getInstance().onDestroy();
                TotalDataManager.getInstance().onDestroy();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (false) {
            startMusicService(IYPYMusicConstant.ACTION_STOP);
        }
    }

    private void setUpSmallMusicLayout() {
        int playId = R.drawable.ic_play_arrow_white_36dp;
        this.mLayoutListenMusic = findViewById(R.id.layout_listen_music);
        this.mBtnSmallPlay = (ImageView) findViewById(R.id.btn_play);
        this.mBtnSmallPlay.setOnClickListener(this);
        this.mBtnSmallPrev = (ImageView) findViewById(R.id.btn_prev);
        this.mBtnSmallPrev.setOnClickListener(this);
        setUpImageViewBaseOnColor(this.mBtnSmallPrev, this.mIconColor, (int) R.drawable.ic_skip_previous_white_36dp, false);
        this.mBtnSmallNext = (ImageView) findViewById(R.id.btn_next);
        this.mBtnSmallNext.setOnClickListener(this);
        setUpImageViewBaseOnColor(this.mBtnSmallNext, this.mIconColor, (int) R.drawable.ic_skip_next_white_36dp, false);
        this.mTvSmallSong = (TextView) findViewById(R.id.tv_song);
        this.mTvSmallSong.setTypeface(this.mTypefaceNormal);
        this.mTvSmallSong.setSelected(true);
        this.mTvSmallSinger = (TextView) findViewById(R.id.tv_singer);
        this.mTvSmallSinger.setTypeface(this.mTypefaceNormal);
        this.mImgSmallSong = (CircleImageView) findViewById(R.id.img_song);
        this.mProgressLoadingMusic = (CircularProgressBar) findViewById(R.id.img_status_loading);
        this.mStartHeight = getResources().getDimensionPixelOffset(R.dimen._60sdp);
        this.mLayoutControlMusic = (LinearLayout) findViewById(R.id.layout_music);
        this.mLayoutDetailListenMusic = (FrameLayout) findViewById(R.id.play_container);
        this.mLayoutControlMusic.setOnClickListener(new C14003());
        this.mBottomSheetBehavior = BottomSheetBehavior.from(this.mLayoutListenMusic);
        this.mBottomSheetBehavior.setPeekHeight(this.mStartHeight);
        this.mBottomSheetBehavior.setState(4);
        this.mBottomSheetBehavior.setBottomSheetCallback(new C14014());
        if (MusicDataMng.getInstance().isPrepaireDone()) {
            showLayoutListenMusic(true);
            TrackModel mCurrentTrack = MusicDataMng.getInstance().getCurrentTrackObject();
            if (mCurrentTrack != null) {
                updateInfoOfPlayTrack(mCurrentTrack);
            }
            if (MusicDataMng.getInstance().isPlayingMusic()) {
                playId = R.drawable.ic_pause_white_36dp;
            }
            setUpImageViewBaseOnColor(this.mBtnSmallPlay, this.mIconColor, playId, false);
            return;
        }
        showLayoutListenMusic(false);
        setUpImageViewBaseOnColor(this.mBtnSmallPlay, this.mIconColor, (int) R.drawable.ic_play_arrow_white_36dp, false);
        if (this.mFragmentMusic != null) {
            this.mFragmentMusic.setUpBackground();
        }
    }

    private void showHeaderMusicPlayer(boolean b) {
        int i = 0;
        this.mLayoutControlMusic.setVisibility(!b ? View.VISIBLE : View.GONE);
        FrameLayout frameLayout = this.mLayoutDetailListenMusic;
        if (!b) {
            i = 4;
        }
        frameLayout.setVisibility(i);
        if (b && this.mFragmentMusic != null) {
            this.mFragmentMusic.setUpBackground();
        }
    }

    public void showAppBar(boolean b) {
        this.mAppBarLayout.setExpanded(b);
    }

    private void setUpTab() {

        this.mTabLayout.addTab(this.mTabLayout.newTab().setText((int) R.string.title_tab_top_charts));

        this.mTabLayout.addTab(this.mTabLayout.newTab().setText((int) R.string.title_tab_discover));

        this.mTabLayout.addTab(this.mTabLayout.newTab().setText((int) R.string.title_tab_my_music));
        this.mTabLayout.addTab(this.mTabLayout.newTab().setText((int) R.string.title_tab_my_playlist));
        setTypefaceForTab(this.mTabLayout, this.mTypefaceBold);

        this.mFragmentTopChart = (FragmentTopChart) Fragment.instantiate(this, FragmentTopChart.class.getName(), new Bundle());
        this.mListHomeFragments.add(this.mFragmentTopChart);
        this.mFragmentGenre = (FragmentGenre) Fragment.instantiate(this, FragmentGenre.class.getName(), new Bundle());
        this.mListHomeFragments.add(this.mFragmentGenre);

        this.mFragmentMyMusic = (FragmentMyMusic) Fragment.instantiate(this, FragmentMyMusic.class.getName(), new Bundle());
        this.mListHomeFragments.add(this.mFragmentMyMusic);
        if (!ApplicationUtils.isOnline(this) || this.mFragmentTopChart == null) {
            this.mFragmentMyMusic.setFirstInTab(true);
        } else {
            this.mFragmentTopChart.setFirstInTab(true);
        }
        this.mFragmentPlaylist = (FragmentPlaylist) Fragment.instantiate(this, FragmentPlaylist.class.getName(), new Bundle());
        this.mListHomeFragments.add(this.mFragmentPlaylist);
        this.mTabAdapters = new DBFragmentAdapter(getSupportFragmentManager(), this.mListHomeFragments);
        this.mViewpager.setAdapter(this.mTabAdapters);
        this.mViewpager.setOffscreenPageLimit(this.mListHomeFragments.size());
        this.mViewpager.addOnPageChangeListener(new TabLayoutOnPageChangeListener(this.mTabLayout) {
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);
            }
        });
        this.mTabLayout.addOnTabSelectedListener(new C14036());
        if (!ApplicationUtils.isOnline(this)) {
            this.mViewpager.setCurrentItem(this.mListHomeFragments.indexOf(this.mFragmentMyMusic));
            registerNetworkBroadcastReceiver(this);
        }
    }

    private void showLayoutListenMusic(boolean b) {
        int i;
        this.mLayoutListenMusic.setVisibility(b ? View.VISIBLE : View.GONE);
        DBViewPager dBViewPager = this.mViewpager;
        if (b) {
            i = this.mStartHeight;
        } else {
            i = 0;
        }
        dBViewPager.setPadding(0, 0, 0, i);
        FrameLayout frameLayout = this.mLayoutContainer;
        if (b) {
            i = this.mStartHeight;
        } else {
            i = 0;
        }
        frameLayout.setPadding(0, 0, 0, i);
        if (!b) {
            this.mBottomSheetBehavior.setState(4);
        }
    }

    public void onNetworkState(boolean isNetworkOn) {
        if (this.mListHomeFragments != null && this.mListHomeFragments.size() > 0) {
            Iterator it = this.mListHomeFragments.iterator();
            while (it.hasNext()) {
                ((DBFragment) ((Fragment) it.next())).onNetworkChange(isNetworkOn);
            }
        }
        if (isNetworkOn && true) {
            setUpLayoutAdmob();
        }
    }

    protected void onDestroy() {
        super.onDestroy();
        this.mColumns = null;
        this.mTempData = null;
        try {
            if (this.mCursor != null) {
                this.mCursor.close();
                this.mCursor = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (this.mListHomeFragments != null) {
            this.mListHomeFragments.clear();
            this.mListHomeFragments = null;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d("oncreateoptionmenu","tru");
        getMenuInflater().inflate(R.menu.menu_main, menu);
        this.mMenu = menu;
        initSetupForSearchView(menu, R.id.action_search, new C14047());
        if(searchView!=null)this.searchView.setOnSuggestionListener(new C14058());
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                backToHome();
                break;
            case R.id.action_equalizer:
                goToEqualizer();
                break;
            case R.id.action_sleep_mode:
                showDialogSleepMode();
                break;
            case R.id.action_visit_website:
                goToUrl(getString(R.string.info_visit_website), IXMusicConstants.URL_WEBSITE);
                break;
            case R.id.action_contact_us:
                ShareActionUtils.shareViaEmail(this, IXMusicConstants.YOUR_CONTACT_EMAIL, "", "");
                break;
            case R.id.action_rate_me:
                ShareActionUtils.goToUrl(this, String.format(IXMusicConstants.URL_FORMAT_LINK_APP, new Object[]{getPackageName()}));
                YPYSettingManager.setRateApp(this, true);
                break;
            case R.id.action_share:

                try {
                    Uri imageUri = null;
                    try {
                        imageUri = Uri.parse(MediaStore.Images.Media.insertImage(this.getContentResolver(),
                                BitmapFactory.decodeResource(getResources(), R.drawable.logo_splash), null, null));
                    } catch (NullPointerException e) {
                    }

                    String urlApp1 = String.format(IXMusicConstants.URL_FORMAT_LINK_APP, new Object[]{getPackageName()});
                    String msg = String.format(getString(R.string.info_share_app), new Object[]{getString(R.string.app_name), urlApp1});
                    Intent sharingIntent = new Intent("android.intent.action.SEND");
                    sharingIntent.setType("image/*");
                    sharingIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
                    sharingIntent.putExtra("android.intent.extra.TEXT", msg);
                    startActivity(Intent.createChooser(sharingIntent, getString(R.string.info_share)));

                    break;
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }

        return super.onOptionsItemSelected(item);
    }

    private void showHideSearchView(boolean b) {
        if (this.mMenu != null) {
            this.mMenu.findItem(R.id.action_search).setVisible(b);
        }
    }

    public void goToDetailPlaylist(PlaylistModel mPlaylistObject, int type) {
        this.mTotalMng.setPlaylistObject(mPlaylistObject);
        setActionBarTitle(mPlaylistObject.getName());
        goToDetailTracks(type);
    }

    private void goToDetailTracks(int type) {
        showHideLayoutContainer(true);
        Bundle mBundle = new Bundle();
        mBundle.putInt(IXMusicConstants.KEY_TYPE, type);
        goToFragment(getFragmentTag(type), (int) R.id.container, FragmentDetailTracks.class.getName(), 0, mBundle);
    }

    public void goToGenre(GenreModel mGenreObject) {
        this.mTotalMng.setGenreObject(mGenreObject);
        setActionBarTitle(mGenreObject.getName());
        showHideLayoutContainer(true);
        goToDetailTracks(16);
    }

    private String getFragmentTag(int type) {
        if (type == 12) {
            return IXMusicConstants.TAG_FRAGMENT_DETAIL_PLAYLIST;
        }
        if (type == 13) {
            return IXMusicConstants.TAG_FRAGMENT_TOP_PLAYLIST;
        }
        if (type == 16) {
            return IXMusicConstants.TAG_FRAGMENT_DETAIL_GENRE;
        }
        if (type == 15) {
            return IXMusicConstants.TAG_FRAGMENT_RECOMMENDED_LIST;
        }
        return null;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            if (this.searchView != null && !this.searchView.isIconified()) {
                hiddenKeyBoardForSearchView();
                return true;
            } else if (collapseListenMusic() || backToHome()) {
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    private boolean backToHome() {
        if (this.mListFragments != null && this.mListFragments.size() > 0) {
            Iterator it = this.mListFragments.iterator();
            while (it.hasNext()) {
                Fragment mFragment = (Fragment) it.next();
                if ((mFragment instanceof DBFragment) && ((DBFragment) mFragment).isCheckBack()) {
                    return true;
                }
            }
        }
        if (!backStack(null)) {
            return false;
        }
        showHideLayoutContainer(false);
        return true;
    }

    public void showHideLayoutContainer(boolean b) {
        int i;
        boolean z;
        int i2 = 8;
        boolean z2 = false;
        this.mLayoutContainer.setVisibility(b ? View.VISIBLE : View.GONE);
        TabLayout tabLayout = this.mTabLayout;
        if (b) {
            i = 8;
        } else {
            i = 0;
        }
        tabLayout.setVisibility(i);
        DBViewPager dBViewPager = this.mViewpager;
        if (!b) {
            i2 = 0;
        }
        dBViewPager.setVisibility(i2);
        getSupportActionBar().setDisplayHomeAsUpEnabled(b);
        getSupportActionBar().setHomeButtonEnabled(b);
        ActionBar supportActionBar = getSupportActionBar();
        if (b) {
            z = false;
        } else {
            z = true;
        }
        supportActionBar.setDisplayUseLogoEnabled(z);
        supportActionBar = getSupportActionBar();
        if (b) {
            z = false;
        } else {
            z = true;
        }
        supportActionBar.setDisplayShowHomeEnabled(z);
        if (!b) {
            z2 = true;
        }
        showHideSearchView(z2);
        if (b) {
            this.mAppBarLayout.setExpanded(true);
            getSupportActionBar().setHomeAsUpIndicator(this.mBackDrawable);
            return;
        }
        setUpActionBar();
    }

    public void notifyData(int type) {
        switch (type) {
            case 7:
                updateFavorite();
                return;
            case 8:
                updateFavorite();
                return;
            case 9:
                if (this.mFragmentPlaylist != null) {
                    this.mFragmentPlaylist.notifyData();
                    if (getSupportFragmentManager().findFragmentByTag(IXMusicConstants.TAG_FRAGMENT_DETAIL_PLAYLIST) != null) {
                        ((FragmentDetailTracks) getSupportFragmentManager().findFragmentByTag(IXMusicConstants.TAG_FRAGMENT_DETAIL_PLAYLIST)).notifyData();
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void updateFavorite() {
        if (getSupportFragmentManager().findFragmentByTag(IXMusicConstants.TAG_FRAGMENT_FAVORITE) != null) {
            ((FragmentDetailTracks) getSupportFragmentManager().findFragmentByTag(IXMusicConstants.TAG_FRAGMENT_FAVORITE)).notifyData();
        }
    }

    public void notifyData(int type, long id) {
        if (type == 11) {
            if (MusicDataMng.getInstance().getCurrentPlayingId() == id) {
                startMusicService(IYPYMusicConstant.ACTION_NEXT);
            }
            notifyFragment();
        }
    }

    public void notifyFragment() {
        super.notifyFragment();
        if (this.mListHomeFragments != null && this.mListHomeFragments.size() > 0) {
            this.mFragmentPlaylist.notifyData();
            this.mFragmentMyMusic.notifyData();
        }
    }

    public void startPlayingList(TrackModel mTrackObject, ArrayList<TrackModel> listTrackObjects) {
        updateInfoOfPlayTrack(mTrackObject);
        if (listTrackObjects != null && listTrackObjects.size() > 0) {
            ArrayList<TrackModel> mListTracks = (ArrayList) listTrackObjects.clone();
            Iterator<TrackModel> mIterator = mListTracks.iterator();
            while (mIterator.hasNext()) {
                if (((TrackModel) mIterator.next()).isNativeAds()) {
                    mIterator.remove();
                }
            }
            MusicDataMng.getInstance().setListPlayingTrackObjects(mListTracks);
            if (this.mFragmentMusic != null) {
                this.mFragmentMusic.setUpInfo(mListTracks);
            }
            TrackModel mCurrentTrack = MusicDataMng.getInstance().getCurrentTrackObject();
            boolean isPlayingTrack = mCurrentTrack != null && mCurrentTrack.getId() == mTrackObject.getId();
            if (isPlayingTrack) {
                try {
                    if (MusicDataMng.getInstance().getPlayer() != null) {
                        setUpImageViewBaseOnColor(this.mBtnSmallPlay, this.mIconColor, MusicDataMng.getInstance().isPlayingMusic() ? R.drawable.ic_pause_white_36dp : R.drawable.ic_play_arrow_white_36dp, false);
                        return;
                    }
                    setUpImageViewBaseOnColor(this.mBtnSmallPlay, this.mIconColor, (int) R.drawable.ic_play_arrow_white_36dp, false);
                    if (MusicDataMng.getInstance().setCurrentIndex(mTrackObject)) {
                        startMusicService(IYPYMusicConstant.ACTION_PLAY);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    setUpImageViewBaseOnColor(this.mBtnSmallPlay, this.mIconColor, (int) R.drawable.ic_play_arrow_white_36dp, false);
                    startMusicService(IYPYMusicConstant.ACTION_STOP);
                }
            } else if (MusicDataMng.getInstance().setCurrentIndex(mTrackObject)) {
                startMusicService(IYPYMusicConstant.ACTION_PLAY);
            }
        }
    }

    private void updateInfoOfPlayTrack(TrackModel mTrackObject) {

        Log.d("call","update");

        if (mTrackObject != null) {
            showLayoutListenMusic(true);
            String artwork = mTrackObject.getArtworkUrl();
            if (TextUtils.isEmpty(artwork)) {
                Uri mUri = mTrackObject.getURI();
                if (mUri != null) {
                    GlideImageLoader.displayImageFromMediaStore(this, this.mImgSmallSong, mUri, R.drawable.ic_disk);
                } else {
                    this.mImgSmallSong.setImageResource(R.drawable.ic_rect_music_default);
                }
            } else {
                GlideImageLoader.displayImage(this, this.mImgSmallSong, artwork, (int) R.drawable.ic_rect_music_default);
            }
            this.mTvSmallSong.setText(Html.fromHtml(mTrackObject.getTitle()));
            String artist = mTrackObject.getAuthor();
            if (StringUtils.isEmptyString(artist) || artist.equalsIgnoreCase(IXMusicConstants.PREFIX_UNKNOWN)) {
                this.mTvSmallSinger.setText(R.string.title_unknown);
            } else {
                this.mTvSmallSinger.setText(artist);
            }
        }
    }

    public void onPlayerUpdateState(boolean isPlay) {
        setUpImageViewBaseOnColor(this.mBtnSmallPlay, this.mIconColor, MusicDataMng.getInstance().isPlayingMusic() ? R.drawable.ic_pause_white_36dp : R.drawable.ic_play_arrow_white_36dp, false);
        if (this.mFragmentMusic != null) {
            this.mFragmentMusic.onPlayerUpdateState(isPlay);
        }
    }

    public void onPlayerStop() {
        showLayoutListenMusic(false);
        setUpImageViewBaseOnColor(this.mBtnSmallPlay, this.mIconColor, MusicDataMng.getInstance().isPlayingMusic() ? R.drawable.ic_pause_white_36dp : R.drawable.ic_play_arrow_white_36dp, false);
        if (this.mFragmentMusic != null) {
            this.mFragmentMusic.onPlayerStop();
        }
    }

    public void onPlayerLoading() {
        Log.d("call","onplaying");
        showLoading(true);
        updateInfoOfPlayTrack(MusicDataMng.getInstance().getCurrentTrackObject());
    }

    public void onPlayerStopLoading() {
        showLoading(false);
    }

    public void onPlayerUpdatePos(int currentPos) {
        if (this.mFragmentMusic != null) {
            this.mFragmentMusic.onUpdatePos(currentPos);
        }
    }

    public void onPlayerError() {
    }

    public void onPlayerUpdateStatus() {
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_play:
                startMusicService(IYPYMusicConstant.ACTION_TOGGLE_PLAYBACK);
                return;
            case R.id.btn_next:
                startMusicService(IYPYMusicConstant.ACTION_NEXT);
                return;
            case R.id.btn_prev:
                startMusicService(IYPYMusicConstant.ACTION_PREVIOUS);
                return;
            default:
                return;
        }
    }

    private void showLoading(boolean b) {
        int i;
        int i2 = 4;
        int i3 = 0;
        this.mBtnSmallPlay.setVisibility(!b ? View.VISIBLE : View.INVISIBLE);
        ImageView imageView = this.mBtnSmallNext;
        if (b) {
            i = 8;
        } else {
            i = 0;
        }
        imageView.setVisibility(i);
        ImageView imageView2 = this.mBtnSmallPrev;
        if (!b) {
            i2 = 0;
        }
        imageView2.setVisibility(i2);
        CircularProgressBar circularProgressBar = this.mProgressLoadingMusic;
        if (!b) {
            i3 = 8;
        }
        circularProgressBar.setVisibility(i3);
//        mImgSmallSong.setVisibility(i);
        if (this.mFragmentMusic != null) {
            this.mFragmentMusic.showLoading(b);
        }
    }

    private void startSuggestion(final String search) {
        DBExecutorSupplier.getInstance().forLightWeightBackgroundTasks().execute(new Runnable() {
            public void run() {
                String countryCode = Locale.getDefault().getCountry();
                InputStream mInputStream = DownloadUtils.download(String.format(IXMusicConstants.URL_FORMAT_SUGESSTION, new Object[]{countryCode, StringUtils.urlEncodeString(search)}));
                if (mInputStream != null) {
                    final ArrayList<String> mListSuggestionStr = XMLParsingData.parsingSuggestion(mInputStream);
                    if (mListSuggestionStr != null) {
                        MainActivity.this.runOnUiThread(new Runnable() {
                            public void run() {
                                MainActivity.this.searchView.setSuggestionsAdapter(null);
                                if (MainActivity.this.mListSuggestionStr != null) {
                                    MainActivity.this.mListSuggestionStr.clear();
                                    MainActivity.this.mListSuggestionStr = null;
                                }
                                MainActivity.this.mListSuggestionStr = mListSuggestionStr;
                                try {
                                    MainActivity.this.mTempData = null;
                                    MainActivity.this.mColumns = null;
                                    if (MainActivity.this.mCursor != null) {
                                        MainActivity.this.mCursor.close();
                                        MainActivity.this.mCursor = null;
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                MainActivity.this.mColumns = new String[]{"_id", "text"};
                                MainActivity.this.mTempData = new Object[]{Integer.valueOf(0), "default"};
                                MainActivity.this.mCursor = new MatrixCursor(MainActivity.this.mColumns);
                                int size = mListSuggestionStr.size();
                                MainActivity.this.mCursor.close();
                                for (int i = 0; i < size; i++) {
                                    MainActivity.this.mTempData[0] = Integer.valueOf(i);
                                    MainActivity.this.mTempData[1] = mListSuggestionStr.get(i);
                                    MainActivity.this.mCursor.addRow(MainActivity.this.mTempData);
                                }
                                MainActivity.this.searchView.setSuggestionsAdapter(new SuggestionDataAdapter(MainActivity.this, MainActivity.this.mCursor, mListSuggestionStr));
                            }
                        });
                    }
                }
            }
        });
    }

    private void goToSearchMusic(String keyword) {
        hiddenKeyBoardForSearchView();
        if (getSupportFragmentManager().findFragmentByTag(IXMusicConstants.TAG_FRAGMENT_SEARCH) != null) {
            ((FragmentSearchTrack) getSupportFragmentManager().findFragmentByTag(IXMusicConstants.TAG_FRAGMENT_SEARCH)).startSearch(keyword);
            return;
        }
        backStack(null);
        Bundle mBundle = new Bundle();
        mBundle.putString(IXMusicConstants.KEY_BONUS, keyword);
        String tag = getCurrentFragmentTag();
        setActionBarTitle((int) R.string.title_search_music);
        showHideLayoutContainer(true);
        showHideSearchView(true);
        if (StringUtils.isEmptyString(tag)) {
            goToFragment(IXMusicConstants.TAG_FRAGMENT_SEARCH, (int) R.id.container, FragmentSearchTrack.class.getName(), null, mBundle);
            return;
        }
        goToFragment(IXMusicConstants.TAG_FRAGMENT_SEARCH, (int) R.id.container, FragmentSearchTrack.class.getName(), tag, mBundle);
    }

    public boolean collapseListenMusic() {
        if (this.mBottomSheetBehavior.getState() != 3) {
            return false;
        }
        this.mBottomSheetBehavior.setState(4);
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        DBLog.LOGD("onback","pressed");
    }
}
