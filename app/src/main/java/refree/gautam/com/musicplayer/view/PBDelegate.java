package refree.gautam.com.musicplayer.view;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.UiThread;

/**
 * Created by admin on 3/26/2018.
 */

public interface PBDelegate {
    @UiThread
    void draw(Canvas canvas, Paint paint);

    @UiThread
    void start();

    @UiThread
    void stop();

    @UiThread
    void progressiveStop(CircularProgressDrawable.OnEndListener listener);
}
