package refree.gautam.com.musicplayer.utils;

import android.content.Context;
import android.os.Build.VERSION;
import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import okio.BufferedSink;
import okio.BufferedSource;
import okio.Okio;

public class IOUtils {
    private static final String TAG = IOUtils.class.getSimpleName();

    public static void writeString(String mDirectory, String mNameFile, String mStrData) {
        if (mDirectory == null || mNameFile == null || mStrData == null) {
            new Exception(TAG + ": Some content can not null").printStackTrace();
            return;
        }
        File mFile = new File(mDirectory);
        if (!mFile.exists()) {
            mFile.mkdirs();
        }
        try {
            BufferedSink sink = Okio.buffer(Okio.sink(new File(mDirectory, mNameFile)));
            sink.writeUtf8(mStrData);
            sink.close();
        } catch (Exception iox) {
            iox.printStackTrace();
        }
    }

    public static String readString(String mDirectory, String mNameFile) {
        try {
            File mFile = new File(mDirectory, mNameFile);
            if (mFile.exists() && mFile.isFile()) {
                BufferedSource source = Okio.buffer(Okio.source(mFile));
                String data = source.readUtf8();
                source.close();
                return data;
            }
        } catch (Exception e) {
            Log.e(TAG, "--->error when read string" + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    public static String readStringFromAssets(Context mContext, String mNameFile) {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(mContext.getAssets().open(mNameFile)));
            StringBuilder contents = new StringBuilder();
            while (true) {
                String line = br.readLine();
                if (line == null) {
                    return contents.toString();
                }
                contents.append(line);
                contents.append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean hasGingerbread() {
        return VERSION.SDK_INT >= 9;
    }

    public static boolean hasLolipop() {
        return VERSION.SDK_INT >= 21;
    }

    public static boolean hasMarsallow() {
        return VERSION.SDK_INT >= 23;
    }

    public static boolean hasKitKat() {
        return VERSION.SDK_INT >= 19;
    }
}
