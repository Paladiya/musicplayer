package refree.gautam.com.musicplayer.utils;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.widget.Toast;

public class ShareActionUtils {
    public static final String TAG = ShareActionUtils.class.getSimpleName();

    public static void shareToTwitter(Activity mActivity, String content) {
        try {
            Intent tweetIntent = new Intent("android.intent.action.SEND");
            tweetIntent.putExtra("android.intent.extra.TEXT", content);
            tweetIntent.setType("text/plain");
            boolean isResold = false;
            for (ResolveInfo app : mActivity.getPackageManager().queryIntentActivities(tweetIntent, 0)) {
                if ("com.twitter.android.PostActivity".equals(app.activityInfo.name)) {
                    ActivityInfo activity = app.activityInfo;
                    ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
                    tweetIntent.addCategory("android.intent.category.LAUNCHER");
                    tweetIntent.setFlags(270532608);
                    tweetIntent.setComponent(name);
                    mActivity.startActivity(tweetIntent);
                    isResold = true;
                    break;
                }
            }
            if (!isResold) {
                Toast.makeText(mActivity, "Please install the twitter application!", 1).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void shareViaSMS(Activity mActivity, String content) {
        try {
            Intent sendIntent = new Intent("android.intent.action.VIEW");
            sendIntent.putExtra("sms_body", content);
            sendIntent.setType("vnd.android-dir/mms-sms");
            mActivity.startActivity(sendIntent);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(mActivity, "Can not share via sms!Please try again", 1).show();
        }
    }

    public static void shareViaEmail(Activity mActivity, String destEmail, String subject, String body) {
        try {
            Intent sharingIntent = new Intent("android.intent.action.SEND");
            if (!StringUtils.isEmptyString(destEmail) && EmailUtils.isEmailAddressValid(destEmail)) {
                sharingIntent.putExtra("android.intent.extra.EMAIL", new String[]{destEmail});
            }
            sharingIntent.setType("message/rfc822");
            if (!StringUtils.isEmptyString(subject)) {
                sharingIntent.putExtra("android.intent.extra.SUBJECT", subject);
            }
            if (!StringUtils.isEmptyString(body)) {
                sharingIntent.putExtra("android.intent.extra.TEXT", body);
            }
            mActivity.startActivity(sharingIntent);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(mActivity, "Can not share via email!Please try again", 1).show();
        }
    }

    public static void callNumber(Activity mActivity, String mNumberPhone) {
        try {
            if (StringUtils.isEmptyString(mNumberPhone)) {
                Toast.makeText(mActivity, "No phonenumber to call!", 0).show();
                return;
            }
            mActivity.startActivity(new Intent("android.intent.action.CALL", Uri.parse("tel:" + mNumberPhone.replaceAll("\\)+", "").replaceAll("\\(+", "").replaceAll("\\-+", ""))));
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(mActivity, "Phonenumber error!Please try again", 0).show();
        }
    }

    public static void goToUrl(Activity mActivity, String mUrl) {
        try {
            Intent mIt = new Intent("android.intent.action.VIEW", Uri.parse(mUrl));
            mIt.addFlags(1073741824);
            mIt.addFlags(536870912);
            mIt.addFlags(4);
            mActivity.startActivity(mIt);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void shareInfo(Activity mActivity, String content) {
        try {
            Intent sendIntent = new Intent("android.intent.action.SEND");
            sendIntent.setType("text/*");
            sendIntent.putExtra("android.intent.extra.TEXT", content);
            mActivity.startActivity(Intent.createChooser(sendIntent, "Share"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
