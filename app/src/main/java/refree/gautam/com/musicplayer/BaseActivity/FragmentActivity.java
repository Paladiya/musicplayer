package refree.gautam.com.musicplayer.BaseActivity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnCloseListener;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.MaterialDialog.ButtonCallback;
import com.afollestad.materialdialogs.MaterialDialog.ListCallback;
import com.bumptech.glide.Glide;
import com.triggertrap.seekarc.SeekArc;

import hotchemi.android.rate.AppRate;
import hotchemi.android.rate.OnClickButtonListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import refree.gautam.com.musicplayer.DataManger.MusicDataMng;
import refree.gautam.com.musicplayer.DataManger.TotalDataManager;
import refree.gautam.com.musicplayer.InterFace.IDBMusicPlayerListener;
import refree.gautam.com.musicplayer.InterFace.IDBSearchViewInterface;
import refree.gautam.com.musicplayer.InterFace.IXMusicConstants;
import refree.gautam.com.musicplayer.InterFace.IYPYCallback;
import refree.gautam.com.musicplayer.InterFace.YPYSettingManager;
import refree.gautam.com.musicplayer.MainActivity;
import refree.gautam.com.musicplayer.Models.PlaylistModel;
import refree.gautam.com.musicplayer.Models.TrackModel;
import refree.gautam.com.musicplayer.R;
import refree.gautam.com.musicplayer.EqualizerActivity;
import refree.gautam.com.musicplayer.PrivacyActivity;
import refree.gautam.com.musicplayer.SplashActivity;
import refree.gautam.com.musicplayer.abtractclass.fragment.DBFragment;
import refree.gautam.com.musicplayer.abtractclass.fragment.IDBFragmentConstants;
import refree.gautam.com.musicplayer.imageloader.target.GlideViewGroupTarget;
import refree.gautam.com.musicplayer.playservice.IYPYMusicConstant;
import refree.gautam.com.musicplayer.playservice.MusicService;
import refree.gautam.com.musicplayer.utils.ApplicationUtils;
import refree.gautam.com.musicplayer.utils.DBLog;
import refree.gautam.com.musicplayer.utils.DownloadUtils;
import refree.gautam.com.musicplayer.utils.FBAds;
import refree.gautam.com.musicplayer.utils.IOUtils;
import refree.gautam.com.musicplayer.utils.ResolutionUtils;
import refree.gautam.com.musicplayer.utils.ShareActionUtils;
import refree.gautam.com.musicplayer.utils.StringUtils;
import refree.gautam.com.musicplayer.view.DividerItemDecoration;

/**
 * Created by admin on 3/23/2018.
 */

public class FragmentActivity extends AppCompatActivity implements IXMusicConstants, IYPYMusicConstant, IDBFragmentConstants {
    public static final String TAG = FragmentActivity.class.getSimpleName();
    private int countToExit;
    private boolean isAllowPressMoreToExit;
    private boolean isLoadingBg;
    public boolean isNeedProcessOther;
    private boolean isPausing;
    public Drawable mBackDrawable;
    public int mContentActionColor;
    public int mIconColor;
    private RelativeLayout mLayoutAds;
    public ArrayList<Fragment> mListFragments;
    private String[] mListStr;
    private ConnectionChangeReceiver mNetworkBroadcast;
    private INetworkListener mNetworkListener;
    private MusicPlayerBroadcast mPlayerBroadcast;
    private Dialog mProgressDialog;
    private GlideViewGroupTarget mTarget;
    public TotalDataManager mTotalMng;
    public Typeface mTypefaceBold;
    public Typeface mTypefaceItalic;
    public Typeface mTypefaceLight;
    public Typeface mTypefaceNormal;
    private IDBMusicPlayerListener musicPlayerListener;
    private long pivotTime;
    private int screenHeight;
    private int screenWidth;
    public SearchView searchView;
    Intent shareIntent;
    Dialog dialogExit;
    TextView yes,no;
    LinearLayout linearLayoutAds;

    class C13892 implements OnClickButtonListener {
        C13892() {
        }

        public void onClickButton(int which) {
            if (which == -1) {
                YPYSettingManager.setRateApp(FragmentActivity.this, true);
                ShareActionUtils.goToUrl(FragmentActivity.this, String.format(IXMusicConstants.URL_FORMAT_LINK_APP, new Object[]{FragmentActivity.this.getPackageName()}));
            }
        }
    }



    class C13936 implements IYPYCallback {
        C13936() {
        }

        public void onAction() {
            FragmentActivity.this.onDestroyData();
            FragmentActivity.this.finish();
        }
    }

    class C13947 implements IYPYCallback {
        C13947() {
        }

        public void onAction() {
        }
    }

    class C13958 implements DialogInterface.OnKeyListener {
        C13958() {
        }

        public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
            if (keyCode == 4) {
                return true;
            }
            return false;
        }
    }

    class C13969 extends MaterialDialog.ButtonCallback {
        C13969() {
        }

        public void onPositive(MaterialDialog dialog) {
            super.onPositive(dialog);
            FragmentActivity.this.mListStr = null;
        }
    }

    private class ConnectionChangeReceiver extends BroadcastReceiver {
        private ConnectionChangeReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            if (FragmentActivity.this.mNetworkListener != null) {
                FragmentActivity.this.mNetworkListener.onNetworkState(ApplicationUtils.isOnline(FragmentActivity.this));
            }
        }
    }

    public interface INetworkListener {
        void onNetworkState(boolean z);
    }

    private class MusicPlayerBroadcast extends BroadcastReceiver {
        private MusicPlayerBroadcast() {
        }

        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                try {
                    String action = intent.getAction();
                    if (!StringUtils.isEmptyString(action)) {
                        String packageName = IXMusicConstants.PREFIX_ACTION;
                        if (action.equals(packageName + IYPYMusicConstant.ACTION_BROADCAST_PLAYER)) {
                            String actionPlay = intent.getStringExtra(IYPYMusicConstant.KEY_ACTION);
                            if (!StringUtils.isEmptyString(actionPlay)) {
                                if (actionPlay.equals(packageName + IYPYMusicConstant.ACTION_NEXT)) {
                                    if (FragmentActivity.this.musicPlayerListener != null) {
                                        FragmentActivity.this.musicPlayerListener.onPlayerUpdateState(false);
                                    }
                                } else if (actionPlay.equals(packageName + IYPYMusicConstant.ACTION_LOADING)) {
                                    if (FragmentActivity.this.musicPlayerListener != null) {
                                        FragmentActivity.this.musicPlayerListener.onPlayerLoading();
                                    }
                                } else if (actionPlay.equals(packageName + IYPYMusicConstant.ACTION_DIMISS_LOADING)) {
                                    if (FragmentActivity.this.musicPlayerListener != null) {
                                        FragmentActivity.this.musicPlayerListener.onPlayerStopLoading();
                                    }
                                } else if (actionPlay.equals(packageName + IYPYMusicConstant.ACTION_ERROR)) {
                                    FragmentActivity.this.showToast((int) R.string.info_play_song_error);
                                    if (FragmentActivity.this.musicPlayerListener != null) {
                                        FragmentActivity.this.musicPlayerListener.onPlayerError();
                                    }
                                } else if (actionPlay.equals(packageName + IYPYMusicConstant.ACTION_PAUSE)) {
                                    if (FragmentActivity.this.musicPlayerListener != null) {
                                        FragmentActivity.this.musicPlayerListener.onPlayerUpdateState(false);
                                    }
                                } else if (actionPlay.equals(packageName + IYPYMusicConstant.ACTION_STOP)) {
                                    MusicDataMng.getInstance().onResetData();
                                    if (FragmentActivity.this.musicPlayerListener != null) {
                                        FragmentActivity.this.musicPlayerListener.onPlayerStop();
                                    }
                                } else if (actionPlay.equals(packageName + IYPYMusicConstant.ACTION_PLAY)) {
                                    if (FragmentActivity.this.musicPlayerListener != null) {
                                        FragmentActivity.this.musicPlayerListener.onPlayerUpdateState(true);
                                    }
                                } else if (actionPlay.equals(packageName + IYPYMusicConstant.ACTION_UPDATE_POS)) {
                                    int currentPos = intent.getIntExtra(IYPYMusicConstant.KEY_VALUE, -1);
                                    if (FragmentActivity.this.musicPlayerListener != null) {
                                        FragmentActivity.this.musicPlayerListener.onPlayerUpdatePos(currentPos);
                                    }
                                } else if (actionPlay.equals(packageName + IYPYMusicConstant.ACTION_UPDATE_STATUS)) {
                                    if (FragmentActivity.this.musicPlayerListener != null) {
                                        FragmentActivity.this.musicPlayerListener.onPlayerUpdateStatus();
                                    }
                                } else if (actionPlay.equals(packageName + IXMusicConstants.ACTION_FAVORITE) && FragmentActivity.this.isNeedProcessOther) {
                                    FragmentActivity.this.notifyData(intent.getIntExtra(IXMusicConstants.KEY_TYPE, -1));
                                } else if (actionPlay.equals(packageName + IXMusicConstants.ACTION_PLAYLIST) && FragmentActivity.this.isNeedProcessOther) {
                                    FragmentActivity.this.notifyData(9);
                                } else if (actionPlay.equals(packageName + IXMusicConstants.ACTION_DELETE_SONG) && FragmentActivity.this.isNeedProcessOther) {
                                    FragmentActivity.this.notifyData(11, intent.getLongExtra(IXMusicConstants.KEY_SONG_ID, -1));
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFormat(1);
        getWindow().setSoftInputMode(3);
        getWindow().setSoftInputMode(48);
        createProgressDialog();
        this.mTypefaceNormal = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
        this.mTypefaceLight = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");
        this.mTypefaceBold = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Bold.ttf");
        this.mTypefaceItalic = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Italic.ttf");
        this.mTotalMng = TotalDataManager.getInstance();
        setStatusBarTranslucent(true);
        this.mContentActionColor = getResources().getColor(R.color.icon_action_bar_color);
        this.mIconColor = getResources().getColor(R.color.icon_color);
        this.mBackDrawable = getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp);
        this.mBackDrawable.setColorFilter(this.mContentActionColor, PorterDuff.Mode.SRC_ATOP);
        int[] mRes = ResolutionUtils.getDeviceResolution(this);
        if (mRes != null && mRes.length == 2) {
            this.screenWidth = mRes[0];
            this.screenHeight = mRes[1];
        }

        dialogExit = new Dialog(this,R.style.WideDialog);
        dialogExit.setContentView(R.layout.activity_exit);
        dialogExit.setCancelable(false);
        this.yes = (TextView) dialogExit.findViewById(R.id.btnYes);
        this.no = (TextView) dialogExit.findViewById(R.id.btnNo);
        this.yes.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogExit.dismiss();
                finish();
            }
        });
        this.no.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogExit.dismiss();
            }
        });
        linearLayoutAds  = (LinearLayout) dialogExit.findViewById(R.id.lad);
        if (DownloadUtils.isInternetOn(this))
        {
            linearLayoutAds.addView(FBAds.GetAdViewMedium(this));

        }else
        {
            linearLayoutAds.setVisibility(View.GONE);
        }
    }

    public void setStatusBarTranslucent(boolean makeTranslucent) {
        if (!IOUtils.hasKitKat()) {
            return;
        }
        if (makeTranslucent) {
            getWindow().addFlags(67108864);
        } else {
            getWindow().clearFlags(67108864);
        }
    }

    public void setUpImageViewBaseOnColor(int id, int color, int idDrawabe, boolean isReset) {
        setUpImageViewBaseOnColor(findViewById(id), color, idDrawabe, isReset);
    }

    public void setUpImageViewBaseOnColor(View mView, int color, int idDrawabe, boolean isReset) {
        Drawable mDrawable = getResources().getDrawable(idDrawabe);
        if (isReset) {
            mDrawable.clearColorFilter();
        } else {
            mDrawable.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        }
        if (mView instanceof Button) {
            mView.setBackgroundDrawable(mDrawable);
        } else if (mView instanceof ImageView) {
            ((ImageView) mView).setImageDrawable(mDrawable);
        } else if (mView instanceof ImageButton) {
            ((ImageView) mView).setImageDrawable(mDrawable);
        }
    }

    public void setUpBackground() {
        try {
            RelativeLayout mLayoutBg = (RelativeLayout) findViewById(R.id.layout_bg);
            if (mLayoutBg != null) {
                this.mTarget = new GlideViewGroupTarget(this, mLayoutBg) {
                    protected void setResource(Bitmap resource) {
                        super.setResource(resource);
                    }
                };
                String imgBg = YPYSettingManager.getBackground(this);
                Log.e("DCM", "=============>getBackground=" + imgBg);
                if (!TextUtils.isEmpty(imgBg)) {
                    Glide.with((android.support.v4.app.FragmentActivity) this).load(Uri.parse(imgBg)).asBitmap().placeholder((int) R.drawable.default_bg_app).into(this.mTarget);                } else if (this instanceof SplashActivity) {
                    mLayoutBg.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                } else {
                    mLayoutBg.setBackgroundColor(getResources().getColor(R.color.colorBackground));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void onPause() {
        super.onPause();
        this.isPausing = true;
    }

    protected void onResume() {
        super.onResume();
        if (this.isPausing || !this.isLoadingBg) {
            this.isPausing = false;
            this.isLoadingBg = true;
            setUpBackground();
        }
    }

    public void showAppRate() {
        if (!YPYSettingManager.getRateApp(this)) {
            AppRate.with(this).setInstallDays(0).setLaunchTimes(3).setRemindInterval(1).setShowLaterButton(true).setShowNeverButton(false).setDebug(false).setOnClickButtonListener(new C13892()).monitor();
            AppRate.showRateDialogIfMeetsConditions(this);
        }
    }

    public void setUpLayoutAdmob() {
        this.mLayoutAds = (RelativeLayout) findViewById(R.id.layout_ads);
        mLayoutAds.addView(FBAds.GetFbAdView(this));
    }

    public  void showInterstitial(final IYPYCallback mCallback) {
       FBAds.showFbInterstitial(this);
        if (mCallback != null) {
            mCallback.onAction();
        }
    }

    public void showAds() {
        this.mLayoutAds.setVisibility(View.VISIBLE);
    }

    public void hideAds() {
        this.mLayoutAds.setVisibility(View.GONE);
    }

    protected void onDestroy() {
        super.onDestroy();
        if (this.mPlayerBroadcast != null) {
            unregisterReceiver(this.mPlayerBroadcast);
            this.mPlayerBroadcast = null;
        }
        if (this.mNetworkBroadcast != null) {
            unregisterReceiver(this.mNetworkBroadcast);
            this.mNetworkBroadcast = null;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        if (this.isAllowPressMoreToExit) {
            if(DownloadUtils.isInternetOn(this)){
                dialogExit.show();
            }else {
                pressMoreToExitApp();
            }
        } else {
            showQuitDialog();
        }
        return true;
    }

    public void setIsAllowPressMoreToExit(boolean isAllowPressMoreToExit) {
        this.isAllowPressMoreToExit = isAllowPressMoreToExit;
    }

    private void pressMoreToExitApp() {
        if (this.countToExit >= 1) {
            if (System.currentTimeMillis() - this.pivotTime <= 2000) {
                onDestroyData();
                finish();
                return;
            }
            this.countToExit = 0;
        }
        this.pivotTime = System.currentTimeMillis();
        showToast((int) R.string.info_press_again_to_exit);
        this.countToExit++;
    }

    public MaterialDialog createFullDialog(int iconId, int mTitleId, int mYesId, int mNoId, String messageId, final IYPYCallback mCallback, final IYPYCallback mNeCallback) {
        MaterialDialog.Builder mBuilder = new MaterialDialog.Builder(this);
        mBuilder.title(mTitleId);
        if (iconId != -1) {
            mBuilder.iconRes(iconId);
        }
        mBuilder.content((CharSequence) messageId);
        mBuilder.backgroundColor(getResources().getColor(R.color.dialog_bg_color));
        mBuilder.titleColor(getResources().getColor(R.color.main_color_text));
        mBuilder.contentColor(getResources().getColor(R.color.main_color_text));
        mBuilder.positiveColor(getResources().getColor(R.color.colorAccent));
        mBuilder.negativeColor(getResources().getColor(R.color.main_color_secondary_text));
        mBuilder.negativeText(mNoId);
        mBuilder.positiveText(mYesId);
        mBuilder.autoDismiss(true);
        mBuilder.typeface(this.mTypefaceBold, this.mTypefaceLight);
        mBuilder.callback(new ButtonCallback() {
            public void onPositive(MaterialDialog dialog) {
                super.onPositive(dialog);
                if (mCallback != null) {
                    mCallback.onAction();
                }
            }

            public void onNegative(MaterialDialog dialog) {
                super.onNegative(dialog);
                if (mNeCallback != null) {
                    mNeCallback.onAction();
                }
            }
        });
        return mBuilder.build();
    }

    public void showFullDialog(int titleId, String message, int idPositive, int idNegative, IYPYCallback mDBCallback) {
        createFullDialog(-1, titleId, idPositive, idNegative, message, mDBCallback, null).show();
    }

    public void showFullDialog(int titleId, String message, int idPositive, int idNegative, IYPYCallback mDBCallback, IYPYCallback mNegative) {
        createFullDialog(-1, titleId, idPositive, idNegative, message, mDBCallback, mNegative).show();
    }

    public void showQuitDialog() {
        createFullDialog(R.drawable.ic_launcher, R.string.title_confirm, R.string.title_yes, R.string.title_no, getString(R.string.info_close_app), new C13936(), new C13947()).show();
    }

    private void createProgressDialog() {
        this.mProgressDialog = new Dialog(this);
        this.mProgressDialog.requestWindowFeature(1);
        this.mProgressDialog.setContentView(R.layout.item_progress_bar);
        ((TextView) this.mProgressDialog.findViewById(R.id.tv_message)).setTypeface(this.mTypefaceLight);
        this.mProgressDialog.setCancelable(false);
        this.mProgressDialog.setOnKeyListener(new C13958());
    }

    public void showProgressDialog() {
        showProgressDialog((int) R.string.info_loading);
    }

    public void showProgressDialog(int messageId) {
        showProgressDialog(getString(messageId));
    }

    public void showProgressDialog(String message) {
        if (this.mProgressDialog != null) {
            ((TextView) this.mProgressDialog.findViewById(R.id.tv_message)).setText(message);
            if (!this.mProgressDialog.isShowing()) {
                this.mProgressDialog.show();
            }
        }
    }

    public void dimissProgressDialog() {
        if (this.mProgressDialog != null) {
            this.mProgressDialog.dismiss();
        }
    }

    public int getScreenWidth() {
        return this.screenWidth;
    }

    public int getScreenHeight() {
        return this.screenHeight;
    }

    public void showToast(int resId) {
        showToast(getString(resId));
    }

    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void showToastWithLongTime(int resId) {
        showToastWithLongTime(getString(resId));
    }

    public void showToastWithLongTime(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    public void onDestroyData() {
        YPYSettingManager.setOnline(this, false);
        this.mTotalMng.onDestroy();
    }

    public void createArrayFragment() {
        this.mListFragments = new ArrayList();
    }

    public void addFragment(Fragment mFragment) {
        if (mFragment != null && this.mListFragments != null) {
            synchronized (this.mListFragments) {
                this.mListFragments.add(mFragment);
            }
        }
    }

    public Fragment getFragmentHome(String nameFragment, int idFragment) {
        if (idFragment > 0) {
            return getSupportFragmentManager().findFragmentById(idFragment);
        }
        if (StringUtils.isEmptyString(nameFragment)) {
            return null;
        }
        return getSupportFragmentManager().findFragmentByTag(nameFragment);
    }

    public String getStringDuration(long duration) {
        String minute = String.valueOf((int) (duration / 60));
        String seconds = String.valueOf((int) (duration % 60));
        if (minute.length() < 2) {
            minute = "0" + minute;
        }
        if (seconds.length() < 2) {
            seconds = "0" + seconds;
        }
        return minute + ":" + seconds;
    }

    public boolean backStack(IYPYCallback mCallback) {
        if (this.mListFragments != null && this.mListFragments.size() > 0) {
            int count = this.mListFragments.size();
            if (count > 0) {
                synchronized (this.mListFragments) {
                    Fragment mFragment = (Fragment) this.mListFragments.remove(count - 1);
                    if (mFragment == null || !(mFragment instanceof DBFragment)) {
                    } else {
                        ((DBFragment) mFragment).backToHome(this);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public void goToFragment(String tag, int idContainer, String fragmentName, String parentTag, Bundle mBundle) {
        goToFragment(tag, idContainer, fragmentName, 0, parentTag, mBundle);
    }

    public void goToFragment(String tag, int idContainer, String fragmentName, int parentId, Bundle mBundle) {
        goToFragment(tag, idContainer, fragmentName, parentId, null, mBundle);
    }

    public void goToFragment(String tag, int idContainer, String fragmentName, int parentId, String parentTag, Bundle mBundle) {
        if (StringUtils.isEmptyString(tag) || getSupportFragmentManager().findFragmentByTag(tag) == null) {
            Fragment mFragmentParent;
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            if (mBundle != null) {
                if (parentId != 0) {
                    mBundle.putInt(IDBFragmentConstants.KEY_ID_FRAGMENT, parentId);
                }
                if (!StringUtils.isEmptyString(parentTag)) {
                    mBundle.putString(IDBFragmentConstants.KEY_NAME_FRAGMENT, parentTag);
                }
            }
            Fragment mFragment = Fragment.instantiate(this, fragmentName, mBundle);
            addFragment(mFragment);
            transaction.add(idContainer, mFragment, tag);
            if (parentId != 0) {
                mFragmentParent = getSupportFragmentManager().findFragmentById(parentId);
                if (mFragmentParent != null) {
                    transaction.hide(mFragmentParent);
                }
            }
            if (!StringUtils.isEmptyString(parentTag)) {
                mFragmentParent = getSupportFragmentManager().findFragmentByTag(parentTag);
                if (mFragmentParent != null) {
                    transaction.hide(mFragmentParent);
                }
            }
            transaction.commit();
        }
    }

    public void showDialogPlaylist(final TrackModel mTrackObject, final IYPYCallback mCallback) {
        final ArrayList<PlaylistModel> mListPlaylist = this.mTotalMng.getListPlaylistObjects();
        if (mListPlaylist == null || mListPlaylist.size() <= 0) {
            this.mListStr = getResources().getStringArray(R.array.list_create_playlist);
        } else {
            int size = mListPlaylist.size() + 1;
            this.mListStr = new String[size];
            this.mListStr[0] = getResources().getStringArray(R.array.list_create_playlist)[0];
            for (int i = 1; i < size; i++) {
                this.mListStr[i] = ((PlaylistModel) mListPlaylist.get(i - 1)).getName();
            }
        }
        MaterialDialog.Builder mBuilder = new MaterialDialog.Builder(this);
        mBuilder.backgroundColor(getResources().getColor(R.color.dialog_bg_color));
        mBuilder.title((int) R.string.title_select_playlist);
        mBuilder.titleColor(getResources().getColor(R.color.main_color_text));
        mBuilder.items(this.mListStr);
        mBuilder.itemsColor(getResources().getColor(R.color.main_color_secondary_text));
        mBuilder.positiveColor(getResources().getColor(R.color.colorAccent));
        mBuilder.positiveText((int) R.string.title_cancel);
        mBuilder.autoDismiss(true);
        mBuilder.typeface(this.mTypefaceBold, this.mTypefaceNormal);
        mBuilder.callback(new C13969());
        mBuilder.itemsCallback(new ListCallback() {

            class C13831 implements IYPYCallback {
                C13831() {
                }

                public void onAction() {
                    ArrayList<PlaylistModel> mListPlaylist = FragmentActivity.this.mTotalMng.getListPlaylistObjects();
                    FragmentActivity.this.mTotalMng.addTrackToPlaylist(FragmentActivity.this, mTrackObject, (PlaylistModel) mListPlaylist.get(mListPlaylist.size() - 1), true, mCallback);
                    if (FragmentActivity.this.isNeedProcessOther) {
                        FragmentActivity.this.notifyData(9);
                    } else {
                        FragmentActivity.this.sendBroadcastPlayer(IXMusicConstants.ACTION_PLAYLIST);
                    }
                }
            }

            public void onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                if (mListPlaylist == null || mListPlaylist.size() <= 0 || which <= 0) {
                    FragmentActivity.this.showDialogCreatePlaylist(false, null, new C13831());
                } else {
                    FragmentActivity.this.mTotalMng.addTrackToPlaylist(FragmentActivity.this, mTrackObject, (PlaylistModel) mListPlaylist.get(which - 1), true, mCallback);
                }
                FragmentActivity.this.mListStr = null;
            }
        });
        mBuilder.build().show();
    }

    public void notifyData(int type) {
    }

    public void notifyData(int type, long value) {
    }

    public void notifyFragment() {
        if (this.mListFragments != null && this.mListFragments.size() > 0) {
            Iterator it = this.mListFragments.iterator();
            while (it.hasNext()) {
                Fragment mFragment = (Fragment) it.next();
                if (mFragment instanceof DBFragment) {
                    ((DBFragment) mFragment).notifyData();
                }
            }
        }
    }

    public void justNotifyFragment() {
        if (this.mListFragments != null && this.mListFragments.size() > 0) {
            Iterator it = this.mListFragments.iterator();
            while (it.hasNext()) {
                Fragment mFragment = (Fragment) it.next();
                if (mFragment instanceof DBFragment) {
                    ((DBFragment) mFragment).justNotifyData();
                }
            }
        }
    }
    boolean z;
    IYPYCallback iYPYCallback;
    PlaylistModel playlistModel;

    public void showDialogCreatePlaylist(boolean isEdit, PlaylistModel mPlaylistObject, IYPYCallback mCallback) {
        View mView = LayoutInflater.from(this).inflate(R.layout.dialog_edit_text, null);
        final EditText mEdPlaylistName = (EditText) mView.findViewById(R.id.ed_name);
        mEdPlaylistName.setTextColor(getResources().getColor(R.color.main_color_text));
        mEdPlaylistName.setHighlightColor(getResources().getColor(R.color.main_color_secondary_text));
        mEdPlaylistName.setHint(R.string.title_playlist_name);
        if (isEdit) {
            mEdPlaylistName.setText(mPlaylistObject.getName());
        }
        MaterialDialog.Builder mBuilder = new MaterialDialog.Builder(this);
        mBuilder.backgroundColor(getResources().getColor(R.color.dialog_bg_color));
        mBuilder.title((int) R.string.title_playlist_name);
        mBuilder.titleColor(getResources().getColor(R.color.main_color_text));
        mBuilder.contentColor(getResources().getColor(R.color.main_color_text));
        mBuilder.customView(mView, false);
        mBuilder.positiveColor(getResources().getColor(R.color.colorAccent));
        mBuilder.positiveText(isEdit ? R.string.title_save : R.string.title_create);
        mBuilder.negativeText((int) R.string.title_cancel);
        mBuilder.negativeColor(getResources().getColor(R.color.main_color_secondary_text));
        mBuilder.autoDismiss(true);
        mBuilder.typeface(this.mTypefaceBold, this.mTypefaceNormal);
        z = isEdit;
        playlistModel = mPlaylistObject;
        iYPYCallback = mCallback;
        mBuilder.callback(new ButtonCallback() {
            public void onPositive(MaterialDialog dialog) {
                super.onPositive(dialog);
                ApplicationUtils.hiddenVirtualKeyboard(FragmentActivity.this, mEdPlaylistName);
                FragmentActivity.this.checkCreatePlaylist(z, playlistModel, mEdPlaylistName.getText().toString(), iYPYCallback);
            }
        });
        final MaterialDialog mDialog = mBuilder.build();
        z = isEdit;
        playlistModel = mPlaylistObject;
        iYPYCallback = mCallback;
        mEdPlaylistName.setOnEditorActionListener(new OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId != 6) {
                    return false;
                }
                FragmentActivity.this.checkCreatePlaylist(z, playlistModel, mEdPlaylistName.getText().toString(), iYPYCallback);
                mDialog.dismiss();
                return true;
            }
        });
        mDialog.getWindow().setSoftInputMode(5);
        mDialog.show();
    }

    private void checkCreatePlaylist(boolean isEdit, PlaylistModel mPlaylistObject, String mPlaylistName, IYPYCallback mCallback) {
        if (StringUtils.isEmptyString(mPlaylistName)) {
            showToast((int) R.string.info_playlist_error);
        } else if (this.mTotalMng.isPlaylistNameExisted(mPlaylistName)) {
            showToast((int) R.string.info_playlist_name_existed);
        } else {
            if (isEdit) {
                this.mTotalMng.editPlaylistObject(mPlaylistObject, mPlaylistName);
            } else {
                mPlaylistObject = new PlaylistModel(System.currentTimeMillis(), mPlaylistName);
                mPlaylistObject.setListTrackObjects(new ArrayList());
                this.mTotalMng.addPlaylistObject(mPlaylistObject);
            }
            if (mCallback != null) {
                mCallback.onAction();
            }
        }
    }

    public void shareFile(TrackModel mTrackObject) {
        if (TextUtils.isEmpty(mTrackObject.getPath())) {
            shareIntent = new Intent("android.intent.action.SEND");
            shareIntent.putExtra("android.intent.extra.TEXT", mTrackObject.getTitle() + "\n" + mTrackObject.getPermalinkUrl());
            shareIntent.setType("text/plain");
            startActivity(Intent.createChooser(shareIntent, "Share Via"));
            return;
        }
        shareIntent = new Intent("android.intent.action.SEND");
        shareIntent.setType("*/*");
        shareIntent.putExtra("android.intent.extra.STREAM", Uri.fromFile(new File(mTrackObject.getPath())));
        startActivity(Intent.createChooser(shareIntent, "Share Via"));
    }

    public void onProcessSeekAudio(int currentPos) {
        startMusicService(IYPYMusicConstant.ACTION_SEEK, currentPos);
    }

    public void startMusicService(String action) {
        Intent mIntent1 = new Intent(this, MusicService.class);
        mIntent1.setAction(IXMusicConstants.PREFIX_ACTION + action);
        startService(mIntent1);
    }

    public void startMusicService(String action, boolean data) {
        Intent mIntent1 = new Intent(this, MusicService.class);
        mIntent1.setAction(IXMusicConstants.PREFIX_ACTION + action);
        mIntent1.putExtra(IYPYMusicConstant.KEY_VALUE, data);
        startService(mIntent1);
    }

    public void startMusicService(String action, int data) {
        Intent mIntent1 = new Intent(this, MusicService.class);
        mIntent1.setAction(IXMusicConstants.PREFIX_ACTION + action);
        mIntent1.putExtra(IYPYMusicConstant.KEY_VALUE, data);
        startService(mIntent1);
    }

    public void registerMusicPlayerBroadCastReceiver(IDBMusicPlayerListener musicPlayerListener) {
        if (this.mPlayerBroadcast == null) {
            this.musicPlayerListener = musicPlayerListener;
            this.mPlayerBroadcast = new MusicPlayerBroadcast();
            IntentFilter mIntentFilter = new IntentFilter();
            mIntentFilter.addAction("super.android.musiconline.stream.action.ACTION_BROADCAST_PLAYER");
            registerReceiver(this.mPlayerBroadcast, mIntentFilter);
        }
    }

    public void sendBroadcastPlayer(String action) {
        Intent mIntent = new Intent("super.android.musiconline.stream.action.ACTION_BROADCAST_PLAYER");
        mIntent.putExtra(IYPYMusicConstant.KEY_ACTION, IXMusicConstants.PREFIX_ACTION + action);
        sendBroadcast(mIntent);
    }

    public void sendBroadcastPlayer(String action, int type) {
        Intent mIntent = new Intent("super.android.musiconline.stream.action.ACTION_BROADCAST_PLAYER");
        mIntent.putExtra(IYPYMusicConstant.KEY_ACTION, IXMusicConstants.PREFIX_ACTION + action);
        mIntent.putExtra(IXMusicConstants.KEY_TYPE, type);
        sendBroadcast(mIntent);
    }

    public void showPopupMenu(View v, TrackModel mTrackObject) {
        showPopupMenu(v, mTrackObject, null);
    }

    public void showPopupMenu(View v, final TrackModel mTrackObject, final PlaylistModel mPlaylistObject) {
        boolean isOffline = this.mTotalMng.isLibraryTracks(mTrackObject);
        PopupMenu popupMenu = new PopupMenu(this, v);
        popupMenu.getMenuInflater().inflate(R.menu.menu_track, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new OnMenuItemClickListener() {

            class C13841 implements IYPYCallback {
                C13841() {
                }

                public void onAction() {
                    FragmentActivity.this.notifyData(9);
                }
            }

            class C13862 implements IYPYCallback {

                class C13851 implements Runnable {
                    C13851() {
                    }

                    public void run() {
                        FragmentActivity.this.notifyData(9);
                    }
                }

                C13862() {
                }

                public void onAction() {
                    FragmentActivity.this.runOnUiThread(new C13851());
                }
            }

            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_share:
                        FragmentActivity.this.shareFile(mTrackObject);
                        break;
                    case R.id.action_add_playlist:
                        FragmentActivity.this.showDialogPlaylist(mTrackObject, new C13841());
                        break;
                    case R.id.action_remove_from_playlist:
                        FragmentActivity.this.mTotalMng.removeTrackToPlaylist(mTrackObject, mPlaylistObject, new C13862());
                        break;
                    case R.id.action_delete_song:
                        FragmentActivity.this.showDialogDelete(mTrackObject);
                        break;
                }
                return true;
            }
        });
        if (!isOffline) {
            popupMenu.getMenu().findItem(R.id.action_delete_song).setVisible(false);
        }
        if (mPlaylistObject == null) {
            popupMenu.getMenu().findItem(R.id.action_remove_from_playlist).setVisible(false);
        } else {
            popupMenu.getMenu().findItem(R.id.action_add_playlist).setVisible(false);
        }
        popupMenu.show();
    }

    public void showDialogDelete(final TrackModel mTrackObject) {
        showFullDialog(R.string.title_confirm, getString(R.string.info_delete_song), R.string.title_ok, R.string.title_cancel, new IYPYCallback() {

            class C13871 implements IYPYCallback {
                C13871() {
                }

                public void onAction() {
                    FragmentActivity.this.showToast((int) R.string.info_delete_song_done);
                    FragmentActivity.this.dimissProgressDialog();
                    FragmentActivity.this.notifyData(11, mTrackObject.getId());
                }
            }

            public void onAction() {
                FragmentActivity.this.showProgressDialog();
                FragmentActivity.this.mTotalMng.deleteSong(mTrackObject, new C13871());
            }
        });
    }

    public void registerNetworkBroadcastReceiver(INetworkListener networkListener) {
        if (this.mNetworkBroadcast == null) {
            this.mNetworkBroadcast = new ConnectionChangeReceiver();
            IntentFilter mIntentFilter = new IntentFilter();
            mIntentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            registerReceiver(this.mNetworkBroadcast, mIntentFilter);
            this.mNetworkListener = networkListener;
        }
    }

    public void goToUrl(String name, String url) {
        Intent mIntent = new Intent(this, PrivacyActivity.class);
        mIntent.putExtra(IXMusicConstants.KEY_HEADER, name);
        mIntent.putExtra(IXMusicConstants.KEY_SHOW_URL, url);
        startActivity(mIntent);
    }

    public void setTypefaceForTab(TabLayout mTabLayout, Typeface sMaterialDesignIcons) {
        try {
            ViewGroup vg = (ViewGroup) mTabLayout.getChildAt(0);
            int tabsCount = vg.getChildCount();
            for (int j = 0; j < tabsCount; j++) {
                ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
                int tabChildsCount = vgTab.getChildCount();
                for (int i = 0; i < tabChildsCount; i++) {
                    View tabViewChild = vgTab.getChildAt(i);
                    if ((tabViewChild instanceof AppCompatTextView) || (tabViewChild instanceof TextView)) {
                        ((TextView) tabViewChild).setTypeface(sMaterialDesignIcons);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setActionBarTitle(String title) {
        ActionBar mAb = getSupportActionBar();
        if (mAb != null) {
            mAb.setTitle((CharSequence) title);
        }
    }

    public void setActionBarTitle(int titleId) {
        setActionBarTitle(getResources().getString(titleId));
    }

    public void setUpCustomizeActionBar() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
            mToolbar.setTitleTextColor(this.mContentActionColor);
            setActionBarTitle((int) R.string.title_equalizer);
            Drawable drawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_more_vert_white_24dp);
            drawable.setColorFilter(this.mContentActionColor, PorterDuff.Mode.SRC_ATOP);
            mToolbar.setOverflowIcon(drawable);
        }
    }

    public void setUpEvalationActionBar() {
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setElevation((float) getResources().getDimensionPixelOffset(R.dimen.card_elevation));
        }
    }

    public void removeEvalationActionBar() {
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setElevation(0.0f);
        }
    }

    public void setColorForActionBar(int color) {
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setBackgroundDrawable(new ColorDrawable(color));
        }
    }

    public void initSetupForSearchView(Menu menu, int idSearch, final IDBSearchViewInterface mListener) {
        this.searchView = (SearchView) menu.findItem(idSearch).getActionView();
//        this.searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        if (searchView != null){
            setUpImageViewBaseOnColor((ImageView) this.searchView.findViewById(android.support.v7.appcompat.R.id.search_button), this.mContentActionColor, (int) R.drawable.ic_search_white_24dp, false);
            setUpImageViewBaseOnColor((ImageView) this.searchView.findViewById(android.support.v7.appcompat.R.id.search_close_btn), this.mContentActionColor, (int) R.drawable.ic_close_white_24dp, false);
            EditText searchEditText = (EditText) this.searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
            searchEditText.setTextColor(this.mContentActionColor);
            searchEditText.setHintTextColor(this.mContentActionColor);
            try {
                ImageView searchSubmit = (ImageView) this.searchView.findViewById(android.support.v7.appcompat.R.id.search_go_btn);
                if (searchSubmit != null) {
                    searchSubmit.setColorFilter(this.mContentActionColor, PorterDuff.Mode.SRC_ATOP);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            this.searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                public boolean onQueryTextSubmit(String keyword) {
                    FragmentActivity.this.hiddenKeyBoardForSearchView();
                    if (mListener != null) {
                        mListener.onProcessSearchData(keyword);
                    }
                    return true;
                }

                public boolean onQueryTextChange(String keyword) {
                    if (mListener != null) {
                        mListener.onStartSuggestion(keyword);
                    }
                    return true;
                }
            });
            this.searchView.setOnSearchClickListener(new OnClickListener() {
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.onClickSearchView();
                    }
                }
            });
            this.searchView.setOnCloseListener(new OnCloseListener() {
                public boolean onClose() {
                    if (mListener != null) {
                        mListener.onCloseSearchView();
                    }
                    return false;
                }
            });
            this.searchView.setQueryHint(getString(R.string.title_search_music));
            this.searchView.setSubmitButtonEnabled(true);

        }else {
            Log.d("search","null");

        }

    }

    public void hiddenKeyBoardForSearchView() {
        if (this.searchView != null && !this.searchView.isIconified()) {
            this.searchView.setQuery("", false);
            this.searchView.setIconified(true);
            ApplicationUtils.hiddenVirtualKeyboard(this, this.searchView);
        }
    }

    public void onBackPressed() {
        if (this.searchView == null || this.searchView.isIconified()) {
            super.onBackPressed();
        } else {
            hiddenKeyBoardForSearchView();
        }
    }

    public String getCurrentFragmentTag() {
        if (this.mListFragments != null && this.mListFragments.size() > 0) {
            Fragment mFragment = (Fragment) this.mListFragments.get(0);
            if (mFragment instanceof DBFragment) {
                return mFragment.getTag();
            }
        }
        return null;
    }

    public void showDialogSleepMode() {
        View mView = LayoutInflater.from(this).inflate(R.layout.dialog_sleep_time, null);
        final TextView mTvInfo = (TextView) mView.findViewById(R.id.tv_info);
        mTvInfo.setTypeface(this.mTypefaceNormal);
        if (YPYSettingManager.getSleepMode(this) > 0) {
            mTvInfo.setText(String.format(getString(R.string.format_minutes), new Object[]{String.valueOf(YPYSettingManager.getSleepMode(this))}));
        } else {
            mTvInfo.setText(R.string.title_off);
        }
        SeekArc mCircularVir = (SeekArc) mView.findViewById(R.id.seek_sleep);
        mCircularVir.setProgressColor(getResources().getColor(R.color.colorAccent));
        mCircularVir.setArcColor(getResources().getColor(R.color.main_color_secondary_text));
//        mCircularVir.setMax(24);
        mCircularVir.setProgressWidth(getResources().getDimensionPixelOffset(R.dimen.tiny_margin));
        mCircularVir.setProgress(YPYSettingManager.getSleepMode(this) / 5);
        mCircularVir.setOnSeekArcChangeListener(new SeekArc.OnSeekArcChangeListener() {
            public void onProgressChanged(SeekArc seekArc, int progress, boolean fromUser) {
                try {
                    YPYSettingManager.setSleepMode(FragmentActivity.this, progress * 5);
                    if (progress == 0) {
                        mTvInfo.setText(R.string.title_off);
                    } else {
                        mTvInfo.setText(String.format(FragmentActivity.this.getString(R.string.format_minutes), new Object[]{String.valueOf(YPYSettingManager.getSleepMode(FragmentActivity.this))}));
                    }
                    ArrayList<TrackModel> mListSongs = MusicDataMng.getInstance().getListPlayingTrackObjects();
                    if (mListSongs != null && mListSongs.size() > 0) {
                        FragmentActivity.this.startMusicService(IYPYMusicConstant.ACTION_UPDATE_SLEEP_MODE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            public void onStartTrackingTouch(SeekArc seekArc) {
            }

            public void onStopTrackingTouch(SeekArc seekArc) {
            }
        });
        MaterialDialog.Builder mBuilder = new MaterialDialog.Builder(this);
        mBuilder.backgroundColor(getResources().getColor(R.color.dialog_bg_color));
        mBuilder.title((int) R.string.title_sleep_mode);
        mBuilder.titleColor(getResources().getColor(R.color.main_color_text));
        mBuilder.contentColor(getResources().getColor(R.color.main_color_secondary_text));
        mBuilder.customView(mView, false);
        mBuilder.positiveColor(getResources().getColor(R.color.colorAccent));
        mBuilder.positiveText((int) R.string.title_done);
        mBuilder.autoDismiss(true);
        mBuilder.typeface(this.mTypefaceBold, this.mTypefaceNormal);
        mBuilder.callback(new MaterialDialog.ButtonCallback() {
            public void onPositive(MaterialDialog dialog) {
                super.onPositive(dialog);
            }
        });
        mBuilder.build().show();
    }

    public void goToEqualizer() {
        startActivity(new Intent(this, EqualizerActivity.class));
    }

    public void setUpRecyclerViewAsListView(RecyclerView mListViewTrack, Drawable mDivider) {
        if (mDivider != null) {
            mListViewTrack.addItemDecoration(new DividerItemDecoration(this, 1, mDivider));
        }
        mListViewTrack.setHasFixedSize(true);
        LinearLayoutManager mLayoutMngList = new LinearLayoutManager(this);
        mLayoutMngList.setOrientation(1);
        mListViewTrack.setLayoutManager(mLayoutMngList);
    }

    public void setUpRecyclerViewAsGridView(RecyclerView mGridView, int numberColumn) {
        mGridView.setHasFixedSize(false);
        mGridView.setLayoutManager(new GridLayoutManager(this, numberColumn));
        mGridView.addItemDecoration(new DividerItemDecoration(this, 1, getResources().getDrawable(R.drawable.alpha_divider_verti)));
    }

}
