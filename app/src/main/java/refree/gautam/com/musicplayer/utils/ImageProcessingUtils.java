package refree.gautam.com.musicplayer.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Matrix;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class ImageProcessingUtils {
    private static final int DEFAULT_BUFFER_SIZE = 4096;
    public static final String TAG = ImageProcessingUtils.class.getSimpleName();

    public static Bitmap decodePortraitBitmap(InputStream mInputStream, int desireW, int desireH) {
        if (mInputStream != null) {
            try {
                byte[] mByteArray = convertInputStreamToArray(mInputStream);
                Options option = new Options();
                option.inJustDecodeBounds = true;
                BitmapFactory.decodeByteArray(mByteArray, 0, mByteArray.length, option);
                option.inJustDecodeBounds = false;
                option.inSampleSize = calculateInSampleSize(option, desireW, desireH);
                return BitmapFactory.decodeByteArray(mByteArray, 0, mByteArray.length, option);
            } catch (Exception e) {
                DBLog.LOGE(TAG, "--------->decodeBitmap error=" + e.getMessage());
                e.printStackTrace();
            }
        }
        return null;
    }

    public static byte[] convertInputStreamToArray(InputStream mInputStream) {
        if (mInputStream != null) {
            try {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                while (true) {
                    int len = mInputStream.read(buffer);
                    if (len > -1) {
                        baos.write(buffer, 0, len);
                    } else {
                        baos.flush();
                        return baos.toByteArray();
                    }
                }
            } catch (IOException e) {
                DBLog.LOGE(TAG, "--------->cloneInputStream error=" + e.getMessage());
                e.printStackTrace();
            }
        }
        return null;
    }

    public static Bitmap getRotatedBitmap(Bitmap originalBitmap) {
        Matrix matrix = new Matrix();
        matrix.postRotate(90.0f);
        Bitmap rotatedBitmap = Bitmap.createBitmap(originalBitmap, 0, 0, originalBitmap.getWidth(), originalBitmap.getHeight(), matrix, true);
        if (rotatedBitmap != null) {
            originalBitmap.recycle();
        }
        return rotatedBitmap;
    }

    public static int calculateInSampleSize(Options options, int reqWidth, int reqHeight) {
        int height = options.outHeight;
        int width = options.outWidth;
        if (height <= reqHeight && width <= reqWidth) {
            return 1;
        }
        if (width > height) {
            return Math.round(((float) height) / ((float) reqHeight));
        }
        return Math.round(((float) width) / ((float) reqWidth));
    }
}
