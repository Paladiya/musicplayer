package refree.gautam.com.musicplayer.InterFace;

/**
 * Created by admin on 3/23/2018.
 */

public interface  IDBMusicPlayerListener {

    void onPlayerError();

    void onPlayerLoading();

    void onPlayerStop();

    void onPlayerStopLoading();

    void onPlayerUpdatePos(int i);

    void onPlayerUpdateState(boolean z);

    void onPlayerUpdateStatus();
}
