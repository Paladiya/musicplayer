package refree.gautam.com.musicplayer.Models;

import com.google.gson.annotations.SerializedName;

public class UserModel {
    @SerializedName("avatar_url")
    private String avatar;
    @SerializedName("id")
    private String id;
    @SerializedName("username")
    private String username;

    public UserModel(String username) {
        this.username = username;
    }

    public UserModel(String id, String avatar, String username) {
        this.id = id;
        this.avatar = avatar;
        this.username = username;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAvatar() {
        return this.avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public UserModel cloneObject() {
        return new UserModel(this.id, this.avatar, this.username);
    }
}
