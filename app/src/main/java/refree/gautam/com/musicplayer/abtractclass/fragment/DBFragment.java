package refree.gautam.com.musicplayer.abtractclass.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import java.util.ArrayList;

import refree.gautam.com.musicplayer.R;
import refree.gautam.com.musicplayer.utils.ApplicationUtils;
import refree.gautam.com.musicplayer.utils.StringUtils;

public abstract class DBFragment extends Fragment implements IDBFragmentConstants {
    public static final String TAG = DBFragment.class.getSimpleName();
    private boolean isAllowFindViewContinous;
    private boolean isCreated;
    private boolean isExtractData;
    private boolean isFirstInTab;
    private boolean isLoadingData;
    public int mIdFragment;
    private RelativeLayout mLayoutAds;
    public ArrayList<Fragment> mListFragments;
    public String mNameFragment;
    public View mRootView;


    public abstract void findView();

    public abstract View onInflateLayout(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle);

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.mRootView = onInflateLayout(inflater, container, savedInstanceState);
        return this.mRootView;
    }

    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (!this.isExtractData) {
            this.isExtractData = true;
            onExtractData();
            findView();
        } else if (this.isAllowFindViewContinous) {
            findView();
        }
        this.isCreated = true;
    }

    public void createArrayFragment() {
        this.mListFragments = new ArrayList();
    }

    public void onStart() {
        super.onStart();
        if (this.isAllowFindViewContinous && this.isCreated) {
            findView();
        }
    }

    public void onExtractData() {
        Bundle args = getArguments();
        if (args != null) {
            this.mNameFragment = args.getString(IDBFragmentConstants.KEY_NAME_FRAGMENT);
            this.mIdFragment = args.getInt(IDBFragmentConstants.KEY_ID_FRAGMENT);
        }
    }

    public void backToHome(FragmentActivity mContext) {
        FragmentTransaction mFragmentTransaction = mContext.getSupportFragmentManager().beginTransaction();
        mFragmentTransaction.remove(this);
        Fragment mFragmentHome = getFragmentHome(mContext);
        if (mFragmentHome != null) {
            mFragmentTransaction.show(mFragmentHome);
        }
        mFragmentTransaction.commit();
    }

    public void setAllowFindViewContinous(boolean isAllowFindViewContinous) {
        this.isAllowFindViewContinous = isAllowFindViewContinous;
    }

    public Fragment getFragmentHome(FragmentActivity mContext) {
        if (this.mIdFragment > 0) {
            return mContext.getSupportFragmentManager().findFragmentById(this.mIdFragment);
        }
        if (StringUtils.isEmptyString(this.mNameFragment)) {
            return null;
        }
        return mContext.getSupportFragmentManager().findFragmentByTag(this.mNameFragment);
    }

    public void notifyData() {
    }

    public void startLoadData() {
    }

    public void onNetworkChange(boolean isNetworkOn) {
    }

    public boolean isLoadingData() {
        return this.isLoadingData;
    }

    public void setLoadingData(boolean loadingData) {
        this.isLoadingData = loadingData;
    }

    public boolean isFirstInTab() {
        return this.isFirstInTab;
    }

    public void setFirstInTab(boolean firstInTab) {
        this.isFirstInTab = firstInTab;
    }

    public void hideBannerAds() {
        if (this.mLayoutAds != null) {
            this.mLayoutAds.setVisibility(View.GONE);
        }
    }


    public void showBannerAds() {
        if (this.mLayoutAds != null) {
            this.mLayoutAds.setVisibility(View.VISIBLE);
        }
    }

    public boolean isCheckBack() {
        return false;
    }

    public void justNotifyData() {
    }
}
