package refree.gautam.com.musicplayer.utils;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Environment;
import android.os.ResultReceiver;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class ApplicationUtils {
    public static int getColor(Context mContext, int attId) {
        try {
            TypedValue typedValue = new TypedValue();
            if (mContext.getTheme().resolveAttribute(attId, typedValue, true)) {
                return typedValue.data;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String getMd5Hash(String input) {
        try {
            String bigInteger = new BigInteger(1, MessageDigest.getInstance("MD5").digest(input.getBytes())).toString(16);
            while (bigInteger.length() < 32) {
                bigInteger = "0" + bigInteger;
            }
            return bigInteger;
        } catch (NoSuchAlgorithmException e) {
            Log.e("MD5", e.getMessage());
            return null;
        }
    }

    public static boolean isOnline(Context mContext) {
        NetworkInfo netInfo = ((ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (netInfo == null || !netInfo.isConnectedOrConnecting()) {
            return false;
        }
        return true;
    }

    public static boolean hasSDcard() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    public static String getDeviceId(Context mContext) {
        String mDeviceID = ((TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
        if (mDeviceID == null || mDeviceID.equals("0")) {
            return Secure.getString(mContext.getContentResolver(), "android_id");
        }
        return mDeviceID;
    }

    public static String getNameApp(Context mContext) {
        ApplicationInfo ai;
        PackageManager pm = mContext.getPackageManager();
        try {
            ai = pm.getApplicationInfo(mContext.getPackageName(), 0);
        } catch (NameNotFoundException e) {
            ai = null;
        }
        return (String) (ai != null ? pm.getApplicationLabel(ai) : "(unknown)");
    }

    public static int getVersionCode(Context mContext) {
        int i = 0;
        try {
            return mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0).versionCode;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
            return i;
        }
    }

    public static String getSignature(Context mContext) {
        try {
            return mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 64).signatures[0].toString();
        } catch (NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void hiddenVirtualKeyboard(Context mContext, View myEditText) {
        try {
            ((InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(myEditText.getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showVirtualKeyboad(Context mContext, EditText myEditText) {
        try {
            ((InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(myEditText, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getHashKey(Context mContext) {
        try {
            Signature[] signatureArr = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 64).signatures;
            if (0 < signatureArr.length) {
                Signature signature = signatureArr[0];
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                return Base64.encodeToString(md.digest(), 0);
            }
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e2) {
            e2.printStackTrace();
        }
        return null;
    }

    public static String getVersionName(Context mContext) {
        try {
            return mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0).versionName;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getAndroidVersion() {
        try {
            int e = VERSION.SDK_INT;
            if (e == 14) {
                return "android 4.0";
            }
            if (e == 15) {
                return "android 4.0.3";
            }
            if (e == 16) {
                return "android 4.1.2";
            }
            if (e == 17) {
                return "android 4.2.2";
            }
            if (e == 18) {
                return "android 4.3.1";
            }
            if (e == 19) {
                return "android 4.4.2";
            }
            if (e == 21) {
                return "android 5.0";
            }
            return "Unknown";
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return null;
    }

    public static String getCarierName(Context mContext) {
        String carrierName = null;
        try {
            carrierName = ((TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE)).getNetworkOperatorName();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return carrierName;
    }

    public static NetworkInfo getConnectionType(Context mContext) {
        try {
            return ((ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        } catch (Exception var2) {
            var2.printStackTrace();
            return null;
        }
    }

    public static String getDeviceName() {
        try {
            String e = Build.MANUFACTURER;
            String model = Build.MODEL;
            if (!StringUtils.isEmptyString(e)) {
                return !StringUtils.isEmptyString(model) ? e + "-" + model : e;
            } else {
                if (!StringUtils.isEmptyString(model)) {
                    return model;
                }
                return "Unknown";
            }
        } catch (Exception var2) {
            var2.printStackTrace();
        }
        return null;
    }

    public static String colorToHexString(int color) {
        return String.format("#%08X", new Object[]{Integer.valueOf(color & -1)});
    }

    public static byte[] createChecksum(String filename) throws Exception {
        return createChecksum(new FileInputStream(filename));
    }

    public static byte[] createChecksum(InputStream fis) throws Exception {
        MessageDigest complete = MessageDigest.getInstance("MD5");
        byte[] buf = new byte[8192];
        while (true) {
            int len = fis.read(buf);
            if (len > 0) {
                complete.update(buf, 0, len);
            } else {
                fis.close();
                return complete.digest();
            }
        }
    }

    public static String getMD5Checksum(String filename) throws Exception {
        return getMD5Checksum(new FileInputStream(filename));
    }

    public static String getMD5Checksum(InputStream fis) throws Exception {
        String result = "";
        for (byte b : createChecksum(fis)) {
            result = result + Integer.toString((b & 255) + 256, 16).substring(1);
        }
        return result;
    }

    public static void showSoftInputUnchecked(Context mContext) {
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            Method showSoftInputUnchecked = null;
            try {
                showSoftInputUnchecked = imm.getClass().getMethod("showSoftInputUnchecked", new Class[]{Integer.TYPE, ResultReceiver.class});
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
            if (showSoftInputUnchecked != null) {
                try {
                    showSoftInputUnchecked.invoke(imm, new Object[]{Integer.valueOf(0), null});
                } catch (IllegalAccessException e2) {
                    e2.printStackTrace();
                } catch (InvocationTargetException e3) {
                    e3.printStackTrace();
                }
            }
        }
    }


}
