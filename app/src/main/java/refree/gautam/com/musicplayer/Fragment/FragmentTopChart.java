package refree.gautam.com.musicplayer.Fragment;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;

import refree.gautam.com.musicplayer.Adapter.TrackDataAdapter;
import refree.gautam.com.musicplayer.DataManger.MusicNetUtils;
import refree.gautam.com.musicplayer.InterFace.IXMusicConstants;
import refree.gautam.com.musicplayer.InterFace.IXmusicSoundCloudConstants;
import refree.gautam.com.musicplayer.Models.ConfigureModel;
import refree.gautam.com.musicplayer.Models.TrackModel;
import refree.gautam.com.musicplayer.R;
import refree.gautam.com.musicplayer.MainActivity;
import refree.gautam.com.musicplayer.abtractclass.fragment.DBFragment;
import refree.gautam.com.musicplayer.executor.DBExecutorSupplier;
import refree.gautam.com.musicplayer.utils.ApplicationUtils;
import refree.gautam.com.musicplayer.view.CircularProgressBar;

public class FragmentTopChart extends DBFragment implements IXMusicConstants {
    public static final String TAG = FragmentTopChart.class.getSimpleName();
    private boolean isDestroy;
    private MainActivity mContext;
    private ArrayList<TrackModel> mListTracks;
    private ArrayList<TrackModel> mOriginalTrack;
    private CircularProgressBar mProgressBar;
    private RecyclerView mRecyclerViewTrack;
    private TrackDataAdapter mTrackDataAdapter;
    private TextView mTvNoResult;
    private int mTypeUI;

    class C14651 implements Runnable {
        C14651() {
        }

        public void run() {
            ConfigureModel mModel = FragmentTopChart.this.mContext.mTotalMng.getConfigureModel();
            String genre = mModel != null ? mModel.getTopChartGenre() : IXmusicSoundCloudConstants.ALL_MUSIC_GENRE;
            String kind = mModel != null ? mModel.getTopChartKind() : IXmusicSoundCloudConstants.KIND_TOP;
            Log.e("DCM", "=====>genre=" + genre + "==>kind=" + kind);
            final ArrayList<TrackModel> mTracks = MusicNetUtils.getListHotTrackObjectsInGenre(genre, kind, 0, 50);
            final ArrayList<TrackModel> mListFinalTrack = FragmentTopChart.this.mContext.mTotalMng.getAllTrackWithAdmob(FragmentTopChart.this.mContext, mTracks, FragmentTopChart.this.mTypeUI);
            FragmentTopChart.this.mContext.runOnUiThread(new Runnable() {
                public void run() {
                    if (!FragmentTopChart.this.isDestroy) {
                        FragmentTopChart.this.mProgressBar.setVisibility(View.GONE);
                        FragmentTopChart.this.setUpInfo(mListFinalTrack, mTracks);
                    }
                }
            });
        }
    }

    public View onInflateLayout(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        return inflater.inflate(R.layout.fragment_recyclerview, container, false);
    }

    public void findView() {
        this.mContext = (MainActivity) getActivity();
        this.mContext = (MainActivity) getActivity();
        this.mTvNoResult = (TextView) this.mRootView.findViewById(R.id.tv_no_result);
        this.mTvNoResult.setTypeface(this.mContext.mTypefaceNormal);
        this.mRecyclerViewTrack = (RecyclerView) this.mRootView.findViewById(R.id.list_datas);
        this.mProgressBar = (CircularProgressBar) this.mRootView.findViewById(R.id.progressBar1);
        ConfigureModel configureModel = this.mContext.mTotalMng.getConfigureModel();
        this.mTypeUI = configureModel != null ? configureModel.getTypeTopChart() : 1;
        if (this.mTypeUI == 1) {
            this.mContext.setUpRecyclerViewAsListView(this.mRecyclerViewTrack, null);
        } else {
            this.mContext.setUpRecyclerViewAsGridView(this.mRecyclerViewTrack, 2);
        }
        if (isFirstInTab()) {
            startLoadData();
        }
    }

    public void startLoadData() {
        if (!isLoadingData() && this.mContext != null) {
            setLoadingData(true);
            startGetData(true);
        }
    }

    public void onNetworkChange(boolean isNetworkOn) {
        super.onNetworkChange(isNetworkOn);
        if (isNetworkOn && this.mTrackDataAdapter == null && this.mContext != null && this.mProgressBar != null) {
            startGetData(true);
        }
    }

    private void startGetData(boolean isNeedShowProgress) {
        if (!ApplicationUtils.isOnline(this.mContext)) {
            this.mProgressBar.setVisibility(View.GONE);
            this.mTvNoResult.setVisibility(View.VISIBLE);
            this.mTvNoResult.setText(R.string.info_lose_internet);
        } else if (this.mProgressBar.getVisibility() != View.VISIBLE || isNeedShowProgress) {
            this.mTvNoResult.setText(R.string.title_no_songs);
            this.mProgressBar.setVisibility(View.VISIBLE);
            this.mTvNoResult.setVisibility(View.GONE);
            DBExecutorSupplier.getInstance().forBackgroundTasks().execute(new C14651());
        }
    }

    private void setUpInfo(ArrayList<TrackModel> mListTracks, final ArrayList<TrackModel> mOriginalTrack) {
        this.mRecyclerViewTrack.setAdapter(null);
        if (this.mListTracks != null) {
            this.mListTracks.clear();
            this.mListTracks = null;
        }
        if (this.mOriginalTrack != null) {
            this.mOriginalTrack.clear();
            this.mOriginalTrack = null;
        }
        this.mListTracks = mListTracks;
        this.mOriginalTrack = mOriginalTrack;
        if (mListTracks != null && mListTracks.size() > 0) {
            this.mTrackDataAdapter = new TrackDataAdapter(this.mContext, mListTracks, this.mContext.mTypefaceBold, this.mContext.mTypefaceLight, this.mTypeUI);
            this.mRecyclerViewTrack.setAdapter(this.mTrackDataAdapter);
            this.mTrackDataAdapter.setOnTrackListener(new TrackDataAdapter.OnTrackListener() {
                public void onListenTrack(TrackModel mTrackObject) {
                    FragmentTopChart.this.mContext.hiddenKeyBoardForSearchView();
                    FragmentTopChart.this.mContext.startPlayingList(mTrackObject, mOriginalTrack);
                }

                public void onShowMenu(View mView, TrackModel mTrackObject) {
                    FragmentTopChart.this.mContext.showPopupMenu(mView, mTrackObject);
                }
            });
        }
        updateInfo();
    }

    private void updateInfo() {
        int i = 0;
        if (this.mTvNoResult != null) {
            boolean b;
            if (this.mListTracks == null || this.mListTracks.size() <= 0) {
                b = false;
            } else {
                b = true;
            }
            TextView textView = this.mTvNoResult;
            if (b) {
                i = 8;
            }
            textView.setVisibility(i);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        this.isDestroy = true;
        if (this.mOriginalTrack != null) {
            this.mOriginalTrack.clear();
            this.mOriginalTrack = null;
        }
        if (this.mListTracks != null) {
            this.mListTracks.clear();
            this.mListTracks = null;
        }
    }
}
