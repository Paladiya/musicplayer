package refree.gautam.com.musicplayer.InterFace;

import android.os.AsyncTask;

public class YPYTask extends AsyncTask<Void, Void, Void> {
    private IYPYTaskListener mDownloadListener;

    public YPYTask(IYPYTaskListener mDownloadListener) {
        this.mDownloadListener = mDownloadListener;
    }

    protected void onPreExecute() {
        if (this.mDownloadListener != null) {
            this.mDownloadListener.onPreExcute();
        }
    }

    protected Void doInBackground(Void... params) {
        if (this.mDownloadListener != null) {
            this.mDownloadListener.onDoInBackground();
        }
        return null;
    }

    protected void onPostExecute(Void result) {
        if (this.mDownloadListener != null) {
            this.mDownloadListener.onPostExcute();
        }
    }
}
