package refree.gautam.com.musicplayer.imageloader.target;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.ViewGroup;


public class GlideViewGroupTarget extends GlideGroupTarget<Bitmap> {
    private Context context;

    public GlideViewGroupTarget(Context context, ViewGroup view) {
        super(view);
        this.context = context;
    }

    protected void setResource(Bitmap resource) {
        ((ViewGroup) this.view).setBackground(new BitmapDrawable(this.context.getResources(), resource));
    }


}
