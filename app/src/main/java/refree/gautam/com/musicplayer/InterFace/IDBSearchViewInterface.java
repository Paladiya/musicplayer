package refree.gautam.com.musicplayer.InterFace;

public interface IDBSearchViewInterface {
    void onClickSearchView();

    void onCloseSearchView();

    void onProcessSearchData(String str);

    void onStartSuggestion(String str);
}
