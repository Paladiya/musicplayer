package refree.gautam.com.musicplayer.Models;

import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.ads.NativeExpressAdView;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.Iterator;

public class PlaylistModel {
    private transient String artist;
    private transient String artwork;
    @SerializedName("id")
    private long id;
    private transient boolean isNativeAds;
    @SerializedName("datas")
    private ArrayList<Long> listTrackIds;
    private transient ArrayList<TrackModel> listTrackObjects;
    @SerializedName("name")
    private String name;
    private transient NativeExpressAdView nativeExpressAdView;

    public PlaylistModel(long id, String name) {
        this.id = id;
        this.name = name;
        this.listTrackObjects = new ArrayList();
    }

    public PlaylistModel(String name) {
        this.name = name;
    }

    public PlaylistModel() {

    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<TrackModel> getListTrackObjects() {
        return this.listTrackObjects;
    }

    public void setListTrackObjects(ArrayList<TrackModel> listTrackObjects) {
        this.listTrackObjects = listTrackObjects;
    }

    public ArrayList<Long> getListTrackIds() {
        return this.listTrackIds;
    }

    public void setListTrackIds(ArrayList<Long> listTrackIds) {
        this.listTrackIds = listTrackIds;
        if (this.listTrackObjects == null) {
            this.listTrackObjects = new ArrayList();
        }
    }

    public void addTrackObject(TrackModel mTrackObject, boolean isAddId) {
        if (this.listTrackObjects == null) {
            this.listTrackObjects = new ArrayList();
        }
        if (this.listTrackObjects != null && mTrackObject != null) {
            this.listTrackObjects.add(mTrackObject);
            if (isAddId && this.listTrackIds != null) {
                this.listTrackIds.add(Long.valueOf(mTrackObject.getId()));
            }
        }
    }

    public void removeTrackObject(TrackModel mTrackObject) {
        if (this.listTrackObjects != null && this.listTrackObjects.size() > 0 && mTrackObject != null) {
            Iterator<TrackModel> mIterator = this.listTrackObjects.iterator();
            while (mIterator.hasNext()) {
                if (((TrackModel) mIterator.next()).getId() == mTrackObject.getId()) {
                    mIterator.remove();
                    break;
                }
            }
            if (this.listTrackIds != null && this.listTrackIds.size() > 0) {
                Iterator<Long> mTrackIdIterator = this.listTrackIds.iterator();
                while (mTrackIdIterator.hasNext()) {
                    if (((Long) mTrackIdIterator.next()).longValue() == mTrackObject.getId()) {
                        mTrackIdIterator.remove();
                        return;
                    }
                }
            }
        }
    }

    public boolean isSongAlreadyExited(long id) {
        if (this.listTrackObjects != null && this.listTrackObjects.size() > 0) {
            Iterator it = this.listTrackObjects.iterator();
            while (it.hasNext()) {
                if (((TrackModel) it.next()).getId() == id) {
                    return true;
                }
            }
        }
        return false;
    }

    public String getArtist() {
        return this.artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getArtwork() {
        if (!TextUtils.isEmpty(this.artwork) || this.listTrackObjects == null || this.listTrackObjects.size() <= 0) {
            return this.artwork;
        }
        return ((TrackModel) this.listTrackObjects.get(0)).getArtworkUrl();
    }

    public Uri getURI() {
        if (this.listTrackObjects == null || this.listTrackObjects.size() <= 0) {
            return null;
        }
        return ((TrackModel) this.listTrackObjects.get(0)).getURI();
    }

    public void setArtwork(String artwork) {
        this.artwork = artwork;
    }

    public boolean isNativeAds() {
        return this.isNativeAds;
    }

    public void setNativeAds(boolean nativeAds) {
        this.isNativeAds = nativeAds;
    }

    public NativeExpressAdView getNativeExpressAdView() {
        return this.nativeExpressAdView;
    }

    public void setNativeExpressAdView(NativeExpressAdView nativeExpressAdView) {
        this.nativeExpressAdView = nativeExpressAdView;
    }

    public long getNumberVideo() {
        if (this.listTrackObjects == null || this.listTrackObjects.size() <= 0) {
            return 0;
        }
        return (long) this.listTrackObjects.size();
    }
}
