package refree.gautam.com.musicplayer.Models;

import com.google.gson.annotations.SerializedName;

public class ConfigureModel {
    @SerializedName("bg")
    private String bg;
    @SerializedName("top_chart_genre")
    private String topChartGenre;
    @SerializedName("top_chart_kind")
    private String topChartKind;
    @SerializedName("ui_detail")
    private int typeDetail;
    @SerializedName("ui_genre")
    private int typeGenre;
    @SerializedName("ui_my_music")
    private int typeMyMusic;
    @SerializedName("ui_playlist")
    private int typePlaylist;
    @SerializedName("ui_search")
    private int typeSearch;
    @SerializedName("ui_top_chart")
    private int typeTopChart;

    public String getBg() {
        return this.bg;
    }

    public void setBg(String bg) {
        this.bg = bg;
    }

    public int getTypeTopChart() {
        return this.typeTopChart;
    }

    public void setTypeTopChart(int typeTopChart) {
        this.typeTopChart = typeTopChart;
    }

    public int getTypeGenre() {
        return this.typeGenre;
    }

    public void setTypeGenre(int typeGenre) {
        this.typeGenre = typeGenre;
    }

    public int getTypeMyMusic() {
        return this.typeMyMusic;
    }

    public void setTypeMyMusic(int typeMyMusic) {
        this.typeMyMusic = typeMyMusic;
    }

    public String getTopChartGenre() {
        return this.topChartGenre;
    }

    public void setTopChartGenre(String topChartGenre) {
        this.topChartGenre = topChartGenre;
    }

    public String getTopChartKind() {
        return this.topChartKind;
    }

    public void setTopChartKind(String topChartKind) {
        this.topChartKind = topChartKind;
    }

    public int getTypePlaylist() {
        return this.typePlaylist;
    }

    public void setTypePlaylist(int typePlaylist) {
        this.typePlaylist = typePlaylist;
    }

    public int getTypeDetail() {
        return this.typeDetail;
    }

    public void setTypeDetail(int typeDetail) {
        this.typeDetail = typeDetail;
    }

    public int getTypeSearch() {
        return this.typeSearch;
    }

    public void setTypeSearch(int typeSearch) {
        this.typeSearch = typeSearch;
    }
}
