package refree.gautam.com.musicplayer.playservice;

public interface IMusicFocusableListener {
    void onGainedAudioFocus();

    void onLostAudioFocus(boolean z);
}
