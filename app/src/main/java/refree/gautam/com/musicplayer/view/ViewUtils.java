package refree.gautam.com.musicplayer.view;

import android.content.res.Resources;
import android.util.TypedValue;
import android.view.View;

public class ViewUtils {
    public static int dpToPx(float dp, Resources resources) {
        return (int) TypedValue.applyDimension(1, dp, resources.getDisplayMetrics());
    }

    public static int getRelativeTop(View myView) {
        if (myView.getId() == 16908290) {
            return myView.getTop();
        }
        return getRelativeTop((View) myView.getParent()) + myView.getTop();
    }

    public static int getRelativeLeft(View myView) {
        if (myView.getId() == 16908290) {
            return myView.getLeft();
        }
        return getRelativeLeft((View) myView.getParent()) + myView.getLeft();
    }
}
