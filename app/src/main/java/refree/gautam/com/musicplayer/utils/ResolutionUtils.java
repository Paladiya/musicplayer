package refree.gautam.com.musicplayer.utils;

import android.app.Activity;
import android.content.Context;
import android.view.Display;

public class ResolutionUtils {
    public static final String LANSCAPE = "Landscape";
    public static final String PORTRAIT = "Portrait";

    public static int[] getDeviceResolution(Activity mContext) {
        int[] res = null;
        Display display = mContext.getWindowManager().getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();
        int i = mContext.getResources().getConfiguration().orientation;
        int finalWidth;
        int finalHeight;
        if (i == 1) {
            res = new int[2];
            finalWidth = height >= width ? width : height;
            if (height <= width) {
                finalHeight = width;
            } else {
                finalHeight = height;
            }
            res[0] = finalWidth;
            res[1] = finalHeight;
        } else if (i == 2) {
            res = new int[2];
            if (height <= width) {
                finalWidth = width;
            } else {
                finalWidth = height;
            }
            if (height >= width) {
                finalHeight = width;
            } else {
                finalHeight = height;
            }
            res[0] = finalWidth;
            res[1] = finalHeight;
        }
        return res;
    }

    public static float convertDpToPixel(Context context, float dp) {
        return dp * (((float) context.getResources().getDisplayMetrics().densityDpi) / 160.0f);
    }

    public static float convertPixelsToDp(Context context, float px) {
        return px / (((float) context.getResources().getDisplayMetrics().densityDpi) / 160.0f);
    }

    public static float convertPixelsToSp(Context context, float px) {
        return px / context.getResources().getDisplayMetrics().scaledDensity;
    }

    public static float convertSpToPixel(Context context, float sp) {
        return (context.getResources().getDisplayMetrics().scaledDensity / 160.0f) * sp;
    }
}
