package refree.gautam.com.musicplayer.Models;

import com.google.gson.annotations.SerializedName;

public class CollectionModel {
    @SerializedName("track")
    private TrackModel trackObject;

    public TrackModel getTrackObject() {
        return this.trackObject;
    }

    public void setTrackObject(TrackModel trackObject) {
        this.trackObject = trackObject;
    }
}
