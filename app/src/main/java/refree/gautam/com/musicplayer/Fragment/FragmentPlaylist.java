package refree.gautam.com.musicplayer.Fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.GridLayoutManager.SpanSizeLookup;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import java.util.ArrayList;

import refree.gautam.com.musicplayer.Adapter.PlaylistDataAdapter;
import refree.gautam.com.musicplayer.InterFace.IXMusicConstants;
import refree.gautam.com.musicplayer.InterFace.IYPYCallback;
import refree.gautam.com.musicplayer.Models.ConfigureModel;
import refree.gautam.com.musicplayer.Models.PlaylistModel;
import refree.gautam.com.musicplayer.Models.TrackModel;
import refree.gautam.com.musicplayer.R;
import refree.gautam.com.musicplayer.MainActivity;
import refree.gautam.com.musicplayer.abtractclass.fragment.DBFragment;
import refree.gautam.com.musicplayer.executor.DBExecutorSupplier;

public class FragmentPlaylist extends DBFragment implements IXMusicConstants, OnClickListener {
    public static final String TAG = FragmentPlaylist.class.getSimpleName();
    private MainActivity mContext;
    private Handler mHandler = new Handler();
    private View mHeader;
    private ArrayList<PlaylistModel> mListPlaylist;
    private PlaylistDataAdapter mPlaylistDataAdapter;
    private RecyclerView mRecyclerView;
    private int mTypeUI;

    class C14531 extends SpanSizeLookup {
        C14531() {
        }

        public int getSpanSize(int position) {
            switch (FragmentPlaylist.this.mPlaylistDataAdapter.getItemViewType(position)) {
                case -1:
                    return 2;
                default:
                    return 1;
            }
        }
    }

    class C14552 implements Runnable {
        C14552() {
        }

        public void run() {
            ArrayList<PlaylistModel> mListPlaylist = FragmentPlaylist.this.mContext.mTotalMng.getListPlaylistObjects();
            if (mListPlaylist == null) {
                FragmentPlaylist.this.mContext.mTotalMng.readCached(5);
                FragmentPlaylist.this.mContext.mTotalMng.readPlaylistCached();
                mListPlaylist = FragmentPlaylist.this.mContext.mTotalMng.getListPlaylistObjects();
            }
            final ArrayList<PlaylistModel> mNewLists = FragmentPlaylist.this.mContext.mTotalMng.getAllPlaylistObjectWithAdmob(FragmentPlaylist.this.mContext, mListPlaylist, FragmentPlaylist.this.mTypeUI);
            FragmentPlaylist.this.mContext.runOnUiThread(new Runnable() {
                public void run() {
                    FragmentPlaylist.this.setUpInfo(mNewLists);
                }
            });
        }
    }

    class C14563 implements PlaylistDataAdapter.OnPlaylistListener {
        C14563() {
        }

        public void onViewDetail(PlaylistModel mPlaylistObject) {
            FragmentPlaylist.this.mContext.goToDetailPlaylist(mPlaylistObject, 12);
        }

        public void showPopUpMenu(View v, PlaylistModel mPlaylistObject) {
            FragmentPlaylist.this.showMenuForPlaylist(v, mPlaylistObject);
        }
    }

    class C14606 implements IYPYCallback {
        C14606() {
        }

        public void onAction() {
            FragmentPlaylist.this.notifyData();
        }
    }

    public View onInflateLayout(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_recyclerview, container, false);
    }

    public void findView() {
        this.mContext = (MainActivity) getActivity();
        this.mRecyclerView = (RecyclerView) this.mRootView.findViewById(R.id.list_datas);
        setUpHeader();
        ConfigureModel configureModel = this.mContext.mTotalMng.getConfigureModel();
        this.mTypeUI = configureModel != null ? configureModel.getTypePlaylist() : 1;
        if (this.mTypeUI == 1) {
            this.mContext.setUpRecyclerViewAsListView(this.mRecyclerView, null);
            return;
        }
        this.mContext.setUpRecyclerViewAsGridView(this.mRecyclerView, 2);
        ((GridLayoutManager) this.mRecyclerView.getLayoutManager()).setSpanSizeLookup(new C14531());
    }

    public void setUpHeader() {
        this.mHeader = LayoutInflater.from(this.mContext).inflate(R.layout.item_header_playlist, null);
        ((TextView) this.mHeader.findViewById(R.id.tv_add_new_playlist)).setTypeface(this.mContext.mTypefaceBold);
        this.mHeader.findViewById(R.id.btn_add_playlist).setOnClickListener(this);
    }

    public void startLoadData() {
        super.startLoadData();
        if (!isLoadingData() && this.mContext != null) {
            setLoadingData(true);
            startGetPlaylist();
        }
    }

    private void startGetPlaylist() {
        DBExecutorSupplier.getInstance().forBackgroundTasks().execute(new C14552());
    }

    private void setUpInfo(ArrayList<PlaylistModel> mListPlaylistObjects) {
        this.mRecyclerView.setAdapter(null);
        if (this.mListPlaylist != null) {
            this.mListPlaylist.clear();
            this.mListPlaylist = null;
        }
        this.mListPlaylist = mListPlaylistObjects;
        if (mListPlaylistObjects != null) {
            this.mPlaylistDataAdapter = new PlaylistDataAdapter(this.mContext, mListPlaylistObjects, this.mContext.mTypefaceBold, this.mContext.mTypefaceLight, this.mHeader, this.mTypeUI);
            this.mRecyclerView.setAdapter(this.mPlaylistDataAdapter);
            this.mPlaylistDataAdapter.setOnPlaylistListener(new C14563());
        }
    }

    public void showMenuForPlaylist(View v, final PlaylistModel mCurrentPlaylist) {
        PopupMenu popupMenu = new PopupMenu(this.mContext, v);
        popupMenu.getMenuInflater().inflate(R.menu.menu_playlist, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new OnMenuItemClickListener() {

            class C14571 implements IYPYCallback {
                C14571() {
                }

                public void onAction() {
                    if (FragmentPlaylist.this.mPlaylistDataAdapter != null) {
                        FragmentPlaylist.this.mPlaylistDataAdapter.notifyDataSetChanged();
                    }
                }
            }

            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_play_all:
                        if (mCurrentPlaylist != null) {
                            ArrayList<TrackModel> mListTrackObjects = mCurrentPlaylist.getListTrackObjects();
                            if (mListTrackObjects != null && mListTrackObjects.size() > 0) {
                                FragmentPlaylist.this.mContext.startPlayingList((TrackModel) mListTrackObjects.get(0), mListTrackObjects);
                                break;
                            }
                            FragmentPlaylist.this.mContext.showToast((int) R.string.info_nosong_playlist);
                            break;
                        }
                        break;
                    case R.id.action_delete_playlist:
                        if (mCurrentPlaylist != null) {
                            FragmentPlaylist.this.showDialogDelete(mCurrentPlaylist);
                            break;
                        }
                        break;
                    case R.id.action_rename_playlist:
                        if (mCurrentPlaylist != null) {
                            FragmentPlaylist.this.mContext.showDialogCreatePlaylist(true, mCurrentPlaylist, new C14571());
                            break;
                        }
                        break;
                }
                return true;
            }
        });
        popupMenu.show();
    }

    private void showDialogDelete(final PlaylistModel mPlaylistObject) {
        this.mContext.showFullDialog(R.string.title_confirm, getString(R.string.info_delete_playlist), R.string.title_ok, R.string.title_cancel, new IYPYCallback() {
            public void onAction() {
                FragmentPlaylist.this.mContext.mTotalMng.removePlaylistObject(mPlaylistObject);
                FragmentPlaylist.this.notifyData();
            }
        });
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add_playlist:
                this.mContext.showDialogCreatePlaylist(false, null, new C14606());
                return;
            default:
                return;
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.mListPlaylist != null) {
            this.mListPlaylist.clear();
            this.mListPlaylist = null;
        }
        this.mHandler.removeCallbacksAndMessages(null);
    }

    public void notifyData() {
        if (this.mContext != null) {
            startGetPlaylist();
        }
    }
}
