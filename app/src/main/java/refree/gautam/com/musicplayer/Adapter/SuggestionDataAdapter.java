package refree.gautam.com.musicplayer.Adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;

import refree.gautam.com.musicplayer.InterFace.IXMusicConstants;
import refree.gautam.com.musicplayer.R;

public class SuggestionDataAdapter extends CursorAdapter implements IXMusicConstants {
    public static final String TAG = SuggestionDataAdapter.class.getSimpleName();
    private ArrayList<String> mListItems;

    public SuggestionDataAdapter(Context context, Cursor c, ArrayList<String> items) {
        super(context, c, false);
        this.mListItems = items;
    }

    public void bindView(View view, Context context, Cursor cursor) {
        ((TextView) view.findViewById(R.id.tv_name_options)).setText((CharSequence) this.mListItems.get(cursor.getPosition()));
    }

    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        if (cursor.getPosition() < 0 || cursor.getPosition() >= this.mListItems.size()) {
            return null;
        }
        View view = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.item_suggestion, parent, false);
        ((TextView) view.findViewById(R.id.tv_name_options)).setText((CharSequence) this.mListItems.get(cursor.getPosition()));
        return view;
    }
}
