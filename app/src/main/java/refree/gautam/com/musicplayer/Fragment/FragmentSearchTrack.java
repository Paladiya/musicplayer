package refree.gautam.com.musicplayer.Fragment;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;

import refree.gautam.com.musicplayer.Adapter.TrackDataAdapter;
import refree.gautam.com.musicplayer.DataManger.MusicNetUtils;
import refree.gautam.com.musicplayer.InterFace.IXMusicConstants;
import refree.gautam.com.musicplayer.Models.ConfigureModel;
import refree.gautam.com.musicplayer.Models.TrackModel;
import refree.gautam.com.musicplayer.R;
import refree.gautam.com.musicplayer.MainActivity;
import refree.gautam.com.musicplayer.abtractclass.fragment.DBFragment;
import refree.gautam.com.musicplayer.executor.DBExecutorSupplier;
import refree.gautam.com.musicplayer.utils.ApplicationUtils;
import refree.gautam.com.musicplayer.utils.StringUtils;
import refree.gautam.com.musicplayer.view.CircularProgressBar;

public class FragmentSearchTrack extends DBFragment implements IXMusicConstants {
    public static final String TAG = FragmentSearchTrack.class.getSimpleName();
    private boolean isDestroy;
    private MainActivity mContext;
    private String mKeyword;
    private ArrayList<TrackModel> mListTrackObjects;
    private CircularProgressBar mProgressBar;
    private RecyclerView mRecyclerView;
    private TrackDataAdapter mTrackDataAdapter;
    private TextView mTvNoResult;
    private int mTypeUI;

    class C14632 implements Runnable {
        C14632() {
        }

        public void run() {
            ArrayList<TrackModel> mListTrack = MusicNetUtils.getListTrackObjectsByQuery(StringUtils.urlEncodeString(FragmentSearchTrack.this.mKeyword), 0, 80);
            final ArrayList<TrackModel> mListDatas = FragmentSearchTrack.this.mContext.mTotalMng.getAllTrackWithAdmob(FragmentSearchTrack.this.mContext, mListTrack, FragmentSearchTrack.this.mTypeUI);
            if (mListDatas != null && mListDatas.size() > 0) {
                mListTrack.clear();
            }
            FragmentSearchTrack.this.mContext.runOnUiThread(new Runnable() {
                public void run() {
                    FragmentSearchTrack.this.showLoading(false);
                    FragmentSearchTrack.this.mContext.dimissProgressDialog();
                    if (mListDatas == null) {
                        FragmentSearchTrack.this.mContext.showToast((int) R.string.info_server_error);
                    } else {
                        FragmentSearchTrack.this.setUpInfo(mListDatas);
                    }
                }
            });
        }
    }

    public View onInflateLayout(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        return inflater.inflate(R.layout.fragment_recyclerview, container, false);
    }

    public void findView() {
        this.mContext = (MainActivity) getActivity();
        this.mRecyclerView = (RecyclerView) this.mRootView.findViewById(R.id.list_datas);
        this.mTvNoResult = (TextView) this.mRootView.findViewById(R.id.tv_no_result);
        this.mTvNoResult.setTypeface(this.mContext.mTypefaceNormal);
        this.mProgressBar = (CircularProgressBar) this.mRootView.findViewById(R.id.progressBar1);
        ConfigureModel configureModel = this.mContext.mTotalMng.getConfigureModel();
        this.mTypeUI = configureModel != null ? configureModel.getTypeSearch() : 1;
        if (this.mTypeUI == 1) {
            this.mContext.setUpRecyclerViewAsListView(this.mRecyclerView, null);
        } else {
            this.mContext.setUpRecyclerViewAsGridView(this.mRecyclerView, 2);
        }
        startSearch(this.mKeyword);
    }

    private void setUpInfo(final ArrayList<TrackModel> mListTrackObjects) {
        if (!this.isDestroy) {
            this.mRecyclerView.setAdapter(null);
            if (this.mListTrackObjects != null) {
                this.mListTrackObjects.clear();
                this.mListTrackObjects = null;
            }
            if (mListTrackObjects == null || mListTrackObjects.size() <= 0) {
                this.mTvNoResult.setText(R.string.title_no_songs);
                this.mTvNoResult.setVisibility(View.VISIBLE);
                return;
            }
            this.mListTrackObjects = mListTrackObjects;
            this.mRecyclerView.setVisibility(View.VISIBLE);
            this.mTvNoResult.setVisibility(View.GONE);
            this.mTrackDataAdapter = new TrackDataAdapter(this.mContext, this.mListTrackObjects, this.mContext.mTypefaceBold, this.mContext.mTypefaceLight, this.mTypeUI);
            this.mRecyclerView.setAdapter(this.mTrackDataAdapter);
            this.mTrackDataAdapter.setOnTrackListener(new TrackDataAdapter.OnTrackListener() {
                public void onListenTrack(TrackModel mTrackObject) {
                    FragmentSearchTrack.this.mContext.startPlayingList(mTrackObject, (ArrayList) mListTrackObjects.clone());
                }

                public void onShowMenu(View mView, TrackModel mTrackObject) {
                    FragmentSearchTrack.this.mContext.showPopupMenu(mView, mTrackObject);
                }
            });
        }
    }

    public void onDestroy() {
        super.onDestroy();
        this.isDestroy = true;
        if (this.mListTrackObjects != null) {
            this.mListTrackObjects.clear();
            this.mListTrackObjects = null;
        }
    }

    public void startSearch(String keyword) {
        if (StringUtils.isEmptyString(keyword)) {
            this.mContext.showToast((int) R.string.info_empty);
        } else if (ApplicationUtils.isOnline(this.mContext)) {
            this.mKeyword = keyword;
            this.mTvNoResult.setVisibility(View.GONE);
            showLoading(true);
            this.mRecyclerView.setVisibility(View.GONE);
            DBExecutorSupplier.getInstance().forBackgroundTasks().execute(new C14632());
        } else {
            this.mContext.showToast((int) R.string.info_lose_internet);
        }
    }

    public void showLoading(boolean b) {
        if (this.mProgressBar != null) {
            this.mProgressBar.setVisibility(b ? View.VISIBLE : View.GONE);
        }
    }

    public void notifyData() {
        if (this.mTrackDataAdapter != null) {
            this.mTrackDataAdapter.notifyDataSetChanged();
        }
    }

    public void onExtractData() {
        super.onExtractData();
        Bundle mBundle = getArguments();
        if (mBundle != null) {
            this.mKeyword = mBundle.getString(IXMusicConstants.KEY_BONUS);
        }
    }

    public boolean isCheckBack() {
        if (this.mProgressBar == null || this.mProgressBar.getVisibility() != View.VISIBLE) {
            return false;
        }
        return true;
    }
}
