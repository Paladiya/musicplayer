package refree.gautam.com.musicplayer.InterFace;

public interface IXMusicConstants {
    public static final String ACTION_DELETE_SONG = ".action.ACTION_DELETE_SONG";
    public static final String ACTION_FAVORITE = ".action.ACTION_FAVORITE";
    public static final String ACTION_PLAYLIST = ".action.ACTION_PLAYLIST";

    public static final String ADMOB_BANNER_ID = "ca-app-pub-1977039295930974/2846063533";
    public static final String ADMOB_INTERSTITIAL_ID = "ca-app-pub-1977039295930974/6593736859";

    public static final String FB_BANNER_ID = "574975896299204_574976179632509";
    public static final String FB_INTERSTITIAL_ID = "574975896299204_574976959632431";
    public static final String FB_MEDIUM_ID = "574975896299204_574977259632401";

    public static final String AD_TEST_ID = "ca-app-pub-3940256099942544/10331737121";

    public static final int[] ADS_FREQ = new int[]{1, 8, 18};
    public static final boolean DEBUG = false;
    public static final String DIR_CACHE = "xmusic_app";
    public static final String DIR_TEMP = ".temp";
    public static final String FILE_CONFIGURE = "config.json";
    public static final String FILE_GENRE = "genre.dat";
    public static final String FILE_PLAYLIST = "playlists.dat";
    public static final String FILE_SAVED_TRACK = "tracks.dat";
    public static final String KEY_BONUS = "bonus_data";
    public static final String KEY_HEADER = "KEY_HEADER";
    public static final String KEY_SHOW_URL = "KEY_SHOW_URL";
    public static final String KEY_SONG_ID = "KEY_SONG_ID";
    public static final String KEY_TYPE = "type";
    public static final int MAX_SEARCH_SONG = 80;
    public static final int MAX_SLEEP_MODE = 120;
    public static final int MAX_SONG_CACHED = 50;
    public static final int MAX_TOP_PLAYLIST_SONG = 25;
    public static final int MIN_SLEEP_MODE = 5;
    public static final int NOTIFICATION_ID = 1;
    public static final String NOTIFICATION_CHANNEL_NAME = "Free Music";
    public static final String NOTIFICATION_CHANNEL_ID = "free_music";
    public static final int NUMBER_INSTALL_DAYS = 0;
    public static final int NUMBER_LAUNCH_TIMES = 3;
    public static final int ONE_MINUTE = 60000;
    public static final String PREFIX_ACTION = "super.android.musiconline.stream";
    public static final String PREFIX_UNKNOWN = "<unknown>";
    public static final int RATE_EFFECT = 10;
    public static final int REMIND_TIME_INTERVAL = 1;
    public static final boolean SHOW_ADS = true;
    public static final boolean SHOW_BANNER_ADS_IN_HOME = true;
    public static final boolean SHOW_NATIVE_ADS = false;
    public static final boolean SHOW_SOUND_CLOUD_TAB = true;
    public static final String SOUND_CLOUD_CLIENT_ID = "a3e059563d7fd3372b49b37f00a00bcf";
    public static final int STEP_SLEEP_MODE = 5;
    public static final boolean STOP_MUSIC_WHEN_EXITS_APP = false;
    public static final String TAG_FRAGMENT_DETAIL_GENRE = "TAG_FRAGMENT_DETAIL_GENRE";
    public static final String TAG_FRAGMENT_DETAIL_PLAYLIST = "TAG_FRAGMENT_DETAIL_PLAYLIST";
    public static final String TAG_FRAGMENT_FAVORITE = "TAG_FRAGMENT_FAVORITE";
    public static final String TAG_FRAGMENT_RECOMMENDED_LIST = "TAG_FRAGMENT_RECOMMENDED_LIST";
    public static final String TAG_FRAGMENT_SEARCH = "TAG_FRAGMENT_SEARCH";
    public static final String TAG_FRAGMENT_TOP_PLAYLIST = "TAG_FRAGMENT_TOP_PLAYLIST";
    public static final String TEST_DEVICE = "51F0A3F4C13F05DD49DE0D71F2B369FB";
    public static final int TYPE_ADD_FAVORITE = 7;
    public static final int TYPE_DELETE = 11;
    public static final int TYPE_DETAIL_GENRE = 16;
    public static final int TYPE_DETAIL_PLAYLIST = 12;
    public static final int TYPE_DETAIL_RECOMMENDED = 15;
    public static final int TYPE_DETAIL_TOP_PLAYLIST = 13;
    public static final int TYPE_FILTER_SAVED = 5;
    public static final int TYPE_PLAYLIST = 9;
    public static final int TYPE_REMOVE_FAVORITE = 8;
    public static final int TYPE_UI_GRID = 2;
    public static final int TYPE_UI_LIST = 1;
    public static final String URL_FORMAT_LINK_APP = "https://play.google.com/store/apps/details?id=%1$s";
    public static final String URL_FORMAT_SUGESSTION = "http://suggestqueries.google.com/complete/search?ds=yt&output=toolbar&hl=%1$s&q=%2$s";
    public static final String URL_WEBSITE = "https://sites.google.com/view/freemusicplayer2019/home";
    public static final String YOUR_CONTACT_EMAIL = "latest.freeapp@gmail.com";
}
