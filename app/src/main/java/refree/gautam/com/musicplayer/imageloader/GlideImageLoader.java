package refree.gautam.com.musicplayer.imageloader;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.Transformation;

import java.io.File;

public class GlideImageLoader {
    public static final int DEFAULT_ANIM_TIME = 200;
    public static final String HTTP_PREFIX = "http";
    public static final String PREFIX_ASSETS = "assets://";
    public static final String PREFIX_NEW_ASSETS = "file:///android_asset/";
    public static Exception e2222,e22;

    public static void displayImage(Context mContext, ImageView mImageView, String artwork, int resId) {
        displayImage(mContext, mImageView, artwork, null, resId);
    }

    public static void displayImage(Context mContext, ImageView mImageView, String artwork, Transformation<Bitmap> mTransform, int resId) {
        if (!TextUtils.isEmpty(artwork)) {
            Uri mUri;
            if (artwork.startsWith(PREFIX_ASSETS)) {
                artwork = artwork.replace(PREFIX_ASSETS, PREFIX_NEW_ASSETS);
            }
            if (artwork.startsWith(HTTP_PREFIX)) {
                mUri = Uri.parse(artwork);
            } else {
                File mFile = new File(artwork);
                if (mFile.exists() && mFile.isFile()) {
                    mUri = Uri.fromFile(mFile);
                } else {
                    mUri = Uri.parse(artwork);
                }
            }
            if (mUri == null) {
                return;
            }
            if (mTransform != null) {
                Glide.with(mContext).load(mUri).crossFade(200).bitmapTransform(mTransform).placeholder(resId).into(mImageView);
                return;
            }
            Glide.with(mContext).load(mUri).crossFade(200).placeholder(resId).into(mImageView);
        }
    }

    public static void displayImage(Context mContext, ImageView mImageView, int resId, Transformation<Bitmap> mTransform) {
        if (resId == 0) {
            return;
        }
        if (mTransform != null) {
            Glide.with(mContext).load(Integer.valueOf(resId)).crossFade(200).bitmapTransform(mTransform).into(mImageView);
            return;
        }
        Glide.with(mContext).load(Integer.valueOf(resId)).crossFade(200).into(mImageView);
    }

    public static void displayImage(Context mContext, ImageView mImageView, int resId) {
        displayImage(mContext, mImageView, resId, null);
    }

    public static boolean displayImageFromMediaStore(Context mContext, ImageView mImageView, Uri imgUrl, int resId) {
        return displayImageFromMediaStore(mContext, mImageView, imgUrl, null, resId);
    }

    public static boolean displayImageFromMediaStore(Context mContext, ImageView mImageView, Uri imgUrl, Transformation<Bitmap> mTransformation, int resId) {
        Exception e;
        Throwable th;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try {
            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            try {
                mmr.setDataSource(mContext, imgUrl);
                byte[] rawArt = mmr.getEmbeddedPicture();
                if (rawArt == null || rawArt.length <= 0) {
                    mImageView.setImageResource(resId);
                    if (mmr != null) {
                        try {
                            mmr.release();
                        } catch (Exception e2) {
                            e2.printStackTrace();
                            mediaMetadataRetriever = mmr;
                        }
                    }
                    return false;
                }
                if (mTransformation != null) {
                    Glide.with(mContext).load(rawArt).placeholder(resId).bitmapTransform(mTransformation).crossFade(200).into(mImageView);
                } else {
                    Glide.with(mContext).load(rawArt).placeholder(resId).crossFade(200).into(mImageView);
                }
                if (mmr != null) {
                    try {
                        mmr.release();
                        return true;
                    } catch (Exception e22) {
                        e22.printStackTrace();
                    }
                }
                mediaMetadataRetriever = mmr;
                return true;
            } catch (Exception e3) {
                e22 = e3;
                mediaMetadataRetriever = mmr;
                try {
                    mImageView.setImageResource(resId);
                    e22.printStackTrace();
                    if (mediaMetadataRetriever != null) {
                        try {
                            mediaMetadataRetriever.release();
                        } catch (Exception e222) {
                            e222.printStackTrace();
                        }
                    }
                    return false;
                } catch (Throwable th2) {
                    th = th2;
                    if (mediaMetadataRetriever != null) {
                        try {
                            mediaMetadataRetriever.release();
                        } catch (Exception e2222) {
                            e2222.printStackTrace();
                        }
                    }
                    try {
                        throw th;
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }
                }
            } catch (Throwable th3) {
                th = th3;
                mediaMetadataRetriever = mmr;
                if (mediaMetadataRetriever != null) {
                    mediaMetadataRetriever.release();
                }
                try {
                    throw th;
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        } catch (Exception e4) {
            e2222 = e4;
            mImageView.setImageResource(resId);
            e2222.printStackTrace();
            if (mediaMetadataRetriever != null) {
                mediaMetadataRetriever.release();
            }
            return false;
        }
        return false;
    }
}
